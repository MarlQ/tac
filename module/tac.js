
// Import Modules
import { SimpleItemSheet } from "./item-sheet.js";
import { SimpleActorSheet } from "./actor-sheet.js";
import { ItemTac } from "./item.js";
import { ActorTac } from "./actor.js";
import { QuickAttackDialogue } from "./dialogue-quickAttack.js"
import { DiceRollerDialogue } from "./dialogue-diceRoller.js"
import { CombatTac } from "./combat.js";
import { CombatTrackerTac } from "./combatTracker.js";
import { CombatantTac } from "./combatant.js";
import {
  TokenHotBar
} from "./token-macrobar.js";
import * as templates from "./templates.js";
import { TAC } from "./config.js";
import { handleAttackRequest } from "./attack.js";
import {
  TokenTac
} from "./token.js"
import { TokenDocumentTac } from "./tokenDocument.js";
import {TACHooks} from "./hooks.js"

/**
* Various hit locations
* chances are in percent, only integers, should but don't have to add up to 100
* damage is a multiplier for damage inflicted after armour reduction
*/
/* CONFIG.hitLocations= [
  {name: "Torso", chance: 20, damage: 1},
  {name: "Head", chance: 10, damage: 2},
  {name: "Neck", chance:5, damage: 1.8},
  {name: "Vitals", chance: 5, damage: 1.8},
  {name: "Hips", chance: 10, damage: 1.4},
  {name: "Arms", chance: 15, damage: 0.8},
  {name: "Legs", chance: 15, damage: 0.8},
  {name: "Feet", chance:5, damage: 0.5},
  {name: "Hands", chance: 5, damage: 0.5},
  {name: "Grace", chance: 10, damage: 0.3},
]; */



//The locations whose hit chances increases/decreases per hit success.
//BEWARE: hitSuccessLimit * hitChanceChangePerSuccess <= lowest chance of a bad hit location !!
/* CONFIG.hitLocations_good = ["Head", "Neck", "Vitals"];
CONFIG.hitLocations_bad = ["Grace", "Legs", "Arms"]; */




CONFIG.rangeValues_firearm = [
  {name: "Melee", value: 0},
  {name: "Close", value: 1},
  {name: "Medium", value: 2},
  {name: "Far", value: 3},
  {name: "Very Far", value: 4}
];

CONFIG.rangeValues_melee= [
  {name: "Short", value: 0},
  {name: "Normal", value: 1},
  {name: "Long", value: 2}
];


CONFIG.TAC = TAC;




/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
  console.log(`Initializing 3AC System`);
  
	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
	//CONFIG.Combat.initiative.formula = "1d10 + @attributes_physical.dexterity.value + @attributes_mental.intuition.value";
  CONFIG.Combat.initiative.formula = "@attributes_mental.intuition.value";
  //Combat.prototype._getInitiativeFormula = _getInitiativeFormula;

  CONFIG.Item.documentClass = ItemTac;
  CONFIG.Actor.documentClass = ActorTac;
  CONFIG.Combat.documentClass = CombatTac;
  CONFIG.Token.objectClass = TokenTac;
  CONFIG.Token.documentClass = TokenDocumentTac;
  CONFIG.ui.combat = CombatTrackerTac;
  CONFIG.time.roundTime = 1;

  CONFIG.Combatant.documentClass = CombatantTac;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("tac", SimpleActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("tac", SimpleItemSheet, {makeDefault: true});

  templates.preloadHandlebarsTemplates();
  templates.registerHandlebarsHelpers();

  game.socket.on('system.tac', (request, ack) => {
    if(request.type === 'attack' && game.user.isGM) {
      handleAttackRequest(request);
    }
    
  });

  

  /* game.socket.on('tac.damageInflict', (request, ack) => {
    //const response = doSomethingWithRequest(request) // Construct an object to send as a response
    console.error(request)
    ack(true); // This acknowledges completion of the task, sent back to the requesting client
    socket.broadcast.emit('damageInflict', true);
  }); */
});


/**
 * This function runs after game data has been requested and loaded from the servers, so entities exist
 */
 Hooks.once("setup", function() {
  TACHooks.register();
  // Localize CONFIG objects once up-front
  const toLocalize = [
    "attributes_physical",
    "attributes_social",
    "attributes_mental",
    "skills_physical",
    "skills_social",
    "skills_mental"
  ];

  // Exclude some from sorting where the default order matters
  const noSort = [
    "attributes_physical",
    "attributes_social",
    "attributes_mental"
  ];

  // Localize and sort CONFIG objects
  for ( let o of toLocalize ) {
    const localized = Object.entries(CONFIG.TAC[o]).map(e => {
      return [e[0], game.i18n.localize(e[1])];
    });
    if ( !noSort.includes(o) ) localized.sort((a, b) => a[1].localeCompare(b[1]));
    CONFIG.TAC[o] = localized.reduce((obj, e) => {
      obj[e[0]] = e[1];
      return obj;
    }, {});
  }
});

/* -------------------------------------------- */
/*  Foundry VTT Ready                           */
/* -------------------------------------------- */

Hooks.once("ready", async function() {
  
  //Set default flags
  let defaultFlags = {
    attack: {ranged: {self_running: false, self_distracted: false, self_offHand: false, target_hidden: false, 
           target_running: false, target_outnumbered: false, target_defending: false,
           target_unaware: false,target_immobile: false, aimLocation: "Any", cover_chance: 0, cover_strength: 2, target_distance: 2,
           dicePool_userMod: 0, targetNumber_userMod: 0, damage_userMod: 0, position: 0}, 
           melee: {self_running: false, self_distracted: false, self_offHand: false, target_hidden: false, 
           target_running: false, target_outnumbered: false, target_defending: false,
           target_unaware: false,target_immobile: false, aimLocation: "Any", cover_chance: 0, cover_strength: 0, target_distance: 1,
           dicePool_userMod: 0, targetNumber_userMod: 0, damage_userMod: 0, position: 0}}
  };
  let userFlags = game.users.current.flags.tac;
  if(!userFlags) game.users.current.update({"flags.tac": defaultFlags});
  else mergeObject(userFlags, defaultFlags, {insertKeys: true, insertValues: true, overwrite: false, inplace: true, enforceTypes: true});
  console.log("User Flags:");
  console.trace(game.users.current.flags.tac);
  

  CONFIG.TOKENBAR = TokenHotBar.tokenHotbarInit();
  game.tooltip.constructor.TOOLTIP_ACTIVATION_MS = 0;
  debounce(createTokenBar, 200);
  //game.users.current.flags.tac.attack.ranged.range = "Far";
  
  
});


/* game.scenes.forEach(scene => {
  scene.tokens.forEach(token => {
    if(!token.document.actorLink && token._actor?.items) {
      console.log(token)
      token._actor.items.forEach(item => {
        if(item.type === "firearm" && item.system.magazine?.system?) {
          const magazine = item.system.magazine;
          console.trace(item)
          let newMagazine = {};
          newMagazine = duplicate(magazine.data);
          newMagazine.system = duplicate(magazine.data.data);
          newMagazine.data = undefined;
    
          console.log(newMagazine)
          const updateData = {"system.magazine": newMagazine};
          //updateData._id = item._id;
          //token.updateEmbeddedDocuments('Item', [updateData])
          item.update(updateData)
        }
      })
    }
    
  })
}) */

/* game.actors.forEach(actor => {
  
})
 */
$(document).ready(() => {


  const diceIconSelector = '#chat-controls .roll-type-select .fa-dice-d20';
  
  
  $(document).on('click', diceIconSelector, ev => {
    ev.preventDefault();
    
    let diceRoller = new DiceRollerDialogue();
    diceRoller.render(true);
  
  });
  
});

/* Token macro bar */
let createTokenBar = (token, select) =>
  {
    let controlled = canvas.tokens.controlled;
    if(controlled.length){
      CONFIG.TOKENBAR.tokens = controlled;
      CONFIG.TOKENBAR.render(true);
    }
    else{
      CONFIG.TOKENBAR.close();
    }  
  }

Hooks.on("controlToken", debounce(createTokenBar, 200));

Hooks.on("updateActor", debounce(createTokenBar, 200));
Hooks.on("updateItem", debounce(createTokenBar, 200));
Hooks.on("updateToken", debounce(createTokenBar, 200));
Hooks.on("createItem", debounce(createTokenBar, 200));
Hooks.on("deleteItem", debounce(createTokenBar, 200));

