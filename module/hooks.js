







export class TACHooks {


    static register(){
        TACHooks._patchWall();
        Hooks.on(`renderWallConfig`, TACHooks._renderWallConfig);
        Hooks.on(`canvasReady`, TACHooks._canvasReady);
    }

    static _canvasReady(canvas) {
        // Initialize variables
        let line = null;
        let text = null;
        
        // Function to create or update the line and text
        const updateLineAndText = async (position, ticks) => {
            if (!line) {
                line = new PIXI.Graphics();
                canvas.stage.addChild(line);
            }
            line.clear();
            line.lineStyle(2, 0xFF0000);
            line.moveTo(canvas.tokens.controlled[0].center.x, canvas.tokens.controlled[0].center.y); // Assuming only one unit is selected
            line.lineTo(position.x, position.y);
            
            if (!text) {
                text = new PIXI.Text(`${ticks} ticks`, { fill: 'white' });
                canvas.stage.addChild(text);
            }
            text.text = `${ticks} ticks`;
            text.position.set(position.x + 10, position.y);
        };
        
        // Function to handle mouse move event
        const handleMouseMove = async (event) => {
            if (canvas.ready && canvas.stage && canvas.tokens.controlled.length) {
                const position = canvas.app.renderer.plugins.interaction.pointer.getLocalPosition(canvas.app.stage);
                const ticks = calculateTicks({ x: canvas.tokens.controlled[0].x, y: canvas.tokens.controlled[0].y }, position);
                updateLineAndText(position, ticks);
            }
            else {
                if (line) {
                    line.clear();
                }
                if (text) {
                    text.destroy();
                    text = null;
                }
            }
        };
    
        // Function to handle mouse out event
        const handleMouseOut = () => {
            if (line) {
                line.clear();
            }
            if (text) {
                text.destroy();
                text = null;
            }
        };
        
        // Function to calculate the number of ticks
        const calculateTicks = (positionA, positionB) => {
            const distances = canvas.grid.measureDistance(positionA, positionB);
            
            const meters = distances * canvas.scene.grid.distance;
            const agility = canvas.tokens.controlled[0].actor.system.run;
            return Math.ceil(meters / agility);
        };
    
        // Event listeners
        canvas.app.view.addEventListener('pointermove', handleMouseMove);
        canvas.app.view.addEventListener('pointerout', handleMouseOut);
    
        // Clean up event listeners when canvas is destroyed
        canvas.app.view.addEventListener('destroy', () => {
            canvas.app.view.removeEventListener('pointermove', handleMouseMove);
            canvas.app.view.removeEventListener('pointerout', handleMouseOut);
        });
    }
    
    static _renderWallConfig(app, html){
        let element = html.find('form > .form-group');
        element = element[element.length-1]
        
        if (!app.object?.object) return;
        console.log("WUT", app, html, element)

        const status_strength = app.object.object.getCoverStrength() ?? 0;
        const status_height = app.object.object.getCoverHeight() ?? 0;
        const selectHTML = `<div class="form-group">
                            <label>${game.i18n.localize("TAC.cover_strength")}</label>
                            <select name="flags.tac.cover_strength" data-dtype="Number">
                                ${
                                    Object.entries(CONFIG.TAC.COVER_STRENGTH_LEVELS).reduce((acc, [key,value]) => acc+=`<option value="${key}" ${key == status_strength ? 'selected' : ''}>${game.i18n.localize('TAC.' + key)}</option>`, ``)
                                }
                            </select>
                            </div>
                            <div class="form-group">
                            <label>${game.i18n.localize("TAC.cover_height")}</label>
                            <select name="flags.tac.cover_height" data-dtype="Number">
                                ${
                                    Object.entries(CONFIG.TAC.COVER_HEIGHT_LEVELS).reduce((acc, [key,value]) => acc+=`<option value="${key}" ${key == status_height ? 'selected' : ''}>${game.i18n.localize('TAC.' + key)}</option>`, ``)
                                }
                            </select>
                            </div>
                            `;

        html.css("height", "auto");
        $(element).after(selectHTML);
    }


    static _createWall(document) {
        document.setFlag("tac", )

    }

    static _patchWall() {
        Wall.prototype.getCoverStrength = function() {

            const definedCover = this.document.getFlag("tac", "cover_strength");

            if (definedCover != undefined) return definedCover;
            return 'strong';
        }
        Wall.prototype.getCoverHeight = function() {

            const definedCover = this.document.getFlag("tac", "cover_height");

            if (definedCover != undefined) return definedCover;
            return 'full';
        }
    }

}