import { AttackDialogue } from "./dialogue-attack.js";
import { ReloadDialogue } from "./dialogue-reload.js";
import { createShortActionMessage } from "./chat.js";

/**
 * Override and extend the basic :class:`Item` implementation
 */
export class ItemTac extends Item {

  /**
   * Augment the basic Item data model with additional dynamic data.
   */
   prepareData() {
    super.prepareData();
    if (!this.img || this.img === "icons/svg/item-bag.svg" || this.img.startsWith('systems/tac/icons/placeholders')){
      this._setPlaceholderIcon();
    }
    this.system.tooltip = this.getTooltip();


    if(this.type === "condition") {

      if(this.system.tick) {
        // let tick = this.system.tick;
        // let nextTickEffect = this.system.effects.filter(e => e.interval).reduce( (prev, cur) => {
        //   return ((prev.interval - (tick % prev.interval)) > (cur.interval - (tick % cur.interval)) ) ? prev : cur;
        // }, {});
        // if(nextTickEffect) {
        //   this.system.remainingTicks = nextTickEffect.interval - (tick % nextTickEffect.interval);
        // }


        if(this.system.duration) {
          this.system.remainingDuration = this.system.duration - this.system.tick;
        }
      }
      
    }

    if(this.type === 'firearm') {
      let systemData = this.system;
      let magazine = systemData.magazine;
      const attributes = ['damage', 'penetration', 'impact', 'projectiles', 'recoil', 'deviation'];
      attributes.forEach(attribute => {
        systemData[attribute + '_mod'] = systemData[attribute];
        if(magazine) {
          systemData[attribute + '_mod'] = Math.max(magazine.system[attribute] + systemData[attribute + '_mod'], 0);
        }
      });
      if(magazine) {
        systemData.range_mod = (+systemData.range + +magazine.system.range) / 2;
        if(systemData.range > systemData.range_mod) systemData.range_mod = Math.ceil(systemData.range_mod);
        else systemData.range_mod = Math.floor(systemData.range_mod);
      }
      else systemData.range_mod = +systemData.range;
      systemData.range_mod = Math.clamped(systemData.range_mod,0,4);
    }
    
    //data.damageType_mod = 
    
    /* 
    if(this.system.magazine) {
      let magazineAttributes = this.system.magazine.system;
      attributes.forEach(attribute => Object.keys(attribute).forEach(key => {
        let trait = attribute[key];
        trait.raw = trait.raw + magazineAttributes[key]; 
        trait.value = Math.clamped(trait.raw, 0, CONFIG.TAC.attribute_maximum);
      }));

    } */
  }

  _setPlaceholderIcon() {
    const data = this.system;
    let img = 'systems/tac/icons/placeholders/item-placeholder.svg';

      let type;
      if(this.type === "melee"){
        if(data.slashing && !data.piercing && !data.crushing) {
          if(data.range < 1) type = "melee_hatchet";
          else type = "melee_axe";
        }
        else if(!data.slashing && data.piercing && !data.crushing) {
          if(data.range < 1) type = "melee_dagger";
          else type = "melee_spear";
        }
        else if(!data.slashing && !data.piercing && data.crushing) {
          if(data.range < 1) type = "melee_baton";
          else type = "melee_club";
        }
        else if(data.slashing && !data.piercing && data.crushing) {
          type = "melee_spikeClub";
        }
        else {
          if(data.range < 1) type = "melee_knife";
          else type = "melee_sword";
        }
      }
      else if(this.type === "firearm"){
        if(data.projectiles > 1) { // Shotgun
          type = "firearm_shotgun";
        }
        else if(data.range <= 1 && !data.fullAuto) { // Pistol
          if(data.recoil > 4) type = "firearm_pistol_heavy";
          else type = "firearm_pistol_light";
        }
        else if(data.range <= 2) { // SMG
          type = "firearm_smg";
        }
        else { // Rifle
          if(data.singleShot && !data.burstFire && !data.fullAuto) type = "firearm_rifle";
          else type = "firearm_ar";
        }
      }
      else if(this.type === "ammo") type = "ammo";
      else if(this.type === "consumable") type = "consumable";
      else if(this.type === "armour"){
        if(data.coverage <= 10) type = "armour_kneepad";
        else if(data.coverage <= 15) type = "armour_boots";
        else if(data.coverage <= 30) type = "armour_helmet";
        else {
          if(data.gunshot > 3) type = "armour_kevlar";
          else if(data.slashing > 1 && data.crushing > 2) type = "armour_riot";
          else type = "armour_standard";
        }
      }
      
      if(type) img = 'systems/tac/icons/placeholders/' + type + '.svg';
      //if(!img) img = 'systems/tac/icons/placeholders/item-placeholder.svg';
      //this.data._source.img = img; 
      this.img = img; 
  }

  get tooltip() {
    return this.getTooltip();
  }

  get buffs() {
    if(!this.system.effects) return undefined;
    let retEffects = [];

    if(Array.isArray(this.system.effects)) {
      // Scale effects according to severity
      for(const effect of this.system.effects) {
        const severityScaling = effect.severityScaling == undefined ? true : effect.severityScaling;
        retEffects.push({...effect, value: effect.value * (severityScaling ? (this.system.severity ?? 1) : 1)});
      }
    }
    else {
      // Get effects from current severity
      const effects = this.system.effects[severity];
      if(Array.isArray(effects)) {
        for(const effect of effects) {
          retEffects.push({...effect});
        }
      }
    }
    return retEffects;
  }
  
  getTooltip() { //TODO:
    let traitHtml = '';

    const traits = {
      ammo: ['damage', 'damageType'],
      armour: ['gunshot', 'crushing', 'slashing', 'piercing', 'coverage', 'durability'],
      condition: [],
      firearm: ['damage_mod', 'damageType', 'penetration_mod', 'range_mod', 'impact_mod', 'recoil_mod', 'deviation_mod', 'projectiles_mod'],
      melee: ['piercing', 'slashing', 'crushing', 'penetration', 'impact'],
      consumable: [],
    }

    if(this.type === "condition") {
      traitHtml = this.buffs.reduce((prev, effect) => {
        if(effect.type === 'trait_buff') {
          return prev += `<div class="trait">${game.i18n.localize('TAC.' + effect.trait.split('.')[1])}: ${effect.value}</div>`;
        }
        else return prev;
      }, '');
    }

    traits[this.type].forEach(trait => {
      let traitValue;
      let traitName;
      if(trait === 'armor') {

      }
      else {
        traitName = trait;
        traitValue = this.system[trait];
      }

        traitHtml += `<div class="trait">${game.i18n.localize('TAC.' + traitName)}: ${traitValue}</div>`;
    });
    let currencyHtml = '';

    let localisedName = this.type === "condition" ? game.i18n.localize('TAC.CONDITIONS.' + this.name) : this.name;

    let html = `
    <span class="tooltip-text">
        <span class="title">${localisedName}</span>
        ${traitHtml}
        <div class="item-description">
          ${this.system.description}
        </div>
        <div class="item-price">
            ${currencyHtml}
        </div>
    </span>`;
    return html;
  }

  /* -------------------------------------------- */

  /**
   * Roll the item to Chat, creating a chat card which contains follow up attack or damage roll options
   * @return {Promise}
   */
  async attack(rollType, bullets=0, damage=1, damageType, ticks_cost, clarity_cost, token) {
    let attackDialogue = new AttackDialogue({weapon: this, rollType: rollType, bullets: bullets, weapon_damage: damage, damageType: damageType, ticks_cost: ticks_cost, clarity_cost: clarity_cost, actor_token: token});
  }

  get capacity() {
    if(this.system.magazine) return this.system.magazine.system.capacity;
    else return 0;
  }

  /**
   * Handle reloading of a firearm
   * @private
   */
   async reload(element, actor_token) {
    console.log("RELO", this)
    const actor = this.actor;
    if(!actor) return;
    if(this.system.magazine){ // Eject
      createShortActionMessage("Ejects magazine");
      
      if(element?.classList) element.classList.remove("reloaded"); //TODO ??
      
      //Add ejected ammo back into the inventory
      const ammoData = {
        name: this.system.magazine.name,
        type: this.system.magazine.type,
        img: this.system.magazine.img,
        system: this.system.magazine.system
      };

     /*  const inventory = actor.items;
      
      const fittingAmmo = inventory.find(ele => (ele.name === ammoData.name) && (ele.type === ammoData.type))
      console.log(ammoData)
      //const index = inventory.findIndex(ele => (ele.name === ammoData.name) && (ele.type === ammoData.type));
      if(fittingAmmo) await fittingAmmo.update({
        'system.quantity': fittingAmmo.system.quantity + ammoData.system.quantity}); //Add ammo to existing mag
      else  */
      await actor.createEmbeddedDocuments("Item", [ammoData]); //Add new ammo item

      this.update({"system.magazine": null}); //Remove mag from weapon
    }
    else{
      // Open reload menu
      let ammoList = new ReloadDialogue({item: this, button: element, actor_token: actor_token});
      ammoList.render(true);
    }
  }

  adjustWeaponHeat(change) {
    if(isNaN(change)) return;
    
    if(this.system.hasOwnProperty('heat')) {
      this.update({'system.heat': Math.clamped(this.system.heat+change, 0, CONFIG.TAC.HEAT_MAXIMUM)});
    }
  }

  async useConsumable(token) {
    /* if(this.system.conditions && this.actor) {
      this.system.conditions.forEach(condition => {
        this.actor.applyEffect({
          name: condition
        });
      });
    } */
    this.actor.applyActionCost( this.getTicksCost('use_consumable'), this.getClarityCost('use_consumable'), token);

    if(this.system.condition && this.actor) {
        await this.actor.applyEffect({
          name: this.system.condition
        });
    }
    await this.update({'system.charges': Math.max(0, this.system.charges - 1)});

    
  }


  getTraits() {
    let traits = this.system.traits;
    console.log("1", traits, this.system.traits)
    if(this.type === "firearm" && this.system.magazine) {
      traits = traits.concat(this.system.magazine.system.traits);
      console.log("2", traits)
    }
    return traits;
  }

  getEffectsOfType(type) {
    let effects = [];
    this.getTraits().forEach(trait => { 
      const cond = CONFIG.TAC.conditions[trait];
      console.log("ASD", cond, type, this.getTraits())
      effects = effects.concat(cond.effects.filter(effect => effect.type === type));
    });
    console.log("EFFECTS OF TYPE", effects, type)
    return effects;
  }

  addTrait(name) {
    if(!CONFIG.TAC.conditions[name] && this.hasTrait(name)) return;
    let traits = this.system.traits ? duplicate(this.system.traits) : [];
    traits.push(name)

    const updateData = {'system.traits': traits};
    this.update(updateData);
  }

  removeTrait(name) {
    let traits = this.system.traits.filter(t => t !== name);
    const updateData = {'system.traits': traits};
    this.update(updateData);
  }

  hasTrait(name) {
    return this.system.traits.some(trait => trait === name)
  }

  /**
   * Returns all the skills associated with the item
   * @return {Promise}
  */
  getSkills() {
    console.log(this)
    const skills = [
      {
        name: "Use " + this.name,
        img: this.img,
        ticks_cost: this.getTicksCost('use_consumable'),
        clarity_cost: this.getClarityCost('use_consumable'),
        condition: () => {
          return (this.type === "consumable" && this.system.charges);
        },
        callback: (token) => {
          createShortActionMessage("Uses " + this.name, token.actor);
          return this.useConsumable(token);
        }
      },
      {
        name: "Single Shot",
        img: "systems/tac/icons/skills/single-shot.svg",
        ticks_cost: this.getTicksCost('single_shot'),
        clarity_cost: this.getClarityCost('single_shot'),
        condition: () => {
          return (this.type === "firearm" && this.system.singleShot === true && !this.system.jammed);
        },
        callback: (token) => {
          return this.attack("Single Shot", 1, this.system.damage, this.system.damageType, this.getTicksCost('single_shot'), this.getClarityCost('single_shot'), token);
        }
      },
      {
        name: "Burst Fire (" + this.system?.burstFire + " shots)",
        img: "systems/tac/icons/skills/burst-fire.svg",
        ticks_cost: this.getTicksCost('burst_fire'),
        clarity_cost: this.getClarityCost('burst_fire'),
        condition: () => {
          return (this.type === "firearm" && this.system.burstFire > 0 && !this.system.jammed);
        },
        callback: (token) => {
          return this.attack("Burst Fire", this.system.burstFire, this.system.damage, this.system.damageType, this.getTicksCost('burst_fire'), this.getClarityCost('burst_fire'), token); 
        }
      },
      {
        name: "Full Auto (" + this.system?.fullAuto + " shots)",
        img: "systems/tac/icons/skills/full-auto.svg",
        ticks_cost: this.getTicksCost('full_auto'),
        clarity_cost: this.getClarityCost('full_auto'),
        condition: () => {
          return (this.type === "firearm" && this.system.fullAuto > 0 && !this.system.jammed);
        },
        callback: (token) => {
          return this.attack("Full Auto", this.system.fullAuto, this.system.damage, this.system.damageType, this.getTicksCost('full_auto'), this.getClarityCost('full_auto'), token);
        }
      },
      {
        name: "Unjam",
        img: "systems/tac/icons/skills/unjam.svg",
        ticks_cost: this.getTicksCost('unjam'),
        clarity_cost: this.getClarityCost('unjam'),
        condition: () => {
          return (this.system.jammed);
        },
        callback: async (token) => {
          createShortActionMessage("Unjams their weapon", token.actor);
          await this.actor.applyActionCost( this.getTicksCost('unjam'), this.getClarityCost('unjam'), token)
          return this.update({'system.jammed': false});
        }
      },
      {
        name: "Stab",
        img: "systems/tac/icons/skills/stab.svg",
        ticks_cost: this.getTicksCost('melee_attack'),
        clarity_cost: this.getClarityCost('melee_attack'),
        condition: () => {
          return (this.type === "melee" && this.system.piercing > 0);
        },
        callback: (token) => {
          return this.attack("Stab", 1, this.system.piercing, "piercing", this.getTicksCost('melee_attack'), this.getClarityCost('melee_attack'), token);
        }
      },
      {
        name: "Slash",
        img: "systems/tac/icons/skills/slash.svg",
        ticks_cost: this.getTicksCost('melee_attack'),
        clarity_cost: this.getClarityCost('melee_attack'),
        condition: () => {
          return (this.type === "melee" && this.system.slashing > 0);
        },
        callback: (token) => {
          return this.attack("Slash", 1, this.system.slashing, "slashing", this.getTicksCost('melee_attack'), this.getClarityCost('melee_attack'), token);
        }
      },
      {
        name: "Bash",
        img: "systems/tac/icons/skills/bash.svg",
        ticks_cost: this.getTicksCost('melee_attack'),
        clarity_cost: this.getClarityCost('melee_attack'),
        condition: () => {
          return (this.type === "melee" && this.system.crushing > 0);
        },
        callback: (token) => {
          return this.attack("Bash", 1, this.system.crushing, "crushing", this.getTicksCost('melee_attack'), this.getClarityCost('melee_attack'), token);
        }
      },
      {
        name: "Crippling Shot",
        img: "systems/tac/icons/skills/crippling-shot.svg",
        ticks_cost: this.getTicksCost('crippling_shot'),
        clarity_cost: this.getClarityCost('crippling_shot'),
        description: `<p>An attack which inflicts the Crippled condition on the target, decreasing the target's action speed by 1 per stack, and halving its movement speed. Applies 1 stack per success (max. 5).</p>
        ${this.actor?.getTalentStacks('ripe_target') ? "<span class='text-highlight'>[Ripe target]</span> " + CONFIG.TAC.TALENT_TREES.shotguns.nodes.ripe_target.description[this.actor.getTalentStacks('ripe_target')-1] : ''}`,
        condition: () => {
          return (this.type === "firearm" && this.system.singleShot === true && !this.system.jammed && this.system.subtype === "shotgun" && this.actor?.getTalentStacks('crippling_shot'));
        },
        callback: (token) => {
          return this.attack("crippling_shot", 1, this.system.damage, this.system.damageType, this.getTicksCost('crippling_shot'), this.getClarityCost('crippling_shot'), token);
        }
      },
    ];
    /* skills.forEach(s => {
      if(!s.ticks_cost) s.ticks_cost = 0;
      if(!s.clarity_cost) s.clarity_cost = 0;
      if(this.actor) s.clarity_cost = Math.floor(s.clarity_cost * this.actor.system.other_traits.clarity_cost_multiplier.value);
    }); */
    return skills.filter(ele => ele.condition());
  }

  getTicksCost(action) {
    if(this.actor) return this.actor.getTicksCost(action, this);

    const configEntry = CONFIG.TAC.BASIC_ACTIONS[action];
    if(!configEntry) {
      console.error("Action not found! ", action);
      return 0;
    }
    return configEntry.ticks_cost ? configEntry.ticks_cost : 0;
  }

  getClarityCost(action) {

    if(this.actor) return this.actor.getClarityCost(action, this);

    const configEntry = CONFIG.TAC.BASIC_ACTIONS[action];
    if(!configEntry) {
      console.error("Action not found! ", action);
      return 0;
    }
    return configEntry.clarity_cost ? configEntry.clarity_cost : 0;
  }

  get statusEffectSeverityBar() {
    if(!this.system.severity_max ||  this.system.severity_max < 2) return '';

    const percentage = Math.floor(this.system.severity/this.system.severity_max*100) +"%";
    if(this.system.severity_max === 2) {
        return `
        <div class="intensity-bar" style="width: 29px; box-shadow: 0 0 5px #000 inset, 0 0 3px 0px ${this.mainColor};
        background: 
        linear-gradient(to right, #0000 0%, #0000 13px, #000f 13px, #000f 16px, #0000 16px),
        linear-gradient(to right, ${this.mainColor} 0%, ${this.mainColor} ${percentage}, grey ${percentage}, grey 100%);"></div>`
    }
    if(this.system.severity_max === 3) {
        return `
        <div class="intensity-bar" style="width: 30px; box-shadow: 0 0 5px #000 inset, 0 0 3px 0px ${this.mainColor};
        background: 
        linear-gradient(to right, #0000 0%, #0000 8px, #000f 8px, #000f 11px, #0000 11px),
        linear-gradient(to right, #0000 0%, #0000 19px, #000f 19px, #000f 22px, #0000 22px),
        linear-gradient(to right, ${this.mainColor} 0%, ${this.mainColor} ${percentage}, grey ${percentage}, grey 100%);"></div>`
    }
    if(this.system.severity_max === 4) {
        return `
        <div class="intensity-bar" style="width: 30px; box-shadow: 0 0 5px #000 inset, 0 0 3px 0px ${this.mainColor};
        background: 
        linear-gradient(to right, #0000 0%, #0000 6px, #000f 6px, #000f 8px, #0000 8px),
        linear-gradient(to right, #0000 0%, #0000 14px, #000f 14px, #000f 16px, #0000 16px),
        linear-gradient(to right, #0000 0%, #0000 22px, #000f 22px, #000f 24px, #0000 24px),
        linear-gradient(to right, ${this.mainColor} 0%, ${this.mainColor} ${percentage}, grey ${percentage}, grey 100%);"></div>`;
    } // var(--color-background-alt) grey
    else {
        return `
        <div class="intensity-bar" style="width: 28px; box-shadow: 0 0 2px #000 inset, 0 0 0px 1px black;
        background: 
        linear-gradient(to right, #0000 0%, #0000 4px, #000f 4px, #000f 6px, #0000 6px),
        linear-gradient(to right, #0000 0%, #0000 10px, #000f 10px, #000f 12px, #0000 12px),  
        linear-gradient(to right, #0000 0%, #0000 16px, #000f 16px, #000f 18px, #0000 18px),
        linear-gradient(to right, #0000 0%, #0000 22px, #000f 22px, #000f 24px, #0000 24px),
        linear-gradient(to right, ${this.mainColor} 0%, ${this.mainColor} ${percentage}, grey ${percentage}, grey 100%);"></div>`
    }
  }

  get mainColor() {
    return CONFIG.TAC.COLORS.conditions_types[this.system.type] ?? CONFIG.TAC.COLORS.conditions_types.default;
  }

  get backgroundColor() {
    return CONFIG.TAC.COLORS.conditions_types_background[this.system.type] ?? CONFIG.TAC.COLORS.conditions_types_background.default;
  }
}
