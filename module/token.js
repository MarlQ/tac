
export class TokenTac extends Token {

  /**
   * Draw a single resource bar, given provided data
   * @Override Draws a custom bar for health, otherwise uses the default function.
   */
  _drawBar( number, bar, data ) {
    if ( data.attribute === 'health' || data.attribute === 'clarity' || data.attribute === 'stamina') {
      let health = this.actor?.system?.[data.attribute];
      if ( health ) {
        // Handle health rendering
        const hor_padding = 12;
        const val = Number( data.value );
        const pct = Math.clamped( val, 0, data.max ) / data.max;
        const pct_damageMax = Math.clamped( health.max_cur, 0, data.max ) / data.max;

        // Determine sizing
        let h = Math.max( ( canvas.dimensions.size / 16 ), 2 );
        if(data.attribute === 'stamina') h /= 2;
        const w = this.w;
        const bs = Math.clamped( h / 8, 1, 2 );
        if ( this.height >= 2 ) h *= 1.6;  // Enlarge the bar for large tokens
        // Determine the color to use
        const blk = 0x000000;
/*         let color;
        if ( number === 0 ) color = PIXI.utils.rgb2hex( [ ( 1 - ( pct / 2 ) ), pct, 0 ] );
        else color = PIXI.utils.rgb2hex( [ ( 0.5 * pct ), ( 0.7 * pct ), 0.5 + ( pct / 2 ) ] ); */

        let color_health = CONFIG.TAC.COLORS[data.attribute].heal;
        let color_damage_max = CONFIG.TAC.COLORS[data.attribute].damage_max;
        let color_background = 0x000000;

        // Draw the bar
        bar.clear();

        let margin = 0;

        bar.beginFill( color_background, 0.5 ).lineStyle( bs, blk, 0.0 ).drawRect( hor_padding, 0, this.w - hor_padding*2, h);
        let sd = bar.beginFill( color_health, 0.8 ).lineStyle( bs, blk, 0.5 ).drawRoundedRect( hor_padding + margin, 0 + margin, pct * (w - hor_padding*2) - margin*2, h - margin*2, 1 );
        bar.beginFill( color_damage_max, 0.8 ).lineStyle( bs, blk, 0.5 ).drawRoundedRect( (w - hor_padding*2)*pct_damageMax + hor_padding, 0, this.w - (w - hor_padding*2)*pct_damageMax - hor_padding*2, h, 1 );
        bar.beginFill( color_background, 0.0 ).lineStyle( bs, blk, 0.5 ).drawRect( hor_padding, 0, this.w - hor_padding*2, h);

        /* sd.filters = [
          new PIXI.filters.GlowFilter({ distance: 5, outerStrength: 2, color: color_health})
        ]; */

        // Set position
        //let posY = number === 0 ? this.h - h : 0;
        let posY =  number === 0 ? - 2*h : - h;
        bar.position.set( 0, posY );
        return;
      }
    }
    // Default
    super._drawBar( number, bar, data );
  }

  getSurroundingTokens(distance) {
    return this.document.getSurroundingTokens(distance);
  }
}
