import { AttackDialogue } from "./dialogue-attack.js";
import { WeaponSwitchDialogue } from "./dialogue-weaponSwitch.js";
import { createShortActionMessage } from "./chat.js";
export class TokenHotBar extends Application {
    constructor(options) {
        super(options);
      }

    /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["worldbuilding", "dialogue"],
            template: "systems/tac/templates/other/tokenHotBar.html",
            popOut: false,
            minimisable: false,
            resisable: false
      });
    }

    getData() {
        const data = super.getData();
        data.config = CONFIG.TAC;
        if(this.tokens){
            this.macros = [];
            this.weapons = [];

            // Prepare generic macros
            /* this.macros.push({
                name: "Skill Check",
                img: "systems/tac/icons/gui/d10.svg",
                sheetMacro: true,
                callback: () => {
                    this.tokens.forEach(token => {
                        //if(token.actor) token.actor.rollAwareness(true, true, token.actor);
                    })
                 }
            }); */

            // Create name for macro bar
            if(this.tokens.length > 1){
                let names = this.tokens.map(token => token.name);
                data.characterName = names.join(", ");
            }

            // Only show item macros if only 1 token was selected
            if(this.tokens.length === 1){
                let token = this.tokens[0];
    
                data.characterName = token.name;
                if(token.actor){
        
                    this.macros = token.actor.getSkills();

                    
                    
                    

                    // Add equipped items and favourited abilities
                    //let equipped = token.actor.items.filter(item => (typeOrder.has(item.type) && item.system.equipped));
                    let equipped = Object.keys(token.actor.system.equipmentSlots).filter(slot => token.actor.system.equipmentSlots[slot].weaponSlot);

                    data.conditions = token.actor.items.filter(item => item.type === "condition");

                    

                    if(token.actor.system.encumbered !== 'none') {
                        const severity = token.actor.system.encumbered === 'heavy' ? 3 : token.actor.system.encumbered === 'medium' ? 2 : 1;
                        const title = game.i18n.localize('TAC.Encumbered_' + token.actor.system.encumbered);
                        const percentage = Math.floor(severity/3*100) +"%";
                        let traitsHtml = '';
                        Object.entries(CONFIG.TAC.ENCUMBRANCE_MODIFIERS[token.actor.system.encumbered]).forEach(e => {
                            const lName = game.i18n.localize('TAC.' + e[0].split('.')[1]);
                            traitsHtml += `<div class="trait">${lName}: ${e[1]}</div>`
                        })
                        data.encumbrance = {
                            img: "systems/tac/icons/conditions/encumbered.svg",
                            name: title,
                            system: {
                                type: 'debilitation',
                                severity: severity,
                                severity_max: 3
                            },
                            tooltip: 
                            `<span class="tooltip-text">
                                <span class="title">${title}</span>
                                ${traitsHtml}
                                <div class="item-description">
                                
                                </div>
                            </span>`,
                            mainColor: CONFIG.TAC.COLORS.conditions_types.debilitation,
                            backgroundColor: CONFIG.TAC.COLORS.conditions_types_background.debilitation,
                            statusEffectSeverityBar: `
                            <div class="intensity-bar" style="width: 30px; box-shadow: 0 0 5px #000 inset, 0 0 3px 0px ${CONFIG.TAC.COLORS.conditions_types.debilitation};
                            background: 
                            linear-gradient(to right, #0000 0%, #0000 8px, #000f 8px, #000f 11px, #0000 11px),
                            linear-gradient(to right, #0000 0%, #0000 19px, #000f 19px, #000f 22px, #0000 22px),
                            linear-gradient(to right, ${CONFIG.TAC.COLORS.conditions_types.debilitation} 0%, ${CONFIG.TAC.COLORS.conditions_types.debilitation} ${percentage}, grey ${percentage}, grey 100%);"></div>`
                        };
                    }

                    let weaponSlotFree = false;
                    equipped.forEach(slotKey => {
                        let slot = token.actor.system.equipmentSlots[slotKey];
                        let item = slot.item;
                        if(slot.itemId === "") {
                            this.weapons.push({
                                name: "Fists",
                                img: slot.icon,
                                usesHeat: false,
                                slot: slotKey,
                                macros: [
                                    {
                                        name: "Punch",
                                        img: "systems/tac/icons/skills/bash.svg", // TODO: Different icon (?)
                                        ticks_cost: token.actor.getTicksCost('unarmed_attack'),
                                        clarity_cost: token.actor.getClarityCost('unarmed_attack'),
                                        callback: () => {
                                            let attackDialogue = new AttackDialogue( {
                                                weapon: { // actorToken: token, FIXME:
                                                actor: token.actor,
                                                type: "melee",
                                                name: "Fists",
                                                system: {
                                                    impact: 0,
                                                    penetration: 1,
                                                    range: 0
                                                }
                                            }, 
                                                rollType: 
                                                "Punch", 
                                                bullets: 1, 
                                                weapon_damage: 0, 
                                                damage_type: "crushing", 
                                                ticks_cost: token.actor.getTicksCost('unarmed_attack'),
                                                clarity_cost: token.actor.getClarityCost('unarmed_attack'),
                                                actor_token: token
                                            });
                                        },
                                        canBeUsed: token.actor.system.clarity.value >= CONFIG.TAC.BASIC_ACTIONS.unarmed_attack.clarity_cost
                                    }
                                ]
                            });
                            weaponSlotFree = true;
                            return;
                        }
                        //let itemEntity = token.actor.items.get(item.id);
                        /* let skills = item.getSkills();
                        console.log(skills)
                        skills.forEach(ele => this.macros.push(ele)); */
                        
                        /* this.macros.push({
                            name: item.name,
                            img: item.data.img,
                            type: item.type,
                            notEquipped: typeOrder.has(item.type) && !item.system.equipped,
                            isFavorite: item.system.isFavorite,
                            callback: () => {
                                return itemEntity.roll();
                            }
                        }) */
                        if(item.type == "firearm" || item.type == "melee") {
                            let macros = item.getSkills();
                            macros.forEach(m => {
                                m.canBeUsed = token.actor.system.clarity.value >= m.clarity_cost;
                            });

                            this.weapons.push({
                                _id: item.id,
                                name: item.name,
                                img: item.img,
                                capacity: item.capacity,
                                ammo: item.system.magazine ? item.system.magazine.system?.quantity : 0,
                                heat: Math.round(item.system.heat * 100) / 100,
                                isFirearm: item.type == "firearm",
                                swap_ticks: item.getTicksCost('swap_weapon'),
                                swap_clarity: item.getTicksCost('swap_weapon'),
                                reload_ticks: item.getTicksCost('reload'),
                                reload_clarity: item.getClarityCost('reload'),
                                eject_ticks: item.getTicksCost('eject'),
                                eject_clarity: item.getClarityCost('eject'),
                                tooltip: item.getTooltip(),
                                heatLevel: +item.system.heat <= CONFIG.TAC.HEAT_THRESHOLD_LOW ? 'heat-low' : (+item.system.heat <= CONFIG.TAC.HEAT_THRESHOLD_HIGH ? 'heat-medium' : 'heat-high'),
                                //belowComposure: item.type == "firearm" && item.system.heat <= token.actor.system.attributes_social.composure.value,
                                magazine: item.system.magazine,
                                data: item.system,
                                slot: slotKey,
                                macros: macros
                            });
                        }
                    });

                    let usable = token.actor.system.bagSlots.filter(item => item.type === "consumable" && item.system.charges > 0);
                    usable.forEach(item => {
                        item.getSkills().forEach(skill => this.macros.push(skill))
                    });
                    this.macros.forEach(ele => ele.img = ele.img? ele.img : "systems/tac/icons/skills/placeholder.svg");

                    this.macros.forEach(m => {
                        m.canBeUsed = token.actor.system.clarity.value >= m.clarity_cost;
                    });
                }
            }
            
            // Sort the macros. Sheet macros > favorited abilities > equipped items (sorted by typeOrder), then alphabetically (except sheet macros)
            //this.macros.sort((a,b) => (typeOrder.has(a.type) ? typeOrder.get(a.type) : (a.sheetMacro ? -2 : -1)) - (typeOrder.has(b.type) ? typeOrder.get(b.type) : (b.sheetMacro ? -2 : -1)) || ((a.sheetMacro && b.sheetMacro) ? 0 : a.name.localeCompare(b.name) ));

            data.macros = this.macros;
            data.weapons = this.weapons;
        }
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find('.macro.basic').click(ev => {
            let index = event.currentTarget.closest(".macro").dataset.index;
            console.log(this.macros[index])
            if(this.macros) {
                if(!this.macros[index].canBeUsed) return ui.notifications.warn(`Error: not enough Clarity!`);
                this.macros[index].callback(this.tokens[0]);
            }
        });
        html.find('.macro.weapon').click(ev => {
            let index = event.currentTarget.closest(".macro").dataset.index;
            let slot = event.currentTarget.closest(".macro").dataset.slot;

            let weapon = this.weapons.find(w => w.slot === slot);
            console.log(weapon.macros[index])
            if(weapon.macros) {
                if(!weapon.macros[index].canBeUsed) return ui.notifications.warn(`Error: not enough Clarity!`);
                weapon.macros[index].callback(this.tokens[0]);
            }
        });

        html.find('.showNonWielded').click(async ev => {
            let toggle = !game.user.flags?.mta?.tokenHotBar?.showNonWielded;
            let updateData = {
                'flags': {
                    'mta': {
                        tokenHotBar: {showNonWielded: toggle}
                    }
                 }
            };
            await game.user.update(updateData);
            this.render(true);
        });
        html.find('.showEquipment').click(async ev => {
            let toggle = !game.user.flags?.mta?.tokenHotBar?.showEquipment;
            let updateData = {
                'flags': {
                    'mta': {
                        tokenHotBar: {showEquipment: toggle}
                    }
                 }
            };
            await game.user.update(updateData);
            this.render(true);
        });
        html.find('.settings').click(ev => {
            let l = $('.settings-menu')
            $('.settings-menu').toggle();
        });


        html.find('.item-reload').click(ev => this._onItemReload(ev));
        html.find('.item-weaponSwitch').click(ev => this._onWeaponSwitch(ev));

        html.find('.heatBar').mousedown(ev => this._onHeatBarClick(ev));
        
        html.find('.condition-click').click(ev => {
            
            let name = ev.currentTarget.closest(".condition-click").dataset.name;
            console.log("CLICK", name)
            if(name){ 
                if(this.tokens.length === 1){
                    let token = this.tokens[0];
                    if(!token.actor) return;
                    token.actor.lapseCondition(name);
                }
            }
        });
        
      }

      static tokenHotbarInit() {
        return new TokenHotBar();
      }

      async _onItemReload(ev) {
        if(this.tokens.length === 1){
            let token = this.tokens[0];
            if(token.actor){
                const weaponId = ev.currentTarget.closest(".item-reload").dataset.itemId;
                const weapon = token.actor.getEmbeddedDocument("Item", weaponId);
                weapon.reload(ev.target, token);
            }
        }
        
      }

      async _onWeaponSwitch(ev) {
        if(this.tokens.length === 1){
            let token = this.tokens[0];
            if(token.actor){
                const weaponId = ev.currentTarget.closest(".item-weaponSwitch").dataset.itemId;
                const weapon = token.actor.getEmbeddedDocument("Item", weaponId);
                const slot = ev.currentTarget.closest(".item-weaponSwitch").dataset.slot;
                let weaponList = new WeaponSwitchDialogue(token.actor, weapon, token, slot);
                weaponList.render(true);
            }
        }
        
      }

      async _onHeatBarClick(ev) {
        if(this.tokens.length === 1){
            let token = this.tokens[0];
            if(token.actor){
                const weaponId = ev.currentTarget.closest(".heatBar").dataset.itemId;
                const weapon = token.actor.getEmbeddedDocument("Item", weaponId);
                let change = 0;
                switch (event.which) {
                    case 1: // Left mouse button
                        change = -1;
                        break;
                    case 2: // Middle mouse button
                        change = -CONFIG.TAC.HEAT_MAXIMUM;
                        break;
                    case 3: // Right mouse button
                        change = 1;
                        break;
                }

                weapon.adjustWeaponHeat(change);
            }
        }
      }

      
}




