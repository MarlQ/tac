export class TokenDocumentTac extends TokenDocument {

 
  

 /*  _onUpdateBaseActor(update={}) {
    super._onUpdateBaseActor(update={});
    if(this.combatant) this.combatant.rollInitiative();
  }

  _onUpdateTokenActor(data, options, userId) {
    super._onUpdateTokenActor(data, options, userId);
    if(this.combatant) this.combatant.rollInitiative();
  } */

   /**
   * A helper method to retrieve the underlying data behind one of the Token's attribute bars
   * @param {string} barName        The named bar to retrieve the attribute for
   * @param {string} alternative    An alternative attribute path to get instead of the default one
   * @returns {object|null}         The attribute displayed on the Token bar, if any
   */
   getBarAttribute(barName, {alternative}={}) {
    const data = super.getBarAttribute(barName, {alternative});
    if(data.attribute === "health" || data.attribute === "clarity") {
      data.editable = true;
    }
    return data;
  }

  getSurroundingTokens(distance) {
    return this._object.scene.tokens.filter(t => canvas.grid.measureDistance(this, t) <= distance && t.id != this.id);
  }

  
}
