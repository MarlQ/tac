
// Draw a random entry from a weighted table,
// where the [0] entries contain the entries
// and [1] contain the weights
export function randomEntry (table) {
    let weightSum = table.reduce((sum, cur) => sum + cur[1], 0);
    let randomNum = Math.random() * weightSum;
    let counter = 0;
    let result;

    table.some(ele => {
      counter += ele[1];
      if(randomNum < counter) {
        result = ele[0];
        return true;
      }
    });
    return result;
  }