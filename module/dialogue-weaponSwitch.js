import { createShortActionMessage } from "./chat.js";

export class WeaponSwitchDialogue extends Application {
  constructor(actor, originalWeapon, actor_token, slot, ...args) {
    super(...args);
    this._actor = actor;
    this._originalWeapon = originalWeapon;
    this.actor_token = actor_token;
    this.slot = slot;
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue"],
  	  template: "systems/tac/templates/dialogue-weaponSwitch.html",
      width: 200,
      height: 400
    });
  }
  
  getData() {
    const data = super.getData();
    
    data.weaponList = this._actor.items.filter(element => (element.type === "melee" || element.type === "firearm") && !this._actor.isEquipped(element));
    console.log(data.weaponList);
    data.weaponEquipped = this._originalWeapon ? true : false;
    return data;
  }
  
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.selectWeapon').click(ev => this._onSelectWeapon(ev));
    html.find('.unequipWeapon').click(ev => {
      //if(this._originalWeapon) this._originalWeapon.update({"system.equipped": false});
      this._actor.unequip(this._originalWeapon);
      createShortActionMessage("Drops their weapon", this._actor);
      this.close();
    });
    
  }

  async _onSelectWeapon(event){  
    const itemId = event.currentTarget.closest(".selectWeapon").dataset.itemId;
    const weapon = this._actor.items.get(itemId);
    
    this._actor.equip(weapon, this.slot);

    this._actor.applyActionCost( CONFIG.TAC.BASIC_ACTIONS.swap_weapon.ticks_cost, CONFIG.TAC.BASIC_ACTIONS.swap_weapon.clarity_cost, this.actor_token);
    
    createShortActionMessage("Swaps weapon", this._actor);

    this.close();
  }
}