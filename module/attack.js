import { createShortActionMessage } from "./chat.js";
import { roll } from "./roll.js";
import { randomEntry } from "./helpers.js";
export class Attack{

  static _randomChance(chance){
    return Math.random() <= chance;
  }
  
  /* static _diceRoll(hitDice, targetNumber){
    let r = new Roll("(@dice)d10cs>=(@diff)", {
      dice: hitDice,
      diff: targetNumber
    }).roll({async: false});
    return r.total;
  } */


/*   let bulletsprayTemplate = canvas.templates.get(args[0].templateId)
let tokenD = canvas.tokens.get(args[0].tokenId);

let templatePosition = bulletsprayTemplate.position;

bulletsprayTemplate.delete();

let sequence = new Sequence()
    .effect()


        .file("modules/JB2A_DnD5e/Library/Generic/Weapon_Attacks/Ranged/Bullet_01_Regular_Orange_30ft_1600x400.webm")
        .atLocation(tokenD)
        .stretchTo(templatePosition)
        .JB2A()


    .wait(1)
    .effect()
        .file("animations/Scifi/gunMuzzleFlash.webm")
        .atLocation(templatePosition)
        .JB2A()
        .scale(1.0)
    .play() */

  static effects = {
    gunshot: "jb2a.bullet.01.orange",
    crushing:"jb2a.melee_generic.slash.01.orange.0",
    slashing: "jb2a.melee_generic.slash.01.orange.0",
    piercing: "jb2a.melee_generic.slash.01.orange.0"
  }

/*   async function fxAttack() {
    await game.lancer.prepareItemMacro("cADEbVHBP97uCtui", "z3cEGbJ9np3uj4Tk");
    let target = Array.from(game.user.targets)[0];
  
    let sequence = new Sequence()
  
    for(let target of Array.from(game.user.targets)){
      sequence.sound()
          .file("modules/lancer-weapon-fx/soundfx/AR_Fire.wav")
          .volume(0.5)
        .effect()
          .file("jb2a.bullet.01.orange") 
          .atLocation(canvas.tokens.controlled[0])
          .stretchTo(target)
          .repeats(4, 100)
          .randomOffset(0.4)
          .waitUntilFinished()
    }
  
    sequence.play();
  } */

  static attackAnimation(attacks=[], projectiles=1) {
    let sequence = new Sequence();
    console.log("PROJ", projectiles)

    for(let i = 0; i < attacks.length; i += projectiles) {
      const attack = attacks[i];
      let effect;

      for(let p = 0; p < projectiles; p++) {
        const att = attacks[i + p];
        effect = sequence.effect()
        .file(Attack.effects[att.type])
        .atLocation(att.from)
        .stretchTo(att.to)
  
        if(att.miss) {
          effect.missed();
          effect.tint(att.miss ? "#FF0000" : "");
          effect.filter("Blur", { blurX: 5, blurY: 5 });
          effect.opacity(0.5);
        }
      }
      sequence.wait(800);
      
    }
    sequence.play();
  }
  
  static applyArmor({damage=0, armor=0, natural_armor=0, natural_armor_crushing=0, penetration=0, projectiles=1, successes=1, coverage=100, durability=100, enable_overpen=true}) {
    successes = Math.min(successes, 5);

    console.log("Applying armor...", {damage, armor, penetration, projectiles, successes, coverage, durability, enable_overpen})

    let flavor_text = "";

    // Bypass part of the armor
    if(Math.random() > coverage * 0.01) {
      armor = Math.floor(CONFIG.TAC.ARMOR_MULT_BYPASS * armor);
      flavor_text += " bypassing armour";
    } 

    // Apply armor durability
    if(durability < CONFIG.TAC.ARMOUR_DURABILITY_BAD) armor = Math.floor(armor * CONFIG.TAC.ARMOUR_SCALING_BAD)
    else if(durability < CONFIG.TAC.ARMOUR_DURABILITY_OK) armor = Math.floor(armor * CONFIG.TAC.ARMOUR_SCALING_OK)

    // Apply normal armor
    let damage_1_normal = Math.max(0, damage - Math.max(armor - penetration));
    let damage_1_crushing = Math.max(0, penetration - armor);

    let damage_2_normal = Math.max(0, damage_1_normal - damage_1_crushing);
    let damage_2_crushing = Math.max(0, damage_1_normal - damage_2_normal);

    let remaining_pen = Math.max(0, armor - penetration);

    // Add natural armor

    let damage_3_normal = Math.max(0, damage_2_normal - Math.max(0, natural_armor - remaining_pen));
    let damage_final_crushing = Math.max(0, damage_2_crushing - Math.max(0, natural_armor_crushing - remaining_pen));

    let overpen_reduction = enable_overpen ? Math.floor(Math.max(0,remaining_pen-natural_armor)/2) : 0;

    let damage_final_normal =  Math.max(Math.ceil(damage_3_normal/2), damage_3_normal - overpen_reduction);
    console.log("Damage after armor", damage_final_normal, damage_final_crushing)

    return {
      crushing_damage: damage_final_crushing,
      normal_damage: damage_final_normal,
      flavor_text
    }
  }

  /**
   * Tests whether a point is within a degree deviation of a
   * ray going from origin to target.
   * @param {*} origin 
   * @param {*} target 
   * @param {*} point 
   * @param {number} degrees
   * @returns 
   */
  static isWithinDeviation(origin, target, point, degrees=45) {
    // Calculate vectors
    const vector1 = { x: target.x - origin.x, y: target.y - origin.y }; // Vector along the ray
    const vector2 = { x: point.x - origin.x, y: point.y - origin.y }; // Vector from (x, y) to (x3, y3)

    // Calculate dot product
    const dotProduct = vector1.x * vector2.x + vector1.y * vector2.y;

    // Calculate magnitudes
    const magnitude1 = Math.sqrt(vector1.x ** 2 + vector1.y ** 2);
    const magnitude2 = Math.sqrt(vector2.x ** 2 + vector2.y ** 2);

    // Calculate angle in radians
    let angle = Math.acos(dotProduct / (magnitude1 * magnitude2));

    // Convert angle to degrees
    angle = (angle * 180) / Math.PI;

    // Check if angle is within ±45 degrees
    return angle <= degrees;
}

  /**
   * Determines bystander hits for deviating shots (successes <= 0)
   * @param {number} successes_hit 
   * @param {*} attackerTokenDoc 
   * @param {*} targetTokenDoc 
   * @returns 
   */
  static _determineMissLocation({successes_hit, aimLocation, attackerTokenDoc, targetTokenDoc, weaponType=""}) {
    const metersDeviation = Math.min(5, Math.floor(Math.abs(successes_hit) / 2)*2 + 2);
    console.log("DETERMINE MISS", successes_hit, metersDeviation)

    // Filter targets
     // TODO: getSurroundingTokens should be a static function
    let targets = targetTokenDoc.getSurroundingTokens(metersDeviation)
      .filter(t => this.isWithinDeviation(attackerTokenDoc, targetTokenDoc, t))
      .filter(t => t.id !== attackerTokenDoc.id);
      
    console.log("Targets", targets)
    console.log("Wat", targetTokenDoc.scene.tokens.map(t =>( {name: t.name, pos: canvas.grid.measureDistance(targetTokenDoc.document, t)})))

    const targetsNum = targets.length;
    if(targetsNum <= 0) return {name: "Miss", damage: 0};

    const missChance = targetsNum < 5 ? (5 - targetsNum) / 5 : 0;
    console.log("Miss chance", missChance, targetsNum);

    if(Math.random() <= missChance) return {name: "Miss", damage: 0};

    let randomTarget = targets[Math.floor(Math.random() * targetsNum)];
    
    const actorDisposition = attackerTokenDoc.disposition;
    let friendlyHitRerollChance = 0;
    if(weaponType === "shotgun") friendlyHitRerollChance += attackerTokenDoc.actor?.getTalentStacks("careful_hunter") * CONFIG.TAC.TALENT_TREES.shotguns.nodes.careful_hunter.rerollChance;

    console.log("Rantarget", randomTarget, friendlyHitRerollChance, attackerTokenDoc, randomTarget.disposition)
    if(friendlyHitRerollChance > 0 && randomTarget.disposition === actorDisposition && Math.random() <= friendlyHitRerollChance) {
      console.log("Rantarget REROLLED", randomTarget)
      return {name: "Miss", damage: 0};
    }

    // Determine hit location
    // TODO: target actor (once locations are actor-specific)
    const min = 1, max = 5;
    const randSuccesses = Math.floor(Math.random() * (max - min + 1)) + min;
    const hitLoc = this._determineHitLocation(randSuccesses, aimLocation);
    hitLoc.targetUuid = randomTarget.uuid
    hitLoc.targetName = randomTarget.name;
    hitLoc.targetPos = randomTarget.object.center;
    console.log("HIT LOC", hitLoc);

    return hitLoc;
  }
  
  static _determineHitLocation(successes_hit, aimLocation, targetActor){
    if(successes_hit < 1) return {name: "Miss", damage: 0};

    let hitLoc;
    let hit_chance = CONFIG.TAC.HIT_CHANCE_BASE;

    let s_max = 0;
    // Find locations equal and below successes, and find maximum successes possible
    let possibleLocations = CONFIG.TAC.hitLocations.filter( e => {
      if(e.successes <= successes_hit) {
        if(e.successes > s_max) s_max = e.successes
        return true;
      }
      return false;
    })

    // Aiming: Only works if successes are higher or equal, and increases hit chancs if successes are higher
    if(aimLocation && aimLocation !== "Any") {
      hitLoc = possibleLocations.find(e => e.name === aimLocation);
    }

    if(!hitLoc) { // Not aiming: find possible hit location
      // Filter out all locations that are worse than successes
      possibleLocations = possibleLocations.filter(e => e.successes === s_max );

      if(possibleLocations.length === 0) hitLoc = {name: "Miss", damage: 0};
      else hitLoc = possibleLocations[ Math.floor(Math.random()*possibleLocations.length) ];
    } 

    // Adjust hit chance
    let loc_successes;
    for(const loc of possibleLocations) {
      if(loc.name === hitLoc.name) {
        loc_successes = loc.successes;
        break;
      }
    }
    hit_chance = Math.min(hit_chance + (successes_hit - loc_successes) * CONFIG.TAC.AMING_CHANCE_INCREASE_PER_SUCCESS, 1);

    if(Math.random() <= hit_chance) { // Hit intended location
      return hitLoc;
    }
    
    // Find another hit location based on weights
    possibleLocations = CONFIG.TAC.hitLocations.filter( e => e.successes !== s_max);
    const max_weight = Math.max(...possibleLocations.map(loc => Math.abs(successes_hit - loc.successes)));

    let location_weights = possibleLocations.map(loc => [loc, Math.max(1, (max_weight - Math.abs(successes_hit - loc.successes)) + 1)]);
    hitLoc = randomEntry(location_weights);
    if(!hitLoc) hitLoc = {name: "ERROR", damage: 0};

    return hitLoc;
  }
  
  static _hitCover(coverChance, hitSuccesses){
    if(coverChance === 100) return true;
    let coverChance_adjusted = Math.max(0,coverChance-hitSuccesses*5);
    let ran = Math.floor((Math.random() * 100) + 1);
    //console.log("RAN: " + ran);
    //console.log("COVER CHANCE: " + coverChance_adjusted + " MADNESS " + coverChance);
    return ran <= coverChance_adjusted;
  }
  /**
   
  CONFIG.Attack.testWeapon({
    num_attacks:1,
    weapon_damage:1,
    penetration:0,
    recoil:0,
    deviation:0,
    projectiles:1,
    impact:0,
    enable_overpenetration:true,
    weapon_increaseHeat: true
  });


   */

  static async testWeapon({
    num_attacks=1,
    weapon_damage=1,
    penetration=0,
    recoil=0,
    deviation=0,
    projectiles=1,
    impact=0,
    optimalDistance=0,
    enable_overpenetration=false,
    weapon_increaseHeat=false,
    melee=false
  }) {
    let dicepools = [1,2,3,4,5,6,7,8,9,10];
    let armor_values = [0,1,2,3,4,5];
    let distances = [-1,0,1,2,3,4];
    if(melee) {distances = [0,1,2,3,4,5]; enable_overpenetration=false; weapon_increaseHeat=false;}
    let iterations = 100;
    let damageSum = 0;

    let armorSums = {0:0,1:0,2:0,3:0,4:0,5:0};
    let distanceSums = [];

    // TODO: Fix async with better for loops
    distances.forEach(async distance => {
      let distanceSum = 0;
      armor_values.forEach(async armorValue => {

        let avg = 0;
        dicepools.forEach(async dicePool => {
          for(let i = 0; i < iterations; i++) {
            let dp = dicePool + impact;
            let target_dp = 0;
            let dist_adj = distance;

            if(dist_adj === -1) { // Melee combat
              dist_adj = 0;
              dp -= 2;
              target_dp = Math.ceil(dicePool/2);
            }
            else if(melee && dist_adj > 2) {
              dist_adj -= 3;
              target_dp = dicePool;
            }

            dp -= Math.abs(optimalDistance - dist_adj) * 2 ;
            
            let targetNum = 6;
            if(dp < 1) {
              targetNum = Math.min(10, targetNum + Math.abs(1 - dp));
              dp = 1;
            }
            let {
              successes_message, 
              confirmedHits, 
              totalDamage, 
              clarityDamage,
              weapon_heat,
              weapon_ammo
            } = await Attack.calculateAttack({
              attackerActor:{
                data: {
                  data: {
                    attributes_physical: {
                      strength: {value: Math.ceil(dicePool/2)}
                    },
                    attributes_social: {
                      composure: {value: Math.ceil(dicePool/2)}
                    }
                  }
                }
              },
              weapon: {
                type: "firearm",
                data: {
                  data: {
                    penetration: penetration,
                    recoil: recoil,
                    heat: 0,
                    deviation: deviation,
                    projectiles: projectiles,
                    impact: impact
                  }
                }
              },
              targetActor: {
              },
              num_attacks: num_attacks,
              player_dicePool: dp,
              player_targetNumber: targetNum,
              weapon_damage: melee ? weapon_damage + Math.ceil(dicePool/2) : weapon_damage,
              weapon_increaseHeat:weapon_increaseHeat,
              damage_type: "gunshot",
              target_dicePool: target_dp,
              target_targetNumber: 6,
              target_distance: dist_adj,
              enable_overpenetration: enable_overpenetration,
              armor_override: {armor: armorValue, coverage: 90, durability: 100}
            });
            if(!totalDamage) totalDamage = 0;
            avg += totalDamage;
          }
        });
        avg /= iterations * dicepools.length;
        
        console.log("Distance ", distance, " Armor: ", armorValue, " Damage: ", avg);
        damageSum += avg;
        distanceSum += avg;
        armorSums[armorValue] += avg;
      });
      distanceSum /= armor_values.length;
      distanceSums.push(distanceSum);
      
    });

    Object.keys(armorSums).forEach( index => {
      console.log("Armor ", index, " Average Damage ", armorSums[index] / distances.length);
    })

    distanceSums.forEach((distanceSum, index)  => {
      console.log("Distance ", index, " Average Damage: ", distanceSum);
    })

    let averageDamage = damageSum / (distances.length * armor_values.length);
    console.log("Average damage: ", averageDamage);

    
  }

  static async calculateAttack({
    attackerActor,
    weapon,
    targetActor,
    num_attacks=1,
    player_dicePool=1,
    player_targetNumber=6,
    weapon_damage,
    damage_type,
    target_dicePool=0,
    target_targetNumber=6,
    target_distance=0,
    target_coverChance=0,
    target_coverStrength=0,
    all_successes = false,
    target_aimLocation="Any",
    weapon_consumeAmmo=false,
    weapon_increaseHeat=false,
    enable_overpenetration=false,
    armor_override,
    targetToken,
    attackerToken,
    weaponType=""
  }={}) {
    let player_strength = attackerActor.system.attributes_physical.strength.value;
    let player_composure = attackerActor.system.attributes_social.composure.value;
    weapon_damage = weapon_damage ?? weapon.system.damage;
    let weapon_penetration = weapon.system.penetration ?? 0;
    let weapon_recoil = weapon.system.recoil ?? 0;
    let weapon_heat = weapon.system.heat ?? 0;
    let weapon_deviation = weapon.system.deviation ?? 0;
    let weapon_projectiles = weapon.type === "firearm" ? weapon.system.projectiles : 1;
    let weapon_impact = weapon.system.impact ?? 0;
    let weapon_ammo = weapon.type === "firearm" && weapon.system?.magazine ? weapon.system.magazine.system.quantity - 1: 0;
    let weapon_quality = weapon.system.quality ?? 0;

    //#1 Determine successes
    let successes_attacker = all_successes ? player_dicePool : roll(player_dicePool, player_targetNumber).total;
    let successes_target = target_dicePool > 0 ? roll(target_dicePool, target_targetNumber).total : 0;
    let successes_total_mod = successes_attacker - successes_target;
    let successes_total = successes_total_mod;
    //console.log("First Hit: " + successes_hit + " successes (" + successes_hit_init + " - " + successes_dodge + ")");
    
    if(successes_total_mod >= 1){
      // Subtract weapon impact from successes (originally added as dice in dialogue-attack)
      // This is only needed if the attack actually hit
      successes_total_mod = Math.max(1, successes_total_mod-weapon_impact);
    } 

    //Limit successes
    successes_total_mod = Math.clamped(successes_total_mod, 0, CONFIG.TAC.HIT_SUCCESS_LIMIT);

    //#3 Determine hit location for all attacks and projectiles fired
    let confirmedHits = [];
    let recoil_adj = 0;
    // TODO: TAC.DAMAGE_LOSS_ABOVE_OPTIMAL_RANGE
    let jam = false;
    for(let n = 0; n < num_attacks; n++){ let firstShot = (n === 0);

      // Consume Ammo
      if(!firstShot && weapon_consumeAmmo){
        if(weapon_ammo < 1) break;
        weapon_ammo--;
      }

      // Adjust recoil downgrade chances
      let heat_adj    = Math.max(0, weapon_heat - CONFIG.TAC.HEAT_THRESHOLD_LOW);
      let penalties   = (recoil_adj*CONFIG.TAC.missMult_recoil)*(target_distance+1);
      //console.log("Distance", target_distance, "Shot", n, "Miss chance", penalties, " Heat: ", heat_adj*CONFIG.TAC.missMult_heat*(target_distance+1), " Recoil: ", recoil_adj*CONFIG.TAC.missMult_recoil*(target_distance+1), " Deviation ", weapon_deviation*CONFIG.TAC.missMult_deviation*(target_distance+1) )

      let reduce_definite = Math.floor(penalties / 100);
      let reduce_chance = penalties - reduce_definite;

      successes_total_mod -= reduce_definite;

      // Downgrade hit
      if(reduce_chance > 0 && Math.random()<= reduce_chance*0.01) {
        successes_total_mod--;
      }

      let hitCover = this._hitCover(target_coverChance, successes_total_mod); // TODO: ?
      
      let succ_adj_projectile = successes_total_mod;                
      for(let i = 0; i < weapon_projectiles; i++){

        // Projectile deviation is handled separately (more impact, the more projectiles)
        let dev_chance = (heat_adj*CONFIG.TAC.missMult_heat + weapon_deviation*CONFIG.TAC.missMult_deviation)*(target_distance+1);
        let dev_reduce_definite = Math.floor(dev_chance / 100);
        let dev_reduce_chance = dev_chance - dev_reduce_definite;

        succ_adj_projectile -= dev_reduce_definite;

        if(dev_reduce_chance > 0 && Math.random() <= dev_reduce_chance*0.01) {
          succ_adj_projectile--;
        }

        let hitLoc = succ_adj_projectile <= 0 ?
          this._determineMissLocation({
            successes_hit: succ_adj_projectile, 
            aimLocation: target_aimLocation, 
            attackerTokenDoc: attackerToken.document, 
            targetTokenDoc: targetToken,
            weaponType
          })
          : this._determineHitLocation(succ_adj_projectile, target_aimLocation);

        confirmedHits.push({...hitLoc, hitCover: hitCover});
      }
      let str_adj = Math.max(0, player_strength - n);

      // Increase recoil and heat
      recoil_adj += Math.max(0, weapon_recoil - str_adj);
      if(weapon_increaseHeat){ weapon_heat = Math.min(CONFIG.TAC.HEAT_MAXIMUM, weapon_heat+CONFIG.TAC.increasePerShot_heat); }

      // Weapon jamming
      if(weapon.type === "firearm") {
        const jamChance = (CONFIG.TAC.JAMMING_CHANCE_BASE + CONFIG.TAC.JAMMING_CHANCE_PER_HEAT * weapon_heat) * Math.pow(1/2, 1/3 * weapon_quality);
        console.log("JAM CHANCE", jamChance)
        if(Math.random() <= jamChance) {
          console.error("JAM")
          jam = true;
          break;
        }
      }
    }


    //#4 Analyse hits and determine total damage
    let totalDamage = 0;
    let totalClarityDamage = 0;
    
    for(const hitLoc of confirmedHits) {
      console.log("CALCULATE DAM", hitLoc)
      let damage = 0;
      let target_armour = 0;
      hitLoc.damageType = damage_type;
      hitLoc.detail = hitLoc.name.charAt(0).toUpperCase() + hitLoc.name.slice(1);
      
      if(hitLoc.name === "Recoil Miss" || hitLoc.name === "Heat Miss" || hitLoc.name === "Miss"){
        hitLoc.detail = hitLoc.name;
        hitLoc.name = "miss";
        continue;
      }

      let hitLocTargetActor = targetActor;

      if(hitLoc.targetUuid) {
        const hitLocTargetTokenDoc = await fromUuid(hitLoc.targetUuid);
        hitLocTargetActor = hitLocTargetTokenDoc.actor;
      }
      console.log("PARSED", hitLocTargetActor)


      // Search for armour slot
      let hitSlot;
      if(hitLocTargetActor.system?.equipmentSlots) {
        hitSlot = hitLocTargetActor.system.equipmentSlots[ CONFIG.TAC.ARMOR_SLOTS[hitLoc.name] ];
        if(hitSlot?.item) {
          target_armour = hitSlot.item.system[ hitLoc.damageType ];
        }
      }
      if(hitLocTargetActor.system.other_traits.armor_modifier.value) target_armour = Math.max(0, target_armour - hitLocTargetActor.system.other_traits.armor_modifier.value);

      if(armor_override) target_armour = armor_override.armor;

      //Hit cover
      if(hitLoc.hitCover) {
        /* if(weapon_penetration >= target_coverStrength){
          //Hit through cover  
          //TODO
          damage = Math.max(0, Math.floor(hitLoc.damage * (Math.max(1,weapon_damage-weapon_penetration) - Math.abs(target_armour - Math.max(0,weapon_penetration-target_coverStrength)))));
          hitLoc.detail += " through cover"; //TODO:
          hitLoc.damage = damage;
          totalDamage += damage;
          return; 
        }
        hitLoc.detail = "Cover";
        hitLoc.name = "miss";
        hitLoc.damage = 0;
        return; */
        target_armour += target_coverStrength; // FIXME: E.g. coverage...
        hitLoc.detail += " through cover";
      }      


      const coverage = armor_override ? armor_override.coverage : hitSlot.item?.system?.coverage;
      const durability = armor_override ? armor_override.durability : hitSlot.item?.system?.durability;
      const natural_armor = hitLocTargetActor.system.natural_armor[hitLoc.name] ?? 0;

      const { 
        crushing_damage,
        normal_damage,
        flavor_text
      } = Attack.applyArmor(
        {
          damage: weapon_damage, 
          armor: target_armour, 
          natural_armor: natural_armor[hitLoc.damageType],
          natural_armor_crushing: natural_armor.crushing,
          penetration: weapon_penetration, 
          projectiles: weapon_projectiles, 
          successes: successes_total, coverage, durability, 
          enable_overpen: enable_overpenetration
        });

        hitLoc.detail += flavor_text;

        // TODO: Add up damage

        hitLoc.damage = Math.ceil(normal_damage * CONFIG.TAC.DAMAGE_TYPES[hitLoc.damageType].health.damageMod)
          + Math.ceil(crushing_damage * CONFIG.TAC.DAMAGE_TYPES.crushing.health.damageMod);

        hitLoc.clarityDamage = Math.ceil(normal_damage * CONFIG.TAC.DAMAGE_TYPES[hitLoc.damageType].clarity.damageMod)
        + Math.ceil(crushing_damage * CONFIG.TAC.DAMAGE_TYPES.crushing.clarity.damageMod);

      totalDamage += hitLoc.damage;
      totalClarityDamage += hitLoc.clarityDamage;

    }

    //console.log("Total damage: " + totalDamage + " " + clarityDamage);


    // Crit wounds
    const damagingHits = confirmedHits.filter(attack => attack.name !== "Miss" && attack.damage > 0);
    const critChance = player_dicePool * 2; // TODO:
    let wound;
    
    if(damagingHits.length && critChance > Math.random() * 100) {
      
      const critHit = damagingHits[Math.floor(Math.random()*damagingHits.length)];

      const injuryTable = CONFIG.TAC.CRIT_INJURIES[critHit.name];
      const possibleInjuries = injuryTable[critHit.damageType] ? {...injuryTable.all, ...injuryTable[critHit.damageType]} : injuryTable.all;

      wound = randomEntry(Object.entries(possibleInjuries));
    }

    return {
      successes_message: successes_total, 
      confirmedHits, 
      totalDamage, 
      totalClarityDamage,
      weapon_heat,
      weapon_ammo,
      jam,
      wound
    };    
  }

  
  static async executeAttack({
    num_attacks=1,
    player_dicePool, 
    player_targetNumber=6,
    target_dicePool, 
    target_targetNumber=6, 
    target_coverChance=0, 
    target_coverStrength=0,
    target_distance=0,
    target_aimLocation="Any",
    modifiers,
    weapon_damage=1, 
    weapon_increaseHeat=false,
    weapon,
    target: targetActor,
    token: targetToken,
    attacker: attackerActor,
    actor_token: attackerToken,
    weapon_consumeAmmo=false,
    weapon_name="",
    attack_name="",
    damage_type="",
    target_name="",
    speaker,
    all_successes=false,
    enable_overpenetration=false,
    weaponType=""
  }={}) {
    console.log("-----------------------");
    console.log(arguments);
    
    /**
    #1: Determine if first attack hit (player vs target)
    #2: Adjust body part hit chances based on successes
    #3 Determine hit location
    #4 Simulate additional bullets fired
    **/

    let {
      successes_message, 
      confirmedHits, 
      totalDamage, 
      totalClarityDamage,
      weapon_heat,
      weapon_ammo,
      jam,
      wound
    } = await Attack.calculateAttack({
      attackerActor,
      weapon,
      targetActor,
      num_attacks,
      player_dicePool,
      player_targetNumber,
      weapon_damage,
      damage_type,
      target_dicePool,
      target_targetNumber,
      target_distance,
      target_coverChance,
      target_coverStrength,
      all_successes,
      target_aimLocation,
      weapon_consumeAmmo,
      weapon_increaseHeat,
      enable_overpenetration,
      targetToken,
      attackerToken,
      weaponType
    });
    
    // Update the weapon data 
    if(weapon && typeof weapon.update === 'function') {
      let updateData = {};
      if(weapon_increaseHeat) updateData['system.heat'] = weapon_heat;
      if(weapon_consumeAmmo && weapon.system?.magazine) {
        if(confirmedHits) updateData['system.magazine.system.quantity']  = weapon_ammo;
        else updateData['system.magazine.system.quantity'] = weapon.system.magazine.system.quantity - num_attacks;
      }
      if(jam) updateData['system.jammed'] = true;

      weapon.update(updateData);
    }
    console.log("TARGET ", targetActor)

    let damageEffects = weapon?.getEffectsOfType && totalDamage > 0 ? weapon.getEffectsOfType('condition_on_damage').map(c => ({conditionID: c.condition})) : [];
    if(wound) damageEffects.push({conditionID: wound});

    if(attack_name === "crippling_shot" && totalDamage > 0) {
      damageEffects.push({conditionID: 'crippled', options:{severity: 1}});
    }

    // Apply damage to other character
    if(confirmedHits && confirmedHits.length > 0) {
      let target_actorId = targetToken.document.actorLink ? targetActor.id : undefined;
      let target_tokenId = targetToken.document.actorLink ? undefined : targetToken.id;

      if(game.user.isGM) {
        handleAttackRequest({type: 'attack', target_tokenId: target_tokenId, target_actorId: target_actorId, clarityDamage: totalClarityDamage, confirmedHits: confirmedHits, damageEffects: damageEffects, dicePool: player_dicePool})
      }
      else {
        game.socket.emit('system.tac', {type: 'attack', target_tokenId: target_tokenId, target_actorId: target_actorId, clarityDamage: totalClarityDamage, confirmedHits: confirmedHits, damageEffects: damageEffects, dicePool: player_dicePool});
      }
    }
    

    /* new Promise(resolve => {
      // This is the acknowledgement callback
      const ackCb = response => {
        console.error("RECEIVED ", response)
        resolve(response);
      };
      console.log("Socket EMIT ")
      game.socket.emit('tac.damageInflict', {target: target, confirmedHits: confirmedHits}, ackCb);
      //socket.emit('module.my-module', arguments, ackCb);
    }); */

    /* new Promise(resolve => {
      socket.emit(eventName, request, response => {
        doSomethingWithResponse(response); // This is the acknowledgement function
        resolve(response); // We can resolve the entire operation once acknowledged
      });
    });
 */
    attack_name = game.i18n.localize('TAC.ABILITIES.' + attack_name);
    console.log("OKOKOKOK", confirmedHits, weapon, weapon.type === "firearm", weapon.system.projectiles);
    let weapon_projectiles = weapon.type === "firearm" ? weapon.system.projectiles : 1;
    //console.log("asdgf",actor_token, token)
    Attack.attackAnimation(confirmedHits.map(hit => ({
      from: attackerToken, 
      to: hit.targetUuid ? hit.targetPos : targetToken, // TODO:
      type: damage_type,
      miss: hit.name === "miss"
    })), weapon_projectiles);

    //attackerToken, targetToken, num_attacks, damage_type

    this._createAttackMessage({weapon_name, attack_name, target_name, target_aimLocation,modifiers, speaker, successes_message, confirmedHits, totalDamage, clarityDamage: totalClarityDamage, damage_type, target: targetActor, token: targetToken, jam, damageEffects});
    
  }
  
  static async _createAttackMessage({weapon_name="", attack_name="", target_name="",target_aimLocation="", modifiers, speaker, successes_message, confirmedHits, totalDamage, clarityDamage, damage_type, target, token, jam=false, damageEffects}={}){
    
    let result_roll = (successes_message<1) ? "Total Miss" : (successes_message>1) ? successes_message + " successes" : successes_message + " success";
    //let result_confirmedHits = shots ? shots.reduce((acc, cur, idx) => acc + "\n Shot " + idx + ": " + cur.name + " (" + cur.damage + "dmg.)") : "";
    
    console.log(result_roll);
    console.trace(confirmedHits);
    console.log("Speaker: "+ speaker);
    console.log("Token",token)
    console.log("Target",target)
    let aimLocation_string = "";
    if(target_aimLocation != "Any") aimLocation_string = " (" + target_aimLocation + ")";
    
    
     // Basic template rendering data
    const templateData = {
      data: {
        result_roll: result_roll, 
        result_attacks: confirmedHits, 
        result_health: totalDamage, 
        result_clarity: clarityDamage, 
        modifiers: modifiers,
        actorId: token.document.actorLink ? target.id : undefined,
        tokenId: token.document.actorLink ? undefined : token.id,
        damageType: damage_type,
        damageEffects: damageEffects.map(e => {return {name: game.i18n.localize('TAC.CONDITIONS.' + e.conditionID), img: "systems/tac/icons/conditions/" + e.conditionID + ".svg"}})
      }
    };

    // Render the chat card template
    const template = `systems/tac/templates/chat/attack-card.html`;
    const html = await renderTemplate(template, templateData);

    // Basic chat message data
    const chatData = {
      user: game.user.id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: html,
      speaker: ChatMessage.getSpeaker({actor: speaker}),
      flavor: weapon_name + " - " + attack_name + " targeting " + target_name + aimLocation_string
    };

    // Toggle default roll mode
    let rollMode = game.settings.get("core", "rollMode");
    if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
    if ( rollMode === "blindroll" ) chatData["blind"] = true;

    // Create the chat message
    const cm = ChatMessage.create(chatData);

    if(jam) createShortActionMessage("Weapon jams", speaker);
    return cm;
  }
  
  
  
}
CONFIG.Attack = Attack;
export async function handleAttackRequest(request) {
  let target;
  console.log("SHOOT ", request)
  if(request.target_actorId) target = game.actors.get(request.target_actorId);
  else target = game.actors.tokens[request.target_tokenId];

  // TODO: Ask for confirmation (dialogue)
 

   /* await target.applyEffect({name: 'attack_clarity', damage: request.clarityDamage,damageType: "exhaustion"}); */ // FIXME: This is not quite right ?
  const damagingHits = request.confirmedHits.filter(attack => attack.name !== "Miss" && attack.damage > 0);

  for(const attack of damagingHits) { // TODO: Apply damage only once
    console.log("ATT", attack)
    let hitLocTargetActor = target;
    if(attack.targetUuid) {
      const hitLocTargetTokenDoc = await fromUuid(attack.targetUuid);
      console.log("PARED", hitLocTargetTokenDoc)
      hitLocTargetActor = hitLocTargetTokenDoc.actor;
    }
    await hitLocTargetActor.applyDamage(attack.damage, "health", attack.damageType);
    /* await target.applyEffect({name: 'attack', damage: attack.damage, hitLocation: attack.name, damageType: attack.damageType});  */
  }

  // TODO:
  await target.applyDamage(request.clarityDamage, "clarity", "exhaustion");

  if(request.damageEffects) {
    await target.addMultipleConditions(request.damageEffects);
  }
  
}