
// Dice pool, with target number 6, and 10-doubles (optional 10-again)
export function roll(dice=1, target=6, explode) {
  let rollString = explode ? `${dice}d10x${explode}cs>=${target}` : `${dice}d10cs>=${target}`;

  let result = new Roll(rollString).roll({async: false});
  console.log(result)

  // 10s counts as double
  result._total += result.terms[0].results.reduce((prev, cur) => cur.result >= 10 ? prev + 1 : prev, 0);
  return result;
}

export function rollWithDifficulty({dice=1, target=6, explode, difficulty=1}) {
  const result = roll(dice, target, explode);
  result._total = Math.clamped(result._total - difficulty, -5, 5);
  return result;
}
