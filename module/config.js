/**
 * WARNING: Remember, if you change anything in this file, it will get overwritten when the system updates. 
 * So make a backup of any changes, and re-apply them afterwards.
 */

// Namespace Configuration Values
export const TAC = {};

import { CONDITIONS, WEAPON_TRAITS_GOOD, WEAPON_TRAITS_BAD, DRUG_CONDITIONS, CRIT_INJURIES } from "./config/effects.js";
import { TALENT_TREES } from "./config/talents.js";

TAC.conditions = CONDITIONS;
TAC.CONDITIONS = CONDITIONS;
TAC.WEAPON_TRAITS_GOOD = WEAPON_TRAITS_GOOD;
TAC.WEAPON_TRAITS_BAD = WEAPON_TRAITS_BAD;
TAC.DRUG_CONDITIONS = DRUG_CONDITIONS;
TAC.CRIT_INJURIES = CRIT_INJURIES;

TAC.TALENT_TREES = TALENT_TREES;

/**
 * The set of Attributes used within the system.
 * While new attributes can freely be added, removal is not advised (as some are used for derived traits).
 * @type {Object}
 */
 TAC.attributes_physical = {
    "dexterity": "TAC.dexterity",
    "strength": "TAC.strength",
    "agility": "TAC.agility",
    "constitution": "TAC.constitution"
  };
TAC.attributes_social = {
    "charisma": "TAC.charisma",
    "rhetoric": "TAC.rhetoric",
    "empathy": "TAC.empathy",
    "composure": "TAC.composure"
  };
TAC.attributes_mental = {
    "awareness": "TAC.awareness",
    "intuition": "TAC.intuition",
    "intelligence": "TAC.intelligence",
    "resolve": "TAC.resolve"
  };

/**
 * The set of Skills used within the system.
 * While new skills can freely be added, removal is not advised (as some are used for derived traits).
 * @type {Object}
 */
TAC.skills_physical = {
    "acrobatics": "TAC.acrobatics",
    "alertness": "TAC.alertness",
    "athletics": "TAC.athletics",
    "crafting": "TAC.crafting",
    "fineMotorSkills": "TAC.fineMotorSkills",
    "marksmanship": "TAC.marksmanship",
    "melee": "TAC.melee",
    "stealth": "TAC.stealth", 
    "survival": "TAC.survival",
    "unarmed": "TAC.unarmed"
  };
TAC.skills_social = {
    "animalHandling": "TAC.animalHandling",
    "intimidation": "TAC.intimidation",
    "leadership": "TAC.leadership",
    "manipulation": "TAC.manipulation",
    "negotiation": "TAC.negotiation",
    "persuasion": "TAC.persuasion", 
    "psychology": "TAC.psychology",
    "seduction": "TAC.seduction",
    "socialise": "TAC.socialise",
    "streetwise": "TAC.streetwise"
  };
TAC.skills_mental = {
    "artisan": "TAC.artisan",
    "chemistry": "TAC.chemistry",
    "computer": "TAC.computer",
    "electronics": "TAC.electronics",
    "history": "TAC.history",
    "investigation": "TAC.investigation", 
    "mechanics": "TAC.mechanics",
    "medicine": "TAC.medicine",
    "militaryExpertise": "TAC.militaryExpertise",
    "society": "TAC.society"
  };
  // Dexterity, Agility, Rhetoric, Empathy, (Composure), Awareness, Intuition, Intelligence, any mental skill
  // Charisma

  TAC.HIT_SUCCESS_LIMIT = 99;
  TAC.hitLocations= [
    {name: "head", successes: 5, damage: 1.5},
    {name: "torso", successes: 4, damage: 1.2},
    {name: "arm_right", successes: 3, damage: 1.0},
    {name: "arm_left", successes: 3, damage: 1.0},
    {name: "leg_right", successes: 2, damage: 0.8},
    {name: "leg_left", successes: 2, damage: 0.8},
    {name: "grace", successes: 1, damage: 0.5},
  ];

  TAC.HIT_CHANCE_BASE = 0.5;
  TAC.AMING_CHANCE_INCREASE_PER_SUCCESS = 0.1;


  TAC.ARMOR_SLOTS = {
    arm_right: 'arms',
    arm_left: 'arms',
    leg_right: 'legs',
    leg_left: 'legs',
    torso: 'torso',
    head: 'head',
    grace: 'torso',
    parry: 'arms'
  }

  TAC.ENCUMBRANCE_MODIFIERS = {
    'heavy': {
      'other_traits.action_speed': -4,
      'other_traits.clarity_cost_multiplier': 1 //TODO:
    },
    'medium': {
      'other_traits.action_speed': -2,
      'other_traits.clarity_cost_multiplier': 0.5
    },
    'light': {
      'other_traits.action_speed': -1,
      'other_traits.clarity_cost_multiplier': 0.25
    },
    'none': {}
  }
  
  TAC.hitChanceChangePerSuccess = 4;
  TAC.hitChanceChangePerSuccess_aim = 12;
  

  //How much a point of recoil/heat/deviation affects the miss chance
  TAC.missMult_recoil = 10;
  TAC.missMult_heat = 10;
  TAC.missMult_deviation = 10;

  //How much heat/recoil increase per shot
  TAC.increasePerShot_heat = 0.5; // TODO: make this a weapon stat
  TAC.increasePerShot_recoil = 1;

  TAC.woundChancePerDamage = 5; // In %

  TAC.health_base = 200;
  TAC.clarity_base = 200;
  TAC.healthIncrease_constitution = 20;
  TAC.healthIncrease_composure = 10;
  TAC.clarityIncrease_resolve = 20;
  TAC.clarityIncrease_composure = 10;


  TAC.CLARITY_PER_TICK = 1;

  TAC.attribute_maximum = 20;

  TAC.ACTION_SPEED_CHANCE = 0.5; // Chance to reduce ticks of actions for every point.
  TAC.TICKS_MINIMUM = 0; // Minimum Ticks that an action can cost
  TAC.HEAT_DECREASE_PER_TICK = 0.1; // How much weapon heat decreases per tick
  TAC.HEAT_MAXIMUM = 100;
  TAC.HEAT_THRESHOLD_HIGH = 10;
  TAC.HEAT_THRESHOLD_LOW = 5;

 


  TAC.Ticks_COST_WEAPON_SWITCH = 4; // TODO: Currently does not update UI

  TAC.DAMAGE_LOSS_ABOVE_OPTIMAL_RANGE = 0.1; // TODO: Damage loss in % for each range above the optimum

  
  TAC.ARMOUR_DURABILITY_OK = 75;
  TAC.ARMOUR_DURABILITY_BAD = 40;
  TAC.ARMOUR_SCALING_OK = 0.75;
  TAC.ARMOUR_SCALING_BAD = 0.5;
  TAC.ARMOR_MULT_BYPASS = 0.1;

  TAC.TRAIT_MAXIMUM_GOOD = {
    ammo: 3,
    armour: 4,
    consumable: 3,
    firearm: 4,
    melee: 4
  }

  TAC.TRAIT_MAXIMUM_TOTAL = {
    ammo: 3,
    armour: 4,
    consumable: 3,
    firearm: 8,
    melee: 8
  }

  TAC.TRAIT_MAXIMUM = 8;

  TAC.WEAPON_WIELD_TYPES = [
    "one_handed",
    "two_handed"
  ];

  TAC.ITEM_QUALITIES = {
    '5': 'ultimate', // legendary
    '4': 'elite', // epic, masterwork
    '3': 'premium', // exceptional, excellent
    '2': 'outstanding', // refined
    '1': 'superior', // good, high, enhanced
    '0': 'stock', // average, base... 3% ?
    '-1': 'inferior', // bad, low
    '-2': 'terrible', // awful, inferior, atrocious
    '-3': 'trash'
  }
  // Determines jamming scaling by the function (1/2)^(1/3*q) where q is the quality.
  // Jamming chance = (heat * JAMMING_CHANCE_PER_HEAT * JAMMING_CHANCE_BASE * (1/2)^(1/3*q))
  TAC.JAMMING_CHANCE_PER_HEAT = 0.001;
  TAC.JAMMING_CHANCE_BASE = 0.004;

  TAC.COVER_STRENGTH_LEVELS = {
    weak: {
      value: 25,
      icon: "systems/tac/icons/gui/cover-weak.svg"
    },
    medium: {
      value: 35,
      icon: "systems/tac/icons/gui/cover-medium.svg"
    },
    strong: {
      value: 50,
      icon: "systems/tac/icons/gui/cover-strong.svg"
    }
  };

  TAC.COVER_HEIGHT_LEVELS = {
    none: {
      value: 0,
      icon: "systems/tac/icons/gui/cover-none.svg"
    },
    low: {
      value: 33,
      icon: "systems/tac/icons/gui/cover-low.svg"
    },
    high: {
      value: 66,
      icon: "systems/tac/icons/gui/cover-high.svg"
    },
    full: {
      value: 100,
      icon: "systems/tac/icons/gui/cover-full.svg"
    }
  }

  
// Ruined - Rusted - Battle-scarred - Well-worn - Stock - Industrial - Premium - Elite - Rare - Ultimate
/**
    Common
    Uncommon
    Unusual
    Remarkable
    Rare
    Outstanding
    Exceptional
    Master
    Elite
    Unique
    Epic
    Legendary
  Broken, Damaged, Worn, Well-used, Normal, Enhanced, Exceptional, Ultimate, Pristine, Perfect
 */

  TAC.DAMAGE_TYPES = {
    direct: { // damage type used for effects, etc.
      name: "Direct",
      health: {
        damageMod: 1.0,
        maxDamageMod: 0, // How much of the health damage is applied to the health maximum
        maxDamageModFull: 1.0 // How much of the health damage is applied to the health maximum, when below 0 health
      },
      clarity: {
        damageMod: 0,
        maxDamageMod: 0,
        maxDamageModFull: 1.0
      }
    },
    exhaustion: { // damage type used for effects, etc.
      name: "Exhaustion",
      health: {
        damageMod: 0,
        maxDamageMod: 0, // How much of the health damage is applied to the health maximum
        maxDamageModFull: 1.0 // How much of the health damage is applied to the health maximum, when below 0 health
      },
      clarity: {
        damageMod: 1.0,
        maxDamageMod: 0.25,
        maxDamageModFull: 1.0
      }
    },
    gunshot: {
      name: "Gunshot",
      health: {
        damageMod: 1.0,
        maxDamageMod: 0.2, // How much of the health damage is applied to the health maximum
        maxDamageModFull: 1.0 // How much of the health damage is applied to the health maximum, when below 0 health
      },
      clarity: {
        damageMod: 0.1,
        maxDamageMod: 0.25,
        maxDamageModFull: 1.0
      }
    },
    crushing: {
      name: "Crushing",
      health: {
        damageMod: 1.0,
        maxDamageMod: 0.1, // How much of the health damage is applied to the health maximum
        maxDamageModFull: 0.75 // How much of the health damage is applied to the health maximum, when below 0 health
      },
      clarity: {
        damageMod: 0.25,
        maxDamageMod: 0.25,
        maxDamageModFull: 1.0
      }
    },
    slashing: {
      name: "Slashing",
      health: {
        damageMod: 1.0,
        maxDamageMod: 0.25, // How much of the health damage is applied to the health maximum
        maxDamageModFull: 1.0 // How much of the health damage is applied to the health maximum, when below 0 health
      },
      clarity: {
        damageMod: 0.2,
        maxDamageMod: 0.25,
        maxDamageModFull: 1.0
      }
    },
    piercing: {
      name: "Piercing",
      health: {
        damageMod: 1.0,
        maxDamageMod: 0.15, // How much of the health damage is applied to the health maximum
        maxDamageModFull: 1.0 // How much of the health damage is applied to the health maximum, when below 0 health
      },
      clarity: {
        damageMod: 0.15,
        maxDamageMod: 0.25,
        maxDamageModFull: 1.0
      }
    }
  };           

  // Name, Weight
  TAC.attack_injuryTable = {
    head: {
      "concussion": 5,
      "blinded": 5,
      "pain": 5,
      "bleeding": 5,
    },
    torso: {
      "pain": 5,
      "bleeding": 5, // 0 for Crushing damage
    },
    arm_right: {
      "pain": 5,
      "bleeding": 5, // 0 for Crushing damage
      "arm_right_injury": 5
    },
    arm_left: {
      "pain": 5,
      "bleeding": 5, // 0 for Crushing damage
      "arm_left_injury": 5
    },
    grace: {
      "pain": 5,
    },
    leg_right: {
      "pain": 5,
      "bleeding": 5,
      "leg_right_injury": 5
    },
    leg_left: {
      "pain": 5,
      "bleeding": 5,
      "leg_left_injury": 5
    }
  }





TAC.BASIC_ACTIONS = {
  wait: {
    ticks_cost: 1,
    clarity_cost: 0
  },
  walk: {
    ticks_cost: 3,
    clarity_cost: 0
  },
  run: {
    ticks_cost: 3,
    clarity_cost: 4
  },
  sprint: {
    ticks_cost: 8,
    clarity_cost: 8
  },
  swap_weapon: {
    ticks_cost: 4,
    clarity_cost: 0
  },
  reload: {
    ticks_cost: 4,
    clarity_cost: 0
  },
  eject: {
    ticks_cost: 0,
    clarity_cost: 0
  },
  unjam: {
    ticks_cost: 4,
    clarity_cost: 0
  },
  use_consumable: {
    ticks_cost: 4,
    clarity_cost: 0
  },
  single_shot: {
    ticks_cost: 6,
    clarity_cost: 2
  },
  burst_fire: {
    ticks_cost: 8,
    clarity_cost: 6
  },
  full_auto: {
    ticks_cost: 10,
    clarity_cost: 10
  },
  melee_attack: {
    ticks_cost: 6,
    clarity_cost: 6
  },
  unarmed_attack: {
    ticks_cost: 6,
    clarity_cost: 6
  },
  crippling_shot: {
    ticks_cost: 8,
    clarity_cost: 10
  }
}

TAC.ATTRIBUTE_POINTS_PER_LEVEL = 1;
TAC.SKILL_POINTS_PER_LEVEL = 2;
TAC.TALENT_POINTS_PER_LEVEL = 1;

TAC.ATTRIBUTE_POINTS_BASE = 12;
TAC.SKILL_POINTS_BASE = 22;
TAC.TALENT_POINTS_BASE = 5;

TAC.RECOIL_MAX_PENALTY = 10;


let skillTree = {
	bob: {name: 'Bob'},
  steve: {name: 'Steve', requires: 'bob', x: 1},
  hello: {name: 'Hello', requires: 'bob', y: 1, x: 1 },
  changa: {name: 'Changa', requires: 'bob', y: 2, x: 1},
  denga: {name: 'Denga', requires: 'changa', y: 2, x: 2},
  carl: {name: 'Carl', y: 3, trained: true},
  cranga: {name: 'Cranga', requires: 'carl', trained: true, y: 3, x: 1},
  madness: {name: 'madness', requires: 'cranga', y: 3, x: 2},
  banga: {name: 'Banga', requires: 'carl', y: 4, x: 1},
}

TAC.COLORS = {
  health: {
    heal: 0xe9161c, // 0x34ca6a 
    damage: 0xbd0202, // 0xbd0202
    damage_max: 0x6f0000 // 0x6f0000
  },
  clarity: {
    heal: 0x26ccee,// 0x26ccee,
    damage: 0xa300bf,// 0xa300bf
    damage_max: 0x4f035c// 0x4f035c 
  },
  stamina: {
    heal: 0xe5c708,// 0x26ccee,
    damage: 0xf7f3ab,// 0xa300bf
    damage_max: 0xd86800// 0x4f035c 
  },
  conditions_types: {
    injury: '#fe0100',
    affliction: '#6fa803',
    psychological: '#c70465',
    debilitation: '#fe7500',
    default: '#444444'
  },
  conditions_types_background: {
    injury: '#5E0000',
    affliction: '#304900',
    psychological: '#5C012E',
    debilitation: '#5C2B00',
    default: '#202020'
  },
  resistance: {
    ballistic: '#326094',
    crushing: '#585757',
    cut: '#F0423A',
    puncture: '#28BB1A',
    thermal: '#E66F00'
  }
}