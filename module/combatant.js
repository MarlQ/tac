import { roll } from "./roll.js";
export class CombatantTac extends Combatant {

  async _preCreate(data, options, userID) {
    console.log("Create Combatant")
    await super._preCreate(data, options, userID);

    console.log(this)
    console.log(data)
    const awareness = this.actor.system.attributes_mental.awareness.value;
    const rollResult = await roll(awareness);
    const AC_init = Math.max(0, 5-rollResult.total);
    console.log(rollResult)
    //this.waiting = true;
    this.updateSource({"flags.tac.ticks": AC_init});
    this.updateSource({"flags.tac.waiting": true});
    //await this.data.setFlag("tac", "ticks", AC_init);
    //await this.data.setFlag("tac", "waiting", false);
    await this.actor.startCombat();
    //this.setFlag("tac", "ticks", 0);
  }

 changeTicks(ticks) {
    console.log("Change Ticks: " + ticks)
    return this.setFlag("tac", "ticks", ticks);
  }

  setWaiting(waiting=true) {
    //this.waiting = waiting; //FIXME
    this.setFlag("tac", "waiting", waiting);
  }

  /* async setState(data) {
    return this.update({
      initiative: data.initiative,
      ["flags.tac.ticks"]: data.ticks
    })
  } */

  /* getState() {
    return {
      _id: this.id,
      initiative: this.initiative,
      ct: this.getFlag("tac", "ticks")
    }
  } */
}
