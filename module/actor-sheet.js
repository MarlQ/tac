import { ReloadDialogue } from "./dialogue-reload.js";
import { DiceRollerDialogue } from "./dialogue-diceRoller.js";
import { ItemTac } from "./item.js";
import { createShortActionMessage } from "./chat.js";
import { roll } from "./roll.js";
import { TippyContextMenu } from "./ui.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 */
export class SimpleActorSheet extends ActorSheet {
  constructor(...args) {
    super(...args);

    /**
     * Keep track of the currently active sheet tab
     * @type {string}
     */
    this._rollParameters = [];
    this._rollDicePool = 0;

    this._currentSkillTreePage = 'shotguns';
    this._currentSelectedTalent;
    
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "sheet", "actor"],
  	  template: "systems/tac/templates/actor-sheet.html",
     /*  width: 884,
      height: 720, */
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "attributes"
      }]
    });
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Actor sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  
  
  
   getData() {
    // Basic data
    let isOwner = this.actor.isOwner;
    const sheetData = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      config: CONFIG.TAC,
      rollData: this.actor.getRollData.bind(this.actor) // What is this?
    };

    // The Actor's data
    sheetData.actor = this.actor;
    sheetData.system = this.actor.system;
    sheetData.items = this.actor.items;

    Object.values(this.actor.system.equipmentSlots).forEach(slot => {
      if(!slot.item) {
        sheetData["color_" + slot.name] = '#0be7f7';
      }
      else if(slot.item.system.durability >= CONFIG.TAC.ARMOUR_DURABILITY_OK) {
        sheetData["color_" + slot.name] = 'rgb(3,237,50)';
      }
      else if(slot.item.system.durability >= CONFIG.TAC.ARMOUR_DURABILITY_BAD) {
        sheetData["color_" + slot.name] = '#eeb822';
      }
      else {
        sheetData["color_" + slot.name] = '#ee2222';
      }
      
      
    })

    const encumbrance_colors = {
      none: '#0be7f7',
      light: '#fedf00',
      medium: '#fe7500',
      heavy: '#fe0100'
    };
    sheetData.encumbrance_color = encumbrance_colors[sheetData.system.encumbered];
    sheetData.encumbrance_percentage = sheetData.system.weight / sheetData.system.weightMax_limit * 100;

    sheetData.exp_percentage = sheetData.system.progression.exp / sheetData.system.progression.exp_needed * 100;
    sheetData.currentSkillTreePage = this._currentSkillTreePage;

    if(this._currentSelectedTalent) {
      const talent = CONFIG.TAC.TALENT_TREES[this._currentSkillTreePage].nodes[this._currentSelectedTalent];
      const talent_stacks = this.actor.getTalentStacks(this._currentSelectedTalent);

      sheetData.currentSelectedTalent = {
          name: talent.name,
          description: Array.isArray(talent.description) ? (talent_stacks === 0 ? talent.description[0] : talent.description[talent_stacks-1]) : talent.description,
          description_next: Array.isArray(talent.description) && talent_stacks < talent.levels && talent_stacks > 0 ? talent.description[talent_stacks] : '',
          levelable: talent.levels ? talent_stacks < talent.levels : talent_stacks === 0,
          stacks: talent_stacks,
          key: this._currentSelectedTalent
      };
    }
    
    console.trace(sheetData);
     
    return sheetData;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
    super.activateListeners(html);

    this.drawTalentTree();

    // Activate tabs
    let tabs = html.find('.tabs');
    let initial = this._sheetTab;
    new Tabs(tabs, {
      initial: initial,
      callback: clicked => this._sheetTab = clicked.data("tab")
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    html.find('.niceNumber').click(function(event) {
      let v = $(event.target).text();
      let i = $(this).find('input');
      let value = parseInt(i.val());
      const max = i.attr('max');
      const min = i.attr('min');
      
      if (v === '+') value++;
      else if (v === '−') value--;
      if(max) value = Math.min(value, parseInt(max));
      if(min) value = Math.max(value, parseInt(min));

      i.val(value);
      i.trigger('change');
    });
    
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(event => {
      const itemId = event.currentTarget.closest(".item-edit").dataset.itemId;
      const item = this.actor.getEmbeddedDocument("Item", itemId);
      item.sheet.render(true);
    });

    html.find('.repair-armour').click(event => {
      this.actor.repairArmour();
    });
    
   html.find('.item-delete').click(ev => {
      const itemId = event.currentTarget.closest(".item-delete").dataset.itemId;
      this.actor.deleteOwnedItem(itemId);
    });

    let menuItems = [
      {name: "Equip to right hand", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return !this.actor.isEquipped(item) && (item.type === "melee" || item.type === "firearm"); // TODO: Allow to swap hand if already equipped
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        this.actor.equip(item, "hand_right");
      } },
      {name: "Equip to left hand", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return !this.actor.isEquipped(item) && (item.type === "melee" || item.type === "firearm"); // TODO: Allow to swap hand if already equipped
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        this.actor.equip(item, "hand_left");
      } },
      {name: "Equip", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return !this.actor.isEquipped(item) && (item.type === "armour");
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        this.actor.automaticEquip(item);
      } },
      {name: "Unequip", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return this.actor.isEquipped(item);
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        this.actor.unequip(item)
      } },
      {name: "Add to bag", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return !this.actor.isInBags(item);
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        if(!this.actor.addToBags(item)) {
          ui.notifications.warn(`Error: Item too heavy!`);
        }
      } },
      {name: "Remove from bag", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return this.actor.isInBags(item);
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        this.actor.removeFromBags(item);
      } },
      {name: "Reload magazine", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return item.system.ammo && !item.system.magazine;
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        item.reload(li);
      } },
      {name: "Eject magazine", icon: "", 
      condition: li => {
        const item = this.actor.items.get(li.data("itemid"));
        return item.system.magazine;
      },
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        item.reload(li);
      } },
      {name: "Edit", icon: "", 
      callback: li => {
        const item = this.actor.items.get(li.data("itemid"));
        item.sheet.render(true);
      } },
      {name: "Delete", icon: "", cssClasses: ["red"],
      callback: async li => {
        const item = this.actor.items.get(li.data("itemid"));
        await this.actor.unequip(item);  //FIXME: Maybe put this in a hook attached to deleting items?
        item.delete();
      } },
    ];
    
    new TippyContextMenu({element: html[0], selector: ".item-cell.filled", menuItems: menuItems})

    //Selecting Attributes/Skill to roll
    html.find('.attribute-check').click(ev => {
      let attributeChecks = $( ".attribute-check:checked" );
      //this._rollParameters = attributeChecks.toArray().map( v => v.dataset.attribute);
      this._rollDicePool = attributeChecks.toArray().map( v => {
        return {name: v.dataset.attribute, value: v.dataset.attributevalue};
      });
    });
    
    
    //Clicking roll button
    html.find('.rollButton').mousedown(ev => {  
      this.actor.skillCheckDialogue(this._rollDicePool, ev.which);

      //Uncheck attributes/skills and reset
      let attributeChecks = $( ".attribute-check:checked" );
      attributeChecks.prop("checked", !attributeChecks.prop("checked"));
      //this._rollParameters = [];
      this._rollDicePool = [];
    });

    html.find('.shortRest').mousedown(ev => {  
      this.actor.shortRest();
    });

    html.find('.longRest').mousedown(ev => {  
      this.actor.longRest();
    });
    /* html.find('.exp-button').mousedown(ev => {  
      this._onExpChange();
    }); */

    const expButtonTippy = tippy('.exp-button', {
      content(reference) {
        return `+ <input class="attribute-value" type="number" value=0 data-dtype="Number" min=0 max=999> EXP`;
      },
      trigger: 'manual',
      interactive: true,
      arrow: false,
      //offset: [0, 0],
      role: 'contextmenu',
      allowHTML: true,
      theme: 'tac'
  })[0];

  html.find('.exp-button').mousedown(ev => {  
    expButtonTippy.show();
  });

  $(expButtonTippy.popper).on("change", '.attribute-value',this._onExpChange.bind(this));

    let actor = this.actor;
    // Tooltips
    tippy('.item-cell.filled', {
      content(reference) {
        const id = reference.getAttribute('data-itemid');
        const item = actor.getEmbeddedDocument('Item', id);

        if(item) return item.getTooltip();
        else return '';
      },
      allowHTML: true,
      animation: 'scale',
      duration: 0,
      theme: 'tac'
    });
  }

  updatingEXP = false;

  async _onExpChange(ev) {
    if(this.updatingEXP) return;
    this.updatingEXP = true;
    console.log("EXP", ev);
    const exp = ev.target.value;
    //await this.actor.addExp(exp); //FIXME: Currently the update screws up the animation

    let curExp = this.actor.system.progression.exp + +exp;
    let curLvl = this.actor.system.progression.level;

    while(curExp >= this.actor.getExpToNextLevel(curLvl)) {
      curExp -= this.actor.getExpToNextLevel(curLvl);
      curLvl++;
    }

    const expPercentage = curExp / this.actor.getExpToNextLevel(curLvl) * 100;

    $(".xp-increase-fx").css("display","inline-block");
    $(".xp-bar-fill").css("box-shadow",/*"0px 0px 15px #06f,*/ "-5px 0px 10px #fff inset");
    setTimeout(function(){$(".xp-bar-fill").css("-webkit-transition","all 0.5s ease"); //FIXME: always animate to the right
    $(".xp-bar-fill").css("width", expPercentage + "%");},100);
    setTimeout(function(){$(".xp-increase-fx").fadeOut(200);$(".xp-bar-fill").css({"-webkit-transition":"all 0.5s ease","box-shadow":""});},200);

    setTimeout(() => {
      this.updatingEXP = false;
      this.actor.update({
        'system.progression.level': curLvl,
        'system.progression.exp': curExp
      });
    },500);
    
  }

   /* -------------------------------------------- */  

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
   async _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const type = header.dataset.type;
    const itemData = {
      name: `New ${type.capitalize()}`,
      type: type,
      data: duplicate(header.dataset)
    };
    
    //delete itemData.data["type"];
    return this.actor.createEmbeddedDocuments("Item", [itemData]);
  }

  /* -------------------------------------------- */

  /**
   * Implement the _updateObject method as required by the parent class spec
   * This defines how to update the subject of the form when the form is submitted
   * @private
   */

  _updateObject(event, formData) {
    console.log("FORM",formData)

    let healthDifference = this.actor.system.health.value - formData['system.health.value'];
    formData['system.health.value'] = this.actor.system.health.value;
    formData['system.health.damage'] = Math.clamped(this.actor.system.health.damage + healthDifference, this.actor.system.health.max-this.actor.system.health.max_cur, this.actor.system.health.max);

    let clarityDifference = this.actor.system.clarity.value - formData['system.clarity.value'];
    formData['system.clarity.value'] = this.actor.system.clarity.value;
    formData['system.clarity.damage'] = Math.clamped(this.actor.system.clarity.damage + clarityDifference, this.actor.system.clarity.max-this.actor.system.clarity.max_cur, this.actor.system.clarity.max);
    //formData["id"] = this.object.id;
    return this.object.update(formData); //this.object is the same as this.entity
  }




  drawTalentTree() {
    const W_DIST = 40;
    const H_DIST = 25;
    const NODE_WIDTH = 60;
    const NODE_HEIGHT = 60;
    const STROKE_WIDTH = 6;
    let highest_y = 0;
    let highest_x = 0;

    const talentNodes = CONFIG.TAC.TALENT_TREES[this._currentSkillTreePage].nodes;


    const calculateYOffset = (node1, node2, node2_key) => {
      if(!this.actor.getTalentStacks(node2_key)) {
        return (Math.abs(node2.y - node1.y) - 1) * (NODE_HEIGHT + H_DIST);
      }
      //let y_offset = (Math.abs(node2.y - node1.y) - 1) * (NODE_HEIGHT + H_DIST);
      let y_offset = 0;

      for(const [key, node] of Object.entries(talentNodes)) {
        if(node.x === node2.x && this.actor.getTalentStacks(key)) {
          if(node.y < node2.y) y_offset = Math.max( (Math.abs(node2.y - node.y) - 1) * (NODE_HEIGHT + H_DIST), y_offset);
        }
      }
      console.log(node2, y_offset)
      return y_offset;
    }

    // Draws a line from node1 to node2, with orthogonal connectors
    const drawLine= (node1, node2, node1_key, node2_key) => {
      const x1 = node1.x_pos, y1 = node1.y_pos, x2 = node2.x_pos, y2 = node2.y_pos;
      let cssClass = this.actor.getTalentStacks(node1_key) && this.actor.getTalentStacks(node2_key) ? 'trained ' : '';
      if(!this.actor.getTalentStacks(node1_key) && !this.actor.getTalentStacks(node2_key)) cssClass += 'unavailable';


      if(y2 > y1 && x2 > x1) { 
        const y_offset = (Math.abs(node2.y - node1.y) - 1) * (NODE_HEIGHT + H_DIST);
        let cssClassVertical = '';
        if(this.actor.getTalentStacks(node1_key) && this.actor.getTalentStacks(node2_key)) {
          cssClassVertical += 'trained '
        }
        else {
          for(const [key, node] of Object.entries(talentNodes)) {
            if(node.x === node2.x && this.actor.getTalentStacks(key) && node.y > node2.y) {
              cssClassVertical += 'trained ';
              break;
            }
          }
          if(!this.actor.getTalentStacks(node1_key) && !this.actor.getTalentStacks(node2_key)) cssClassVertical += 'unavailable'
        }
          
        
        
        //if(this.actor.hasTalent(node2_key)) y_offset = 0;
        return `
          <line class="${cssClassVertical}" x1="${x1 + W_DIST/2}" y1="${y1+STROKE_WIDTH/2+y_offset}" x2="${x1 + W_DIST/2}" y2="${y2+STROKE_WIDTH/2}"></line>
          <line class="${cssClass} horizontal" x1="${x1 + W_DIST/2 +STROKE_WIDTH/2}" y1="${y2}" x2="${x2-NODE_WIDTH}" y2="${y2}"></line>
        `;
      }

      return `<line class="${cssClass}" x1="${x1}" y1="${y1}" x2="${x2-NODE_WIDTH}" y2="${y2}"></line>`;
    }

    let nodesHtml = `<div class='nodes-tree'>`;

    let lines = '';
    for(const [key,node] of Object.entries(talentNodes)) { 
      if(!node.x) node.x = 0;
      if(!node.y) node.y = 0;
      let h = node.y * NODE_HEIGHT + (node.y) * H_DIST;
      let w = node.x * NODE_WIDTH + (node.x) * W_DIST;
      const trained = this.actor.getTalentStacks(key);
      let cssClass = trained ? 'trained ' : '';

      if(!trained && node.requires) {
        if(!this.actor.getTalentStacks(node.requires)) cssClass += 'unavailable ';
      }
      if(this._currentSelectedTalent === key) cssClass += 'selected ';
      nodesHtml += `<div class='node ${cssClass}' style='top:${h}px;left:${w}px' data-talent='${key}'>${node.name}${node.levels > 1 ? `<span class="levels">${trained}</span>` : ''}</div>`;
      node.x_pos = w + NODE_WIDTH;
      node.y_pos = h + NODE_HEIGHT/2;

      highest_y = Math.max(highest_y, node.y_pos + NODE_HEIGHT/2);
      highest_x = Math.max(highest_x, node.x_pos);
    }
    nodesHtml += '</div>';

    for(const [key,node] of Object.entries(talentNodes)) { 
      if(node.requires) {
        let prev = talentNodes[node.requires];
        lines += drawLine(prev,node, node.requires, key);
      } 
    }
    let connectionHtml = `<svg class='nodes-connections' width='${highest_x}px' height='${highest_y}px'>${lines}</svg>`;

    $('.skill-tree').html(`
      ${connectionHtml}
      ${nodesHtml}
    `);

    $('.node').on('click', (event) => {
      const talentName = $(event.target).data('talent');
      //const talent = talentNodes[talentName];

      this._currentSelectedTalent = talentName;
      this.render();
    });

    $('.talent-button').on('click', event => {
      const talentName = $(event.target).data('talent');

      this.actor.addTalent(talentName);
    })

    $('.talent-remove-button').on('click', (event) => {      
      const talent = $(event.target).data('talent');
      this.actor.removeTalent(talent);
    });
  }
}
