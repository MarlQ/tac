import { createShortActionMessage } from "./chat.js";
export class CombatTac extends Combat {

  /**
     * Define how the array of Combatants is sorted in the displayed list of the tracker.
     * This method can be overridden by a system or module which needs to display combatants in an alternative order.
     * By default sort by ticks value, then initiative, next falling back to name, lastly tie-breaking by combatant id.
     * @private
     */
  _sortCombatants(a, b) {
    console.log("sort combatant")
    //console.log(a)
    //console.log(b)

    const a_ticks = a.getFlag("tac", "ticks") ?? 0;
    const b_ticks = b.getFlag("tac", "ticks") ?? 0;

    const cta = Number.isNumeric(a_ticks) ? a_ticks : -9999;
    const ctb = Number.isNumeric(b_ticks) ? b_ticks : -9999;

    let cct = cta - ctb;
    if ( cct !== 0 ) return cct;

    const ia = Number.isNumeric(a.initiative) ? a.initiative : -9999;
    const ib = Number.isNumeric(b.initiative) ? b.initiative : -9999;

    let ci = ib - ia;
    if ( ci !== 0 ) return ci;

    let cn = a.name.localeCompare(b.name);
    if ( cn !== 0 ) return cn;

    return a.id - b.id;
  }

  /* _prepareCombatant(c, scene, players, settings = {}) {
    console.log("Prepare Combatant")
    let combatant = super._prepareCombatant(c, scene, player, settings);
    combatant.flags.ticks = Number.isNumeric(combatant.flags.ticks) ? Number(combatant.flags.ticks) : 0;

    return combatant;
  } */

  async rollInitiative(ids, formulaopt, updateTurnopt, messageOptionsopt) {
    console.log("Roll Initiative")
    await super.rollInitiative(ids, formulaopt, updateTurnopt, messageOptionsopt);
    return this.update({turn: 0});
  }

  /**
   * Return the Array of combatants sorted into initiative order
   * @returns {Combatant[]}
   */
   setupTurns() {
    console.log("SETUP TURNS")

    // Determine the turn order and the current turn
    const turns = this.combatants.contents.sort(this._sortCombatants);
    if ( this.turn !== null) this.turn = 0;

    // Update state tracking
    let c = turns[this.turn];
    this.current = {
      round: this.round,
      turn: 0,
      combatantId: c ? c.id : null,
      tokenId: c ? c.tokenId : null
    };
    console.log("TURN", this.turn)
    // Return the array of prepared turns
    return this.turns = turns;
  }

  async startCombat() { // FIXME: Crashes when there are no combatants
    console.log("Start Combat")

    let missingInitiative = this.combatants.filter( c => c.initiative === undefined);
    console.log(missingInitiative)
    if(missingInitiative.length > 0) {
      missingInitiative.forEach(c =>
        ui.notifications.error(game.i18n.format("TAC.Combat.MissingInitiative", {token: c.token.name}))
      );
      return;
    }

    this.setupTurns();
    console.log("combatats", this.combatants)
    if(!this.combatants.length) return super.startCombat();

    const updates = this.combatants.map(c => {
      return {
        _id: c.id,
        ["flags.tac.ticks"]: c.getFlag("tac", "ticks")
      }
    });
    
    let actionPossible = false;
    console.log(updates)

    updates.forEach(u => {
      if(u["flags.tac.ticks"] <= 0) actionPossible = true;
    });

    // Lower every combatant's ct by 1 until it reaches 0 for one combatant
    while(!actionPossible) {
      updates.forEach(u => {
        u["flags.tac.ticks"] = Math.max(u["flags.tac.ticks"]-1, 0);
        if(u["flags.tac.ticks"] <= 0) actionPossible = true;
      });
    }
    console.log("UPDATE combatant", updates)
    await this.updateEmbeddedDocuments("Combatant", updates);
    console.log("Starting combat")
    return super.startCombat();
  }

  /* async changeCT(combatant, ct) {
    console.log("Increase CT: " + ct)
    return this.updateCombatant({
      _id: combatant.id,
      ["flags.ticks"]: ct
    })
  } */
  async _pushHistory(){ // TODO: Implement

  }

  _onUpdate(data, options, userId) {
    console.log("ON UPDATE")
    super._onUpdate(data, options, userId);
    this.turn = 0;

  }

  waitAction(combatant) {
    combatant.setWaiting(true);
    createShortActionMessage("Waits", combatant.actor);
    console.log("WAITS", combatant)
    return this.changeTicks(combatant, combatant.getFlag("tac", "ticks") + 1)
  }


  async changeTicks(combatant, ticks) {
    //console.error("Change Ticks WHAAAAAAAAAAAAT")
    console.log("CHANGE Ticks",combatant)
    //this._pushHistory(combatant.getState());
    
    //await this.updateEmbeddedDocuments("Combatant", updates);
    await combatant.changeTicks(ticks);
    await this.update({turn: 0});
    
  }

  async increaseTicks(combatant, ticks) {
    console.log("Change Ticks")
    console.log(combatant)

    let ticks_reduction = 0;
    
    let action_speed = combatant.actor.system.other_traits.action_speed.raw;
    if(action_speed) {
      
      for(let i = 0; i < Math.abs(action_speed); i++) {
        let ran = Math.random();
        if(ran <= CONFIG.TAC.ACTION_SPEED_CHANCE) {
          if(action_speed > 0) ticks_reduction++;
          else ticks_reduction--;
        }
      }
    }
    if(ticks_reduction) {
      
      if(ticks_reduction > 0) {
        let ticks_message = ticks - ticks_reduction < CONFIG.TAC.TICKS_MINIMUM ? ticks - CONFIG.TAC.TICKS_MINIMUM : ticks_reduction;
        createShortActionMessage("Ticks reduced by " + ticks_message, combatant.actor)
      }
      else { 
        let ticks_message = -ticks_reduction;
        createShortActionMessage("Ticks increased by " + ticks_message, combatant.actor)
      }
    }
    console.log("Action Speed", ticks_reduction)
    ticks = Math.max(CONFIG.TAC.TICKS_MINIMUM, ticks - ticks_reduction);

    //this._pushHistory(combatant.getState());
    return this.changeTicks(combatant, combatant.getFlag("tac", "ticks") + ticks)
    //return combatant.changeTicks(combatant.getFlag("tac", "ticks") + ct); // FIXME
  }

  async nextTurn() {
    console.log("Pass")
    return this.waitAction(this.combatant);
  }

  async nextRound() {
    console.log("Next Round")
    const updates = this.combatants.map(c => {
      return {
        _id: c.id,
        ["flags.tac.ticks"]: c.getFlag("tac", "ticks")
      }
    });

    let roundIncrease = 0;
    let actionPossible = false;
    console.log(this.combatants)

    // Lower every combatant's ticksby 1 until it reaches 0 for one combatant
    while(!actionPossible) {
      roundIncrease += 1;
      updates.forEach(u => {
        u["flags.tac.ticks"] = Math.max(u["flags.tac.ticks"] - 1, 0);
        if(u["flags.tac.ticks"] <= 0) actionPossible = true;
      });
    }
    await this.updateEmbeddedDocuments("Combatant", updates)

    for(const c of this.combatants) {
      console.log("ROUND", c.getFlag("tac", "waiting"))
      c.actor.combatTick(this.round, this.round + roundIncrease, c.getFlag("tac", "waiting"));
      c.setWaiting(false);
      //c.actor.adjustWeaponHeat( -roundIncrease * CONFIG.TAC.HEAT_DECREASE_PER_TICK);
    }


    let turn = this.turn === null ? null : 0; // Preserve the fact that it's no-one's turn currently.
    if ( this.settings.skipDefeated && (turn !== null) ) {
      turn = this.turns.findIndex(t => !t.isDefeated);
      if (turn === -1) {
        ui.notifications.warn("COMBAT.NoneRemaining", {localize: true});
        turn = 0;
      }
    }
    let advanceTime = Math.max(this.turns.length - this.turn, 0) * CONFIG.time.turnTime;
    advanceTime += CONFIG.time.roundTime;

    const updateData = {round: this.round+roundIncrease, turn: turn};
    const updateOptions = {advanceTime, direction: 1}

    Hooks.callAll("combatRound", this, updateData, updateOptions);
    return this.update(updateData, updateOptions);
  }

  _onDelete(options, userId) {
    super._onDelete(options, userId);
    if ( this.collection.viewed === this ) ui.combat.initialize({render: false});
    if ( userId === game.userId ) this.collection.viewed?.activate();

    // Call endCombat() for each actor in combat
    for(const c of this.combatants) {
      c.actor.endCombat()
    }
  }

  //TODO: previousRound, previousTurn
  
}
