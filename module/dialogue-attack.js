import { Attack } from "./attack.js";

export class AttackDialogue extends Application {
  constructor({
    weapon, rollType, bullets=1, weapon_damage=0, damageType="crushing", ticks_cost=0, clarity_cost=0, actor_token
  }, ...args) {
    super(...args);
    
    this.weapon_owner = weapon.actor;
    if (this.weapon_owner == null) {
      console.error("Error: no actor found.")
      return;
    }
    
    //Initialise values
    this.weapon = weapon;
    this.weapon_damage = this.weapon.type === "firearm" ? weapon_damage : weapon_damage+this.weapon_owner.system.attributes_physical.strength.value;
    this.damage_total = this.weapon_damage;
    this.damage_type = damageType;
    this.ticks_cost = ticks_cost;
    this.clarity_cost = clarity_cost;
    this.actor_token = actor_token;
    
    this.dicePool_total = 0;
    this.targetNumber_total = 6;
    
    this.rollType = rollType;
    this.bullets = bullets;
    this.options.title = this.weapon_owner.name + ": " + this.weapon.name + " - Attack";


    
    if(this.weapon.system.jammed) {
      ui.notifications.warn(`Error: weapon is jammed!`);
      return;
    }

    if(this.weapon_owner.system.clarity.value < this.clarity_cost) {
      return ui.notifications.warn(`Error: not enough Clarity!`);
    }

    let target_tokens = game.user.targets;
    if (target_tokens.size == 0) {
      ui.notifications.warn(`Error: no target selected`);
      return;
    } else if (target_tokens.size > 1) {
      ui.notifications.warn(`Error: please select only one target`);
      return;
    }
    let it = target_tokens.values();
    
    this.target = it.next().value;


    
    Handlebars.registerHelper('isFirearm', function (type) {
      return type === "firearm";
    });
    
    Handlebars.registerHelper('isMelee', function (type) {
      return type === "melee";
    });

    this.render(true);
    
  }
  
  getData() {
    const data = super.getData();

    const interscetions = this._getCoverIntersections(this.actor_token.center, this.target.center);
    console.log("INTER", interscetions)

    data.weaponType = this.weapon.type;
    data.dicePool = this.dicePool_total;
    data.targetNumber = this.targetNumber_total;
    data.damage = this.damage_total;
    data.aimTargets = [{name: "Any"}].concat(CONFIG.TAC.hitLocations);
    const flags = game.users.current.flags.tac.attack[ (this.weapon.type === "firearm")?'ranged':'melee' ];
    data.flags = flags ? flags : {
      dicePool_userMod: 0,
      targetNumber_userMod: 0,
      damage_userMod: 0
    }

    return data;
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["worldbuilding", "dialogue"],
      template: "systems/tac/templates/dialogues/dialogue-attack.html",
      resizable: true
    });
  }

  
  async _onExecuteAttack(event, html){
    
    let modifiers = [];
    let userInputs = this._fetchInputs(html);
    
    let target_dicepool = this.target.actor.system.attributes_physical.agility.value;
    
    // Construct the modifier list (mostly just visual for the chat message)
    if(userInputs.cover_chance > 0) modifiers.push("Cover: " + userInputs.cover_chance + "%");
    if(userInputs.target_distance !== undefined) modifiers.push("Distance: " + userInputs.target_distance);
    if(userInputs.position !== undefined) modifiers.push("Position: " + userInputs.position);
    if(userInputs.self_running) modifiers.push("Running");
    if(userInputs.self_distracted) modifiers.push("Distracted");
    if(userInputs.self_offHand) modifiers.push("Off-hand");
    if(userInputs.target_hidden) modifiers.push("Target hidden");
    if(userInputs.target_running) modifiers.push("Target running");
    if(userInputs.target_outnumbered) modifiers.push("Target outnumbered");
    
    if(userInputs.target_defending){
      modifiers.push("Target defending");
      target_dicepool += this.target.actor.system.skills_physical.melee.value;
    } 
    
    if(userInputs.target_unaware){
      modifiers.push("Target unaware");
      target_dicepool = 0;
    } 
    
    if(userInputs.target_immobile){
      modifiers.push("Target immobile");
      target_dicepool = 0;
    } 
    
    let consumeAmmo = false;
    let weapon_ammo = 0;
    if(this.weapon.type === "firearm" && this.weapon.system?.magazine){
      consumeAmmo = true;
      weapon_ammo = this.weapon.system.magazine.system.quantity;
      if(weapon_ammo < 1) {
        ui.notifications.warn(`Error: no ammo in the weapon`);
        return;
      }
    } else if(this.weapon.type === "firearm" && !this.weapon.system?.magazine){
      ui.notifications.warn(`Error: no ammo in the weapon`);
      return;
    }
    let weapon_increaseHeat = this.weapon.type === "firearm" ? true : false;
    
    //Update user flags
    let currentUser = game.users.current;
    if(this.weapon.type === "firearm") currentUser.update({"flags.tac.attack.ranged": userInputs});
    else currentUser.update({"flags.tac.attack.melee": userInputs});

    this.weapon_owner.applyActionCost(this.ticks_cost, this.clarity_cost, this.actor_token);
    
    await Attack.executeAttack({
      num_attacks: this.bullets,
      player_dicePool: this.dicePool_total,
      player_targetNumber: this.targetNumber_total,
      target_dicePool: target_dicepool,
      target_targetNumber: 6, //TODO
      target_coverChance: userInputs.cover_chance, 
      target_coverStrength: userInputs.cover_strength,
      target_distance: userInputs.target_distance,
      target_aimLocation: userInputs.aimLocation,
      modifiers: modifiers,
      weapon_damage: this.damage_total,
      weapon_increaseHeat: weapon_increaseHeat, //TODO
      weapon: this.weapon, 
      target: this.target.actor,
      token: this.target,
      attacker: this.weapon_owner,
      actor_token: this.actor_token,
      weapon_consumeAmmo: consumeAmmo, //TODO
      weapon_name: this.weapon.name,
      attack_name: this.rollType,
      target_name: this.target.actor.name,
      speaker: this.weapon_owner,
      damage_type: this.damage_type,
      all_successes: userInputs.target_immobile,
      enable_overpenetration: this.weapon.type === "firearm",
      weaponType: this.weapon.system.subtype
    });
    //setTimeout(() => { aimingDialogue.close(); }, 2000);
  }

  _getCoverIntersections(from, to) {
    const intersectingWalls = canvas.walls.placeables.filter((wall, key) => {
      //const dist = Math.sqrt( (wall.object.x - x)^2 + (wall.object.y - y)^2 );

      const intersect = foundry.utils.lineSegmentIntersection(from, to, wall.A, wall.B);
      if(intersect) {
          return wall;
      }
        
    });
  return intersectingWalls;
  }

  _updateBaseValueFields(html){
    const dicePoolBase = html.find('[name="dicePoolBase"]');
    const targetNumberBase = html.find('[name="targetNumberBase"]');
    const damageBase = html.find('[name="damageBase"]');
    
    dicePoolBase[0].value = this.dicePool_total;
    targetNumberBase[0].value =  this.targetNumber_total
    damageBase[0].value = this.damage_total
  }
  
  _updateModifiers(html){
    let userInputs = this._fetchInputs(html);
    let dicePool_mod = 0;
    let dicePool = 0;
    
    if(this.weapon.type === "firearm") dicePool = this.weapon_owner.system.attributes_physical.dexterity.value + this.weapon_owner.system.skills_physical.marksmanship.value;
    else if(this.weapon.type === "melee") dicePool = this.weapon_owner.system.attributes_physical.agility.value + this.weapon_owner.system.skills_physical.melee.value;
    dicePool += this.weapon.system.impact + this.weapon_owner.system.dicePoolChange;
    
    //Calculate difficulty incurred from range difference
    let dicePool_rangePenalty = 2*Math.abs(this.weapon.system.range - userInputs.target_distance);
    
    //Get modifiers
    if(userInputs.self_running) dicePool_mod += this.weapon_owner.system.run_action_penalty;
    if(userInputs.self_distracted) dicePool_mod -= 2;
    if(userInputs.self_offHand) dicePool_mod -= 2;
    if(userInputs.target_hidden){
      if(this.weapon.type === "firearm") dicePool_mod -= 3;
      else dicePool_mod -= 2;
    } 
    if(userInputs.target_running) dicePool_mod += this.target.actor.system.run_attacker_penalty;
    if(userInputs.target_outnumbered) dicePool_mod += 2;
    if(userInputs.position) dicePool_mod += userInputs.position;
    
    //Calculate new dice pool, target number
    let newDicePool = dicePool + userInputs.dicePool_userMod - dicePool_rangePenalty + dicePool_mod;
    let targetNumber_diceMod = 0;
    if(newDicePool < 1){ 
      //Dice Pool can't go below 1, increase target number instead
      targetNumber_diceMod = Math.abs(newDicePool)+1;
      newDicePool = 1;
    }
    
    this.dicePool_total = newDicePool;
    this.targetNumber_total = Math.max(0,Math.min(10, 6 + userInputs.targetNumber_userMod + targetNumber_diceMod));
    this.damage_total = Math.max(0, this.weapon_damage + userInputs.damage_userMod);
  }
  
  _initialiseValues(html){
    const flags = game.users.current.flags.tac.attack[ (this.weapon.type === "firearm")?'ranged':'melee' ];
    [...html.get(0).querySelectorAll('input,select')].forEach( v => {
      if(v.name && flags[v.name] !== undefined){
        if(v.type == 'radio'){  v.checked = (v.value == flags[v.name])?true:false; }
        else{ v[(v.type == 'checkbox')?'checked':'value'] = flags[v.name]; }
      }
    });
  }

  _fetchInputs(html){
    return [...html.get(0).querySelectorAll('input,select')].reduce( (acc,val) => {
      if     (val.type == 'radio'){ if(! acc.hasOwnProperty(val.name)){acc[val.name]=undefined;} acc[val.name]=(val.checked)?val.value*1:acc[val.name]; }
      else if(val.type == 'number'){ acc[val.name] = val.value*1 }
      else{ acc[val.name]=(val.type == "checkbox")?val.checked:val.value; }   return acc;}, {});
  }
  
  
  activateListeners(html) {
    super.activateListeners(html);
    
    this._initialiseValues(html);
    
    this._updateModifiers(html);
    this._updateBaseValueFields(html);
    
    html.find('[name="target_distance"], [name="cover_chance"], [name="cover_strength"], [name="position"]').click(ev => {
      this._updateModifiers(html);
      this._updateBaseValueFields(html);
    });
    
    html.find('[name="self_distracted"], [name="self_running"], [name="self_offHand"], [name="target_hidden"], [name="target_running"], [name="target_outnumbered"]').change(ev => {
      this._updateModifiers(html);
      this._updateBaseValueFields(html);
    });
    
    html.find('[name="dicePool_userMod"], [name="targetNumber_userMod"], [name="damage_userMod"]').on("input", ev => {
      this._updateModifiers(html);
      this._updateBaseValueFields(html);
    });
    
    //Execute Attack
    html.find('.attack-execute').click(ev => this._onExecuteAttack(ev, html));
  }
}