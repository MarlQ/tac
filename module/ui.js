/* possibleTraits = possibleTraits.concat(CONFIG.TAC.WEAPON_TRAITS_BAD.map(name => {
    let img = "systems/tac/icons/conditions/" + name + ".svg";
    let type = CONFIG.TAC.conditions[name].type;
    return {
      name: name,
      icon: `<img src="${img}" title="${name}" class="${type}"/>`,
      condition: li => !this.item.hasTrait(name),
      callback: li => this.item.addTrait(name)
    }
  }));
  

  new ContextMenu(html, ".add-trait-button", possibleTraits, {eventName: 'click'}); */



export class TippyContextMenu {

        /**
     * @param {HTMLElement|jQuery} element                The containing HTML element within which the menu is positioned
     * @param {string} selector                           A CSS selector which activates the context menu.
     * @param {ContextMenuEntry[]} menuItems              An Array of entries to display in the menu
     * @param {object} [options]                          Additional options to configure the context menu.
     * @param {string} [options.eventName="contextmenu"]  Optionally override the triggering event which can spawn the
     *                                                    menu
     * @param {ContextMenuCallback} [options.onOpen]      A function to call when the context menu is opened.
     * @param {ContextMenuCallback} [options.onClose]     A function to call when the context menu is closed.
     */
    constructor({element, selector, menuItems, eventName="contextmenu", onOpen, onClose, placement='right-start', alignToCursor=false}) {
        this.selector = selector;
        this.eventName = eventName;
        this.menuItems = menuItems;
        this.onOpen = onOpen;
        this.onClose = onClose;
        this.placement = placement;


        const rightClickableArea = element.querySelectorAll(selector);
        for(const area of rightClickableArea) {
            area.addEventListener(eventName, (event) => {
                this.target = event.currentTarget;
                event.preventDefault();

                event.stopPropagation();
                //this.onOpen?.(this.#target);
                return this.render(area);
            });
        }
        
    }

    render(target) {
        let ol = document.createElement('ol');
        ol.classList.add('context-items');

        // Build menu items
        for (let item of this.menuItems) {
            // Determine menu item visibility (display unless false)
            let display = true;
            if ( item.condition !== undefined ) {
                display = ( item.condition instanceof Function ) ? item.condition($(this.target)) : item.condition;
            }
            if ( !display ) continue;

            // Construct and add the menu item
            let name = game.i18n.localize(item.name);
            let li = document.createElement('li');
            li.classList.add('context-item');
            li.innerHTML = `${item.icon}${name}`;

            if(item.cssClasses) {
                for(const cssClass of item.cssClasses) {
                    li.classList.add(cssClass);
                }
            }
            

            ol.appendChild(li);
            // Record a reference to the item
            item.element = li;
        }
        
        // Bail out if there are no children
       // if ( ol.children().length === 0 ) return;

        // Deactivate global tooltip
        game.tooltip.deactivate();

        this.instance = tippy(target, {
            content: ol,
            placement: this.placement,
            trigger: 'manual',
            interactive: true,
            arrow: false,
            offset: [0, 0],
            role: 'contextmenu',
        });

        if(this.alignToCursor) {
            this.instance.setProps({
                getReferenceClientRect: () => ({
                  width: 0,
                  height: 0,
                  top: event.clientY,
                  bottom: event.clientY,
                  left: event.clientX,
                  right: event.clientX,
                }),
            });
        }
        
        this.instance.show();

        // Apply interactivity
        this.activateListeners(this.instance.popper);
    }

    /**
   * Local listeners which apply to each ContextMenu instance which is created.
   * @param {jQuery} html
   */
    activateListeners(html) {
       $(html).on("click", "li.context-item", this.#onClickItem.bind(this));
    }

    /**
   * Handle click events on context menu items.
   * @param {PointerEvent} event      The click event
   */
    #onClickItem(event) {
        event.preventDefault();
        event.stopPropagation();
        const li = event.currentTarget;
        const item = this.menuItems.find(i => i.element === li);
        item?.callback($(this.target));
        this.instance.hide();
    }
}


