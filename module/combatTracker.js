export class CombatTrackerTac extends CombatTracker {


  get template() {
    return "systems/tac/templates/combat-tracker.hbs";
  }

  async getData(options) {
    const data = await super.getData(options);

    if(!data.hasCombat) {
      return data;
    }

    for(let [i, combatant] of data.combat.turns.entries()) {
      data.turns[i].ticks= combatant.getFlag("tac", "ticks");
    }
    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find(".ticks-input").change(this._onTicksChanged.bind(this));
  }

  async _onTicksChanged(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);

    await this.viewed.changeTicks(c, +btn.value);
    this.render();
  }
  
}
