export class AimingDialogue extends Application {
  constructor(...args) {
    super(...args);
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["worldbuilding", "dialogue"],
      template: "systems/tac/templates/dialogue-aiming.html",
      width: 600,
      height: 800
    });
  }

  getData() {
    const data = super.getData();
    return data;
  }
  
  static getPointBetweenPoints(x1, x2, y1, y2, distance) {
    distBetween = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
    distRatio = distance / distBetween;
    x = x1 + distRatio * (x2 - x1);
    y = y1 + distRatio * (y2 - y1);
    return {x: x, y: y};
  }
  
  //minAngle and maxAngle should be values from 0 to 2PI
  static getRandomPoint(x, y, distance, minAngle, maxAngle) {
    let angle_random = Math.random() * (+maxAngle - +minAngle) + +minAngle;
    console.log("Angle: " + angle_random);
    let x_result = Math.floor(x + Math.cos(angle_random) * distance);
    let y_result = Math.floor(y + Math.sin(angle_random) * distance); 
    
    if (x_result <= 0) x_result = 1;
    else if (x_result >= 299) x_result = 299 - 1;
    if (y_result <= 0) y_result = 1;
    else if (y_result >= 633) y_result = 633 - 1;
    
    return {x: x_result, y: y_result};
  }
  
  static getHitLocation(x, y){
    let parentElement = document.querySelector('#hitBoi').parentNode;
    var rect = parentElement.getBoundingClientRect();
    let hit_xTransf = x + rect.left;
    let hit_yTransf = y + rect.top;
    let hitLocation = document.elementFromPoint(hit_xTransf, hit_yTransf);
    return hitLocation.classList[0];
  }
  
  
  static setPoint(x, y){
    document.querySelector("#hitBoi").style = `top: ${y-2}px; left: ${x-2}px`;
  }

  async simulateAttack({
    bullets=1,
    player_hitDice, 
    player_difficulty=6,
    player_strength=1,
    player_composure=1,
    target_hitDice, 
    target_difficulty=6, 
    target_armour=0,
    target_cover=0, 
    target_distance=0,
    weapon_damage=1, 
    weapon_size= 0, 
    weapon_recoil=0, 
    weapon_penetration=0,
    weapon_heat=0,
    weapon_increaseHeat=false,
    weapon,
    weapon_consumeAmmo=false, 
    weapon_magazine
  }={}) {
    console.log("-----------------------");
    console.log(bullets);
    console.log(player_hitDice);
    console.log(player_difficulty);
    console.log(player_strength);
    console.log(player_composure);
    console.log(target_hitDice);
    console.log(target_difficulty);
    console.log(target_armour);
    console.log(target_cover);
    console.log(target_distance);
    console.log(weapon_damage);
    console.log(weapon_size);
    console.log(weapon_recoil);
    console.log(weapon_penetration);
    console.log(weapon_heat);
    console.log(weapon_consumeAmmo);
    console.log(weapon_magazine);

    /**
      if(event.target.classList.contains("figureBody")){
        console.log("Aimed at Body");
      }
      else if(event.target.classList.contains("figureHead")){
        console.log("Aimed at Head" );
      }
      else if(event.target.classList.contains("figureMiss")){
        console.log("Aimed at Miss");
      }else{ 
        console.log("Aimed at ???");
      }
      **/
/**
    let owner = weapon.actor;
    if (owner == null) {
      console.log("Error: no actor found.")
      return;
    }

    let target_actors = game.user.targets;
    if (target_actors.size == 0) {
      console.log("Error: no target selected.");
      return;
    } else if (target_actors.size > 1) {
      console.log("Error: please select only one target.");
      return;
    }
    let it = target_actors.values();
    let firstTarget = it.next();
**/
    
    if(weapon_consumeAmmo && weapon_magazine){
      let ammoAmount = weapon_magazine.system.quantity.value;
      if (ammoAmount <= 0) {
        console.log("Not enough ammo");
        return;
      }
    }

    //Determine successes
    let r = new Roll("(@dice)d10cs>=(@diff)", {
      dice: player_hitDice,
      diff: player_difficulty
    });
    r.roll();
    let s_hit = r.total;
    console.log("Hit result: " + r.total);

    let r2 = new Roll("(@dice)d10cs>=(@diff)", {
      dice: target_hitDice,
      diff: target_difficulty
    });
    r2.roll();
    let s_dodge = r2.total;
    console.log("Dodge result: " + r2.total);

    let dist_base = 30; //in px
    let svg_x_max = 299; //in px
    let svg_y_max = 633; //in px

    let s = s_hit - s_dodge;
    console.log("Successes: " + s);
    if (s < 1) {
      console.log("Miss");
      //TODO: Where to put the crosshair??
      return;
    }
    
    //Cover calculation
    if(target_cover > 0){
      let coverChance_adjusted = target_cover-s*5;
      let coverCheck = Math.floor((Math.random() * 100) + 1);
      if(coverCheck <= coverChance_adjusted){
        console.log("Hit Cover");
        return;
      }
    }

    //Determine point that was hit
    let ranAngle = Math.random() * Math.PI * 2;
    let heat_adj = Math.max(0, weapon_heat - player_composure);
    let dist = 0;

    dist = Math.max(0, (5 - s + heat_adj) * dist_base);

    let currentUser = game.users.current;
    let hit_location = AimingDialogue.getRandomPoint(currentUser.flags.tac.aimPosition.x, currentUser.flags.tac.aimPosition.y, dist, 0, 2*Math.PI);

    console.log("Hit Position: " + hit_location.x + " " + hit_location.y);
    let hitLocation_name = AimingDialogue.getHitLocation(hit_location.x, hit_location.y);
    
    document.querySelector("#hitBoi").style = `top: ${hit_location.y-2}px; left: ${hit_location.x-2}px`;
    
    
    if(weapon_increaseHeat) weapon_heat++;
    if(weapon_heat > 10) weapon_heat = 10;
    let hitList = [];
    let b = 1;
    for(;b < bullets; b++){
      
      //Calculate new position from recoil
      let recoil_adj = Math.max(0, weapon_recoil - player_strength);
      heat_adj = Math.max(0, weapon_heat - player_composure);
      let recoil_dist = (recoil_adj + heat_adj + 1) * dist_base * (target_distance+1);
      console.log("Recoil Dist. " + recoil_dist);
      hit_location = AimingDialogue.getRandomPoint(hit_location.x, hit_location.y, recoil_dist, 7*Math.PI/4, 5*Math.PI/4);
      
      if (hit_location.x <= 1) break;
      else if (hit_location.x >= 298) break;
      if (hit_location.y <= 1) break;
      else if (hit_location.y >= 632) break;
      
      hitList.push({x: hit_location.x, y: hit_location.y});
      if(weapon_increaseHeat) weapon_heat++;
      if(weapon_heat > 10) weapon_heat = 10;
    }
    console.log(bullets-b + " bullets wasted");
    let e = 1;
    console.log("Hit Locations: ");
    hitList.forEach(element => {
      console.trace(element);
      
      setTimeout(() => { AimingDialogue.setPoint(element.x, element.y); }, e*1000);
      e++;
      //document.querySelector("#hitBoi").style = `top: ${hit_location.y-2}px; left: ${hit_location.x-2}px`;
    });
    console.log("----------");

    
    if(weapon_increaseHeat && weapon) weapon.update({"system.heat": weapon_heat});
    
    

    let damageMod = 0;

    if (hitLocation_name) {
      damageMod = CONFIG.damageModifiers[hitLocation_name];
      if (!damageMod) damageMod = 0;
    } else {
      hitLocation_name = "???";
    }

    console.log("Hit " + hitLocation_name + " (" + damageMod + "x damage)");
    
    
    if(weapon_size > 0){
      let dist_split = weapon_size/10;

      for(let k = 1; k <= 359; k++){
        for(let h = 1; h <= 10; h++){
          let distTest = dist_split*h;
          let testAngle = k/360 * Math.PI*2;
          let testX = Math.floor(hit_xTransf + Math.cos(testAngle)*distTest);
          let testY = Math.floor(hit_yTransf + Math.sin(testAngle)*distTest);
          if(testX < 0 || testX > svg_x_max || testY < 0 || testY > svg_y_max) continue; 
          let testTarget = document.elementFromPoint(testX, testY);
          let testLocation_name = testTarget.classList[0];
          damageMod += CONFIG.damageModifiers[testLocation_name];
        }
      }
    }


    let damage_final = Math.max(0, Math.floor(damageMod * weapon_damage - Math.abs(target_armour - weapon_penetration)));

    console.log("Damage: " + damage_final);

  }


  activateListeners(html) {
    super.activateListeners(html);

    let currentUser = game.users.current;
    if (currentUser.flags.hasOwnProperty('tac')) {
      if (currentUser.flags.tac.hasOwnProperty('aimPosition')) {
        const aimPosition = currentUser.flags.tac.aimPosition;
        document.querySelector("#aimLocation").style = `top: ${aimPosition.y-2}px; left: ${aimPosition.x-2}px`;
      }
    }

    console.trace(game);


    document.querySelector('#HITME').addEventListener('mousedown', event => {

      let currentUser = game.users.current;



      //if(!flags.hasOwnProperty('tac')) flags.update({"tac": {}});
      currentUser.update({
        "flags.tac.aimPosition": {
          x: event.offsetX,
          y: event.offsetY
        }
      });

      document.querySelector("#aimLocation").style = `top: ${event.offsetY-2}px; left: ${event.offsetX-2}px`;



    });
  }









}