


/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
 export const preloadHandlebarsTemplates = async function () {
  // Define template paths to load
  const templatePaths = [
    // Actor Sheet Partials
    "systems/tac/templates/actors/parts/actor-inventory.hbs",
    "systems/tac/templates/actors/parts/actor-attributes.hbs"
  ];

  // Load the template parts
  return loadTemplates(templatePaths);
};
export const registerHandlebarsHelpers = function () {
  Handlebars.registerHelper('largerOne', function (value) {
    return value > 1;
  });
  Handlebars.registerHelper('isMagCol', function (value) {
    return value === "Magazine";
  });
  Handlebars.registerHelper('convertRangeFirearm', function (value) {
    let range = CONFIG.rangeValues_firearm.find(ele => ele.value == value);
    if(!range) return "ERROR";
    return range.name;
  });
  Handlebars.registerHelper('convertRangeMelee', function (value) {
    let range = CONFIG.rangeValues_melee.find(ele => ele.value == value);
    if(!range) return "ERROR";
    return range.name;
  });
  Handlebars.registerHelper('convertHeat', function (value) {
    return Math.clamped(value*10,0,100);
  });
  
  Handlebars.registerHelper('convertBase', function (value, base) {
    return value !== base ? "(" + value + ")" : "";
  });

  Handlebars.registerHelper('chooseNum', function (value1, value2) {
    return Number.isInteger(value1) ? value1 : Number.isInteger(value2) ? value2 : 0;
  });

  Handlebars.registerHelper('posneg', function (value, comp) {
    return value >= comp ? "positive" : "negative";
  });

  Handlebars.registerHelper('repeat', function(n, block) {
    var accum = '';
    for(var i = 0; i < n; ++i)
        accum += block.fn(i);
    return accum;
});
}