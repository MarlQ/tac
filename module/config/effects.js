  /**
   * Effect Properties:
   * 
   * type:
   * - trait_buff : increases/decreases trait [trait] by [value]
   * - trait_buff_category : increases/decreases all traits in [category] (e.g. 'attributes_physical') by [value]
   * - condition_on_damage : inflicts [condition] on any damage
   * - clarity_tick : receive [value] clarity damage/heal every tick in combat (counter does not decrease when using the wait action if [stopOnWait] is true)
   * - health_tick : the same, but for health
   * 
   * severityScaling: true/false (default: true) -- determines whether the trait should scale linearly with severity
   * 
   * Condition Properties:
   * 
   * weaker: array of conditions which will be replaced by this condition
   * worse: array of conditions which will cause this condition to not apply
   * next: a condition this condition will transform into when it is re-applied
   * lapse: a condition this condition will transform into when it lapses (clicked, or duration expires)
   * stopOnWait: stops the condition from ticking when using the Wait action in combat
   * duration: amount of ticks in combat before the condition expires
   * (TODO: Implement) severity_max: maximum amount of severity for the condition. If the number is higher than 1, the [value] field for every effect is multiplied by the current amount of severity (atleast 1), except for effects specifying [noStacking] as true
   * 
   * type:
   * - short-term: (default) cleansed by short rest - #fedf00
   * - medium-term: cleansed by medium rest - #fe7500
   * - long-term: #fe0100
   * - good-short-term: #26ccee
   * - good-medium-term: #34ca6a
   * - good-long-term: #43ee26
   * 
   *
   * 
   */

export const CONDITIONS = {
    'concussion': {
      effects: [
        {type: 'trait_buff', trait: "attributes_physical.agility", value: -2},
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: -2},
      ],
    },
    'blinded': { // TODO: Make character see less, based on severity
      effects: [
        {type: 'trait_buff', trait: "attributes_mental.awareness", value: -5}, 
        {type: 'trait_buff', trait: "attributes_physical.dexterity", value: -2} // TODO: Problematic
      ],
    },
    'scarred': {
      effects: [
        {type: 'trait_buff', trait: "attributes_social.charisma", value: -1}
      ],
      type: 'injury', //TODO: should it be permanent?
      stacking: true // FIXME: unsure...
    },
    /* 'fatigue': {
      effects: [
        {type: 'trait_buff', trait: "attributes_physical.dexterity", value: -1}
      ]
    },
    'irritability': {
      effects: [
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -1},
        {type: 'trait_buff', trait: "attributes_social.composure", value: -2}
      ]
    }, */
    /* 'amnesia': {
      effects: [
        {type: 'trait_buff_category', category: "skills_mental", value: -1}
      ], 
      type: "severe"
    },
    'apathy': {
      effects: [
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -3},
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: -1}
      ], 
      type: "severe"
    }, */
    // 'broken_ribs': {
    //   effects: [
    //     {type: 'trait_buff', trait: "attributes_physical.constitution", value: -2},
    //     {type: 'trait_buff', trait: "skills_physical.acrobatics", value: -1},
    //     {type: 'trait_buff', trait: "skills_physical.athletics", value: -1}
    //   ]
    // },
    // 'injured_guts': {
    //   effects: [ 
    //     {type: 'trait_buff', trait: "attributes_social.composure", value: -2},
    //     {type: 'trait_buff', trait: "attributes_mental.reolve", value: -2},
    //   ]
    // },
    'bleeding': {
      effects: [{type: "clarity_tick", value: -1},
                {type: "health_tick", value: -1}],
      stopOnWait: true, //TODO: should be in effects..
      type: 'debilitation'
    },
    'pain': {
      effects: [{type: "dicepool", value: -1}],
      type: 'debilitation'
    },
    'arm_right_injury': { // TODO: Rework arm conditions
      effects: [
        {type: 'trait_buff', trait: "skills_physical.melee", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.marksmanship", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.unarmed", value: -1} 
      ],
    },
    'arm_left_injury': { // TODO: Rework arm conditions
      effects: [
        {type: 'trait_buff', trait: "skills_physical.melee", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.marksmanship", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.unarmed", value: -1} 
      ],
    },
    'leg_right_injury': { // TODO: Rework these
      effects: [
        {type: 'trait_buff', trait: "skills_physical.acrobatics", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.athletics", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.stealth", value: -1},
        {type: 'trait_buff', trait: "skills_physical.melee", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.unarmed", value: -1}, 
      ],
    },
    'leg_left_injury': { // TODO: Rework these
      effects: [
        {type: 'trait_buff', trait: "skills_physical.acrobatics", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.athletics", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.stealth", value: -1},
        {type: 'trait_buff', trait: "skills_physical.melee", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.unarmed", value: -1}, 
      ],
    },
    'al-60': { // TODO: REWORK (al-60 -> burnout -> addiction, with severities)
      effects: [
        {type: 'trait_buff', trait: "attributes_physical.dexterity", value: 2}, 
        {type: 'trait_buff', trait: "attributes_physical.agility", value: 1}, 
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: 2},
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -2},
        {type: 'trait_buff', trait: "other_traits.action_speed", value: 2}
      ],
      type: "good-short-term",
      duration: 60,
      worse: ['al-60 overdose'],
      next: 'al-60 overdose',
      lapse: 'al-60 aftershock'
    },
    'al-60 overdose': { // TODO: Effects, Duration
      effects: [
        {type: "clarity_tick", value: -1},
        {type: 'trait_buff', trait: "attributes_physical.dexterity", value: 3}, 
        {type: 'trait_buff', trait: "attributes_physical.agility", value: 2}, 
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: 3},
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -3},
        {type: 'trait_buff', trait: "other_traits.action_speed", value: 3}
      ],
      type: "good-short-term",
      duration: 100,
      weaker: ['al-60'],
      lapse: 'al-60 burnout'
    },
    'al-60 burnout': {
      effects: [
        {type: 'trait_buff', trait: "attributes_physical.dexterity", value: -3}, 
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: -2}, 
        {type: 'trait_buff', trait: "attributes_physical.constitution", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.alertness", value: -3},
        {type: 'trait_buff', trait: "other_traits.action_speed", value: -4}
      ],
      type: "medium-term",
      lapse: 'al-60 abuse'
    },
    'al-60 aftershock': {
      effects: [
        {type: 'trait_buff', trait: "attributes_physical.dexterity", value: -2}, 
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: -1}, 
        {type: 'trait_buff', trait: "skills_physical.alertness", value: -2},
        {type: 'trait_buff', trait: "other_traits.action_speed", value: -2}
      ],
      type: "medium-term",
      lapse: 'al-60 long-term'
    },
    'al-60 long-term': {
      effects: [
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -1}
      ],
      worse: ['al-60 abuse', 'al-60 addiction'],
      next: 'al-60 abuse',
      type: "long-term",
      aggregates: true // TODO: Eh...
    },
    'al-60 abuse': {
      effects: [
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -2},
        {type: 'trait_buff', trait: "attributes_social.charisma", value: -1},
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: -1}
      ],
      weaker: ['al-60 long-term'],
      worse: ['al-60 addiction'],
      type: "long-term"
    },
    'al-60 addiction': {
      effects: [
        {type: 'trait_buff', trait: "attributes_social.empathy", value: -4},
        {type: 'trait_buff', trait: "attributes_social.charisma", value: -2},
        {type: 'trait_buff', trait: "attributes_mental.intuition", value: -1}
      ],
      weaker: ['al-60 long-term', 'al-60 abuse'],
      type: "long-term",
    },
    /* 'ed_weaponized': {
      effects: [{type: "dicepool", value: -3}],
      weaker: ["ache"],
      worse: ["pain", "agony"],
      getsWorse: false
    }, */
    'ed_weaponized_ammunition': {
      effects: [
        {type: 'condition_on_damage', condition: 'pain'}
      ],
    },
    'unreliable': {
      effects: [

      ],
      type: "severe",
    },
    'armor-shred': {
      effects: [
        {type: 'trait_buff', trait: "other_traits.armor_modifier", value: -1}
      ],
      type: "debilitation"
    },
    'crippled': {
      effects: [
        {type: 'trait_buff', trait: "other_traits.action_speed", value: -1},
        {type: 'trait_buff', trait: "other_traits.movement_speed_multiplier", value: -0.5, severityScaling: false}
      ],
      type: "debilitation",
      stacking: true
    }
  }

  export const WEAPON_TRAITS_GOOD = [
    'ed_weaponized_ammunition',
  ]

  export const WEAPON_TRAITS_BAD = [
    'pain',
  ]

  export const DRUG_CONDITIONS = [
    'al-60'
  ]

  // Weights for crit injuries for each body part
  export const CRIT_INJURIES = {
    head: {
      all: {
        "bleeding": 1,
        "blurry_vision": 1,
        "pain": 1,
        "scarred_face": 1,
      },
      crushing: {
        "broken_nose": 1,
        "scarred_face": 0,
        "concussion": 1
      }
    },
    torso: {
      all: {
        "bleeding": 2,
        "injured_guts": 1,
        "pain": 1,
        /* "heart_contusion": 1 */
      },
      crushing: {
        "bleeding": 0,
        "broken_ribs": 1,
      }
    },
    arm_right: {
      all: {
        "arm_right_contusion": 2,
        "hand_right_injured": 2,
        "bleeding": 1,
        "pain": 1,
      },
      crushing: {
        /* "shoulder_right_dislocated": 2, */
        "bleeding": 0
      }
    },
    arm_left: {
      all: {
        "arm_left_contusion": 2,
        /* "hand_left_injured": 2, */
        "bleeding": 1,
        "pain": 1,
      },
      crushing: {
        /* "shoulder_left_dislocated": 2, */
        "bleeding": 0
      }
    },
    grace: {
      all: {
        "ache": 1
      }
    },
    leg_right: {
      all: {
        "leg_right_contusion": 1,
        "bleeding": 1,
        "pain": 1,
      },
      crushing: {
        "bleeding": 0
      }
    },
    leg_left: {
      all: {
        "leg_left_contusion": 1,
        "bleeding": 1,
        "pain": 1,
      },
      crushing: {
        "bleeding": 0
      }
    }
  }

