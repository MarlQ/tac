export const TALENT_TREES = {
    shotguns: {
      name: "Shotguns",
      visible_condition: "character_android", //TODO: Just an example
      nodes: { 
        break_armor: { // TODO: Implement 
          name: "Break Armor",
          levels: 2,
          requires: 'shred',
          unlock: "Skill_Firearm_>_3|Attribute_Dexterity_>_2",
          description: [
            "Shred now inflicts the Armor Shred condition, which reduces the target's armor coverage by 5 per stack. Applies <span class='text-highlight'>1</span> stack.",
            "Shred now inflicts the Armor Shred condition, which reduces the target's armor coverage by 5 per stack. Applies <span class='text-highlight'>2</span> stacks."
          ],
          y: 2,
          x: 2
        },
        shred: { // TODO: Implement, requires cover (walls?) which is permantently reduced if hit
          name: "Shred",
          requires: 'in_the_action',
          y: 2,
          x: 1,
          description: "<span class='text-highlight'>[Active Skill]</span> Reduces the cover strength of the target by one step."
        },
        careful_hunter: {
          name: "Careful Hunter",
          requires: 'in_the_action',
          description: "Decreases the likeliness of accidentally hitting friendly targets with shotguns by 50% ('friendly' targets are any characters with the same disposition as the attacker).",
          x: 1,
          y: 1,
          rerollChance: 0.5
        },
        crippling_shot: { // TODO: Implement 
          name: "Crippling Shot",
          requires: 'shotgun_warrior',
          unlock: "Skill_Firearm_>_3|Attribute_Dexterity_>_2",
          description: `<span class='text-highlight'>[Active Skill]</span> An attack which inflicts the Crippled condition on the target, decreasing the target's action speed by 1 per stack, and halving its movement speed. Applies 1 stack (max. 5).
          Uses the firearm's single shot setting.`,
          x: 2
        },
        ripe_target: { // TODO: Implement 
          name: "Ripe Target",
          levels: 2,
          requires: 'crippling_shot',
          description: [
            "Crippling shot now also stuns the target for <span class='text-highlight'>2</span> ticks. This may be reduced by the target's action speed.",
            "Crippling shot now also stuns the target for <span class='text-highlight'>4</span> ticks. This may be reduced by the target's action speed.",
          ],
          x: 3
        },
        shotgun_warrior: { // TODO: Implement, requires melee combat detection (show a ring around the subject if someone is penalized - the range could also be improved by talents)
          name: "Shotgun Warrior",
          requires: 'in_the_action',
          description: "Removes the distracted penalty from using a shotgun in melee combat.",
          x: 1
        },
        in_the_action: { // TODO: Implement, requires new reload mechanics
          name: "In the Action",
          description: "Reloading magazine-based shotguns requires only 2AP. Cycling ('pumping') a pump-action shotgun costs no AP. Half of a shotgun's shells can now be reloaded with 1 AP, instead of a single shell.",
          reloading_shotgun_ticks: 2, // TODO:
          cycling_shotgun_ticks: 0, // TODO: Implement
          reloading_shotgun_bullets_modifier: 2 // TODO: Implement
        },
      }
    }
  }