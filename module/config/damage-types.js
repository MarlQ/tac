export const DAMAGE_TYPES = {
    direct: { // damage type used for effects, etc.
      name: "Direct",
      health: 1.0,
      clarity: 0.0
    },
    exhaustion: { // damage type used for effects, etc.
      name: "Exhaustion",
      health: 0.0,
      clarity: 1.0
    },
    gunshot: {
      name: "Gunshot",
      health: 1.0,
      clarity: 0.5,
      armor: "ballistic"
    },
    crushing: {
      name: "Crushing",
      health: 0.75,
      clarity: 1.0,
      armor: "padding" , blunt, impact
    },
    slashing: {
      name: "Slashing", cut, slice, edge ... arrow
      health: 1.0,
      clarity: 0.8,
      armor: "mesh"
    },
    piercing: {
      name: "Piercing", sharp, pierce,puncture
      health: 1.0,
      clarity: 0.65,
      armor: 
    } chopping
  };

  /**
  * Ballisic: grey
  * Impact: Brown
  * Cut: Yellow
  * Puncture: Light green
  * Thermal Orange
  * 
  */

  
