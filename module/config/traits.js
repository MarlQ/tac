











export const WEAPON_TRAITS = {
    bladed: {
        weapontype: "melee",
        restricted: ["sword", "axe"],
        cost: 1,
        levels: [
            {
                damageModifier: 0.8
            },
            {
                damageModifier: 1.0
            },
            {
                damageModifier: 1.2
            }
        ],
    },
    burstFire: {
        weapontype: "firearm",
        cost: 1,
        levels: [
            {
                numBullets: 0.8
            },
            {
                numBullets: 1.0
            },
            {
                numBullets: 1.2
            }
        ],
    },
    'ed_weaponized_ammunition': {
        weapontype: 'ammo',
        cost: 1,
        levels: [
            {
                type: 'condition_on_damage',
                condition: 'pain',
                onEveryHit: true,
                chance: 0.5
            }
        ]
    },
}












export const WEAPON_TRAITS_GOOD = [
    'ed_weaponized_ammunition',
]

export const WEAPON_TRAITS_BAD = [

]
