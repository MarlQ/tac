import { ItemTac } from "./item.js";
import { ReloadDialogue } from "./dialogue-reload.js";
import { createShortActionMessage } from "./chat.js";

export class QuickAttackDialogue extends Application {
  constructor(actor, ...args) {
    super(...args);
    this.actor = actor;
    this.options.title = this.actor.name + " - Quick Attack";
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue"],
  	  template: "systems/tac/templates/dialogues/dialogue-quickAttack.html"
    });
  }
  
  getData() {
    const data = super.getData();

    data.weaponList = this.actor.items.filter(element => (element.type === "firearm" || element.type === "melee") && element.data.equipped);
    
    return data;
  }
  
  activateListeners(html) {
    super.activateListeners(html);
    
      html.find('.selectWeapon').click(event => {
       event.preventDefault();
       event.stopPropagation();
        let parent = $(event.currentTarget),
            menu = this._contextMenu.menu;

        // Remove existing context UI
        $('.context').removeClass("context");

        // Close the current context
        if ( $.contains(parent[0], menu[0]) ) this._contextMenu.close();

        // If the new target element is different
        else {
          this._contextMenu.render(parent);
          ui.context = this._contextMenu;
        }
     });
    
     html.find('.item-reload').click(ev => this._onItemReload(ev));

    this._createContextMenu(html);
  }
  
  _createContextMenu(html) {
      this._contextMenu = new ContextMenu(html, ".selectWeapon", ItemTac.getContextMenuAttacks(this.actor, this));
    }
  
  async _onItemReload(ev) {
    const weaponId = event.currentTarget.closest(".item-reload").dataset.itemId;
    const weapon = this.actor.getEmbeddedDocument("Item", weaponId);
    if(weapon.system.magazine){
      createShortActionMessage("Ejects magazine", this.actor);
      //Eject
      ev.target.classList.remove("reloaded");
      
      //Add ejected ammo back into the inventory
      const ammoData = weapon.system.magazine.data;
      const inventory = this.actor.items;
      
      const index = inventory.findIndex(ele => (ele.name === ammoData.name) && (ele.type === ammoData.type));
      if(index > -1) inventory[index].update({'system.quantity': 
                                              inventory[index].system.quantity
                                              +ammoData.data.quantity}); //Add ammo to existing mag
      else this.object.createOwnedItem(ammoData); //Add new ammo item

      await weapon.update({"system.magazine": null}); //Remove mag from weapon
      this.render();
    }
    else{
      //Open reload menu
      let ammoList = new ReloadDialogue(weapon, ev.target, this);
      ammoList.render(true);
    }
    
  }
}