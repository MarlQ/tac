/**
 * Override and extend the basic :class:`Actor` implementation
 */
 import { DiceRollerDialogue } from "./dialogue-diceRoller.js";
 import { createShortActionMessage } from "./chat.js";
import { TAC } from "./config.js";
export class ActorTac extends Actor {
  showingText = false;
  floatingTextQueue = [];
  prepareData() {
    super.prepareData();

    // base: stat visible on sheet, before modifiers
    // raw: stat after all modifiers
    // value: stat after all modifiers, with minimum and maximum

    // Get the Actor's data object
    const systemData = this.system;

    // Initialise attribute/skill values
    const attributes = [systemData.attributes_physical, systemData.attributes_mental, systemData.attributes_social, systemData.skills_physical, systemData.skills_social, systemData.skills_mental, systemData.other_traits];
    attributes.forEach(attribute => Object.values(attribute).forEach(trait => {
      trait.value = trait.base;
      trait.raw = trait.base;
    }));

    //Get effects from items (modifiers to any attribute/skill/etc.)
    for (let item of this.items) {
      if (item.buffs && item.system?.effectsActive) { // only look at active effects

        let traitBuffs = [];
        item.buffs.forEach(effect => {

          // Expand category buffs
          if(effect.type === 'trait_buff_category') {
            traitBuffs = traitBuffs.concat(Object.keys(CONFIG.TAC[effect.category]).map(skill => { return {type: 'trait_buff', trait: effect.category + '.' + skill, value_stacked: effect.value}}));
          }
          else if(effect.type === 'trait_buff'){
            traitBuffs.push(effect);
          }
        });

        traitBuffs.forEach(effect => {
          const trait = effect.trait.split('.').reduce((o,i) => o[i], systemData);
          if(trait) {
            trait.raw = trait.raw + effect.value; 
            trait.value = trait.raw;
          }
        });
      }
    }

    for (const [key, value] of Object.entries(systemData.other_traits)) {
      if(key.endsWith('multiplier')) value.multiplier = true;
    }

    // Clamp standard attributes
    [systemData.attributes_physical, systemData.attributes_mental, systemData.attributes_social, systemData.skills_physical, systemData.skills_social, systemData.skills_mental].forEach(attribute => Object.values(attribute).forEach(trait => {
      trait.value = Math.clamped(trait.raw, 0, CONFIG.TAC.attribute_maximum);
    }));

    const strength = systemData.attributes_physical.strength.value;

    systemData.run_action_penalty = -2;
    systemData.run_attacker_penalty = -2;
    systemData.sprint_attacker_penalty = -2;
    systemData.other_traits.action_speed.value += systemData.attributes_mental.intuition.value; // 3 ticks
    systemData.run = 2 * systemData.attributes_physical.agility.value * systemData.other_traits.movement_speed_multiplier.value; // 3 ticks
    systemData.sprint = 5 * systemData.attributes_physical.agility.value + 3 * systemData.attributes_physical.strength.value * systemData.other_traits.movement_speed_multiplier.value; // 8 ticks
    systemData.walk = systemData.attributes_physical.agility.value * systemData.other_traits.movement_speed_multiplier.value;
    systemData.initiative = +systemData.attributes_mental.intuition.value;

    systemData.health.max = CONFIG.TAC.health_base 
                                  + CONFIG.TAC.healthIncrease_constitution * systemData.attributes_physical.constitution.value 
                                  + CONFIG.TAC.healthIncrease_composure * systemData.attributes_social.composure.value;

    systemData.health.max_cur = systemData.health.max - systemData.health.damage_max;
    
    systemData.clarity.max = CONFIG.TAC.clarity_base 
                            + CONFIG.TAC.clarityIncrease_resolve * systemData.attributes_mental.resolve.value 
                            + CONFIG.TAC.clarityIncrease_composure * systemData.attributes_social.composure.value;

    systemData.clarity.max_cur = systemData.clarity.max - systemData.clarity.damage_max;

    systemData.health.value = Math.clamped(systemData.health.max - systemData.health.damage, 0, systemData.health.max);
    systemData.clarity.value = Math.clamped(systemData.clarity.max - systemData.clarity.damage, 0, systemData.clarity.max);

    // TODO:
    systemData.natural_armor = {
      head: {
        crushing: 10,
        ballistic: 0,
        slashing: 0,
        piercing: 5
      },
      torso: {
        crushing: strength * 4,
        ballistic: 0,
        slashing: 0,
        piercing: 0
      },
      arms: {
        crushing: strength * 3,
        ballistic: 0,
        slashing: 0,
        piercing: 0 
      },
      legs: {
        crushing: strength * 4,
        ballistic: 0,
        slashing: 0,
        piercing: 0  
      },
    };

    // TODO: Add bonus from equipped bag
    systemData.weightMax_light = 1 * (systemData.attributes_physical.strength.value + systemData.attributes_physical.constitution.value); 
    systemData.weightMax_medium = 2 * (systemData.attributes_physical.strength.value + systemData.attributes_physical.constitution.value); 
    systemData.weightMax_heavy = 3 * (systemData.attributes_physical.strength.value + systemData.attributes_physical.constitution.value); 
    systemData.weightMax_limit = 4 * (systemData.attributes_physical.strength.value + systemData.attributes_physical.constitution.value); 
    /* data.weight = 0;
    this.items.forEach(item => {
      if(this.isInBags(item) || this.isEquipped(item)) data.weight += item.system.weight;
    }); */
    systemData.weight = this.items.reduce((prev, cur) => {
      return this.isInBags(cur) || this.isEquipped(cur) ? prev + cur.system.weight : prev;
    }, 0); 
    //data.weight = Math.floor(data.weight);
    // TODO: Round down or up?

    

    if(systemData.weight > systemData.weightMax_heavy) systemData.encumbered = 'heavy';
    else if(systemData.weight > systemData.weightMax_medium) systemData.encumbered = 'medium';
    else if(systemData.weight > systemData.weightMax_light) systemData.encumbered = 'light';
    else systemData.encumbered = 'none';
    Object.entries(CONFIG.TAC.ENCUMBRANCE_MODIFIERS[systemData.encumbered]).forEach(effect => {
      const trait = effect[0].split('.').reduce((o,i) => o[i], systemData);
      if(trait) {
        trait.raw = trait.raw + effect[1]; 
        trait.value = Math.clamped(trait.raw, 0, CONFIG.TAC.attribute_maximum);
      }
    })

    // General dice pool modifier
    systemData.dicePoolChange = this.items.reduce((item_acc, item) => {
      let change = 0;
      if(item.type === "condition") {
        change += item.buffs.reduce((effect_acc, effect) => {
          return effect_acc + (effect.type === "dicepool" ? effect.value : 0);
        }, 0);
      }
      return item_acc + change;
    }, 0);


    const typeOrder = new Map ([
      ["firearm", 0], 
      ["melee", 1],
      ["armour", 2],
      ["ammo", 3],
      ["consumable", 4]
    ]);

    systemData.inventory = [];
    systemData.bagSlots = [];
    this.items.forEach( item => {
      if(typeOrder.has(item.type)) {
        if(this.isInBags(item)) systemData.bagSlots.push(item);
        else if(!this.isEquipped(item)) systemData.inventory.push(item);
      }
    } );

    Object.values(systemData.equipmentSlots).forEach(slot => {
      if(slot.itemId) {
        if(this.items.get(slot.itemId)) {
          slot.item = this.items.get(slot.itemId);
          slot.icon = slot.item.img;
        }
      }
    });
    
    systemData.inventory.sort((a,b) => a.type !== b.type ? typeOrder.get(a.type)  - typeOrder.get(b.type) : a.name.localeCompare(b.name) );
    const slots = 4 * 7;
    systemData.emptySlots = slots - systemData.inventory.length;
    systemData.emptyBagSlots = slots - systemData.bagSlots.length;

    // Progression
    systemData.progression.points_attributes_spent = Object.values({...systemData.attributes_physical, ...systemData.attributes_social, ...systemData.attributes_mental}).reduce((prev, cur) => prev+cur.base-1, 0);
    systemData.progression.points_skills_spent = Object.values({...systemData.skills_physical, ...systemData.skills_social, ...systemData.skills_mental}).reduce((prev, cur) => prev+cur.base-1, 0);

    systemData.progression.points_attributes = CONFIG.TAC.ATTRIBUTE_POINTS_BASE + CONFIG.TAC.ATTRIBUTE_POINTS_PER_LEVEL * systemData.progression.level - systemData.progression.points_attributes_spent;
    systemData.progression.points_skills = CONFIG.TAC.SKILL_POINTS_BASE + CONFIG.TAC.SKILL_POINTS_PER_LEVEL * systemData.progression.level - systemData.progression.points_skills_spent;
    systemData.progression.points_talents = CONFIG.TAC.TALENT_POINTS_BASE + CONFIG.TAC.TALENT_POINTS_PER_LEVEL * systemData.progression.level - systemData.progression.points_talents_spent;

    systemData.progression.exp_needed = this.getExpToNextLevel(this.system.progression.level);
  }

  addExp(exp) {
    console.log("ADDED EXP", exp);
    let curExp = this.system.progression.exp + +exp;
    let curLvl = this.system.progression.level;

    while(curExp >= this.getExpToNextLevel(curLvl)) {
      curExp -= this.getExpToNextLevel(curLvl);
      curLvl++;
    }

    return this.update({
      'system.progression.level': curLvl,
      'system.progression.exp': curExp
    })
  }

  levelUp() {
    this.update({'system.progression.level': this.system.progression.level + 1});
  }  

  getExpToNextLevel(lvl) { //TODO: Move somewhere else?
    return (lvl + 1 ) * 100;
  }

  // Activates per-tick effects during combat
  async combatTick(tickPrev=1, tickNext=2, waiting=false) {
    console.log("Combat Tick ", tickPrev, tickNext, waiting)

    const tickDelta = (tickNext - tickPrev);

    let conditions = this.items.filter(item => item.type === "condition");//.map(condition => { return {tick: condition.system.tick, effects: condition.system.effects}; });

    let totalClarityChange = CONFIG.TAC.CLARITY_PER_TICK * tickDelta;
    let totalHealthChange = 0;

    for(const condition of conditions)  {
      
      if(waiting && condition.system.stopOnWait) return;

      const effectDelta = Math.min(condition.system.remainingDuration, tickDelta); //FIXME: Test
      await condition.update({'system.tick': condition.system.tick + tickDelta});

      for(const effect of condition.buffs) {

        // Ticking effects
        if(effect.type === 'clarity_tick' || effect.type === 'health_tick') { // FIXME: Calculate correctly for duration

          if(effectDelta) {
            if(effect.type === 'clarity_tick') totalClarityChange += effect.value  * effectDelta;
            if(effect.type === 'health_tick') totalHealthChange += effect.value * effectDelta;
          }
        }
      }

      // Duration runs out
      if(condition.system.duration && condition.system.remainingDuration <= 0) {
        await this.lapseCondition(condition.name);
      }
    }

    if(totalClarityChange) {
      createShortActionMessage("Clarity changes by " + totalClarityChange, this);
      this.applyEffect({name: 'attack_clarity', damage: -totalClarityChange, noWound: true});
    }

    if(totalHealthChange) {
      createShortActionMessage("Health changes by " + totalHealthChange, this);
      this.applyEffect({name: 'attack', damage: -totalHealthChange, noWound: true});
    }

    this.adjustWeaponHeat( -(tickDelta) * CONFIG.TAC.HEAT_DECREASE_PER_TICK);
  }

  async startCombat() {
    this.items.filter(item => item.type === "condition").forEach(condition => {
      if(!condition.tick) condition.update({'system.tick': 1});
    })
  }

  applyActionCost(ticks_cost, clarity_cost, actor_token) {
    // Increase Ticks if the attacker is in combat
    const activeCombat = game.combats.find(c => c.active);
    if(activeCombat) {
      const combatant = activeCombat.combatants.find(c => {
        if(actor_token) return c.tokenId === actor_token.id;
        else return c.actorId === this.id;
      });
      if(combatant) {
        activeCombat.increaseTicks(combatant, ticks_cost);
      }
    }
    this.applyDamage(clarity_cost, 'clarity', 'exhaustion');
  }


  // Called when a combat is deleted
  endCombat() {
    // Reset weapon heat.
    this.adjustWeaponHeat(-CONFIG.TAC.HEAT_MAXIMUM);
  }

  adjustWeaponHeat(change) {
    this.items.forEach(item => {
      item.adjustWeaponHeat(change);
    });
  }

  async repairArmour() {
    console.log(this)

    for(let slot of Object.values(this.system.equipmentSlots)) {
      if(slot.item && 'durability' in slot.item.system) {
        await slot.item.update({'system.durability': 100});
      }
    }
  }
  
  skillCheckDialogue(dicepool=[], type) {
    let dice = dicepool.reduce((acc, ele) => +acc + +ele.value, 0);
    console.log(dicepool);
    console.log(dice);
    
    function ucfirst(s){ return s.charAt(0).toLocaleUpperCase() + s.substring(1); }
    //let flavor = this._rollParameters.reduce((acc, ele) => {ucfirst(acc) + " + " + ucfirst(ele)}); //TODO
    let flavor = dicepool.map(x=>ucfirst(x.name)).join(' + ');

    dice += this.system.dicePoolChange;

    switch (type) {
      case 1:
          let diceRoller = new DiceRollerDialogue(dice, 6, flavor, true);
          diceRoller.render(true);
          break;
      case 2:
          break;
      case 3:
          //Quick Roll
          DiceRollerDialogue.createDiceRollMessage(dice, 6, flavor);
          break;
    }
  }

  // Draw a random entry from a weighted table,
  // where the [0] entries contain the entries
  // and [1] contain the weights
  randomEntry(table) {// TODO: move this!
    let weightSum = table.reduce((sum, cur) => sum + cur[1], 0);
    let randomNum = Math.random() * weightSum;
    let counter = 0;
    let result;
    console.log(weightSum)
    console.log(randomNum)

    table.some(ele => {
      counter += ele[1];
      if(randomNum < counter) {
        result = ele[0];
        return true;
      }
    });
    return result;
  }

  applyEffectDialogue() {
    let d = new Dialog({
      title: "Apply Condition",
      content: "<div> <span> Name </span> <input class='attribute-value' type='text' name='input.name' placeholder='No Reason'/></div>",
      buttons: {
        ok: {
          icon: '<i class="fas fa-check"></i>',
          label: "OK",
          callback: html => {
            let name = html.find(".attribute-value[name='input.name']").val();
            if (name) this.addCondition({conditionID: name});
          }
        },
        cancel: {
          icon: '<i class="fas fa-times"></i>',
          label: "Cancel"
        }
      },
      default: "cancel"
    });
    d.render(true);
  }

  async shortRest() {
    let updates = {
      'system.health.damage': Math.clamped(0, this.system.health.max-this.system.health.max_cur, this.system.health.max),
      'system.clarity.damage': Math.clamped(0, this.system.clarity.max-this.system.clarity.max_cur, this.system.clarity.max)
    };

    for(const item of this.items) {
      if(item.type === "condition" && (item.system.type === "short-term" || item.system.type === "good-short-term")) {
        await this.lapseCondition(item.name);
      }
    }

    return this.update(updates);
  }

  async longRest() {
    let updates = {
      'system.health.damage_max': 0,
      'system.clarity.damage_max': 0
    };
    await this.update(updates);

    await this.shortRest();

    for(const item of this.items) {
      if(item.type === "condition" && (item.system.type === "medium-term" || item.system.type === "good-medium-term")) {
        await this.lapseCondition(item.name);
      }
    }
  }


  /** @override */
  async modifyTokenAttribute(attribute, value, isDelta, isBar) {
    console.log("MODIFYING TOKEN ATTRIBUTE", attribute, value, isDelta, isBar)
    if ( attribute === "health" || attribute === "clarity") {
      const resource = this.system[attribute];                               ;
      const delta = isDelta ? (-1 * value) : (resource.value) - value;
      return this.applyDamage(delta, attribute, 'direct');
    }
    return super.modifyTokenAttribute(attribute, value, isDelta, isBar);
  }

  applyDamage(amount, type, damageType) {
    console.log("Applying damage", amount, type, damageType)
    if(!amount) return;
    
    let resource = this.system[type];

    let diff = resource.value - amount;
    let belowZero = 0;

    if(diff < 0) {
      belowZero = -diff;
      amount = resource.value;
    }
    if(!damageType) damageType = "direct";

    let maxDamageMod = CONFIG.TAC.DAMAGE_TYPES[damageType][type].maxDamageMod;
    let maxDamageModFull = CONFIG.TAC.DAMAGE_TYPES[damageType][type].maxDamageModFull;

    let propNameDamage = `system.${type}.damage`;
    let propNameDamageMax = `system.${type}.damage_max`;

    let newMax = Math.floor(Math.clamped(resource.damage_max 
      + amount * maxDamageMod
      + belowZero * maxDamageModFull, 0, resource.max));

    let newVal = Math.floor(Math.clamped(resource.damage + amount, resource.max-resource.max_cur, resource.max));

    let updates = {
      [propNameDamage]: newVal,
      [propNameDamageMax]: newMax
    };

    // Floating text
    const tokens = this.isToken ? [this.token?.object] : this.getActiveTokens(true);
    const label = -amount;
    const healing = amount < 0;
    const color = healing ? CONFIG.TAC.COLORS[type].heal :  CONFIG.TAC.COLORS[type].damage;
    for ( let t of tokens ) {
      if ( !t.visible || !t.renderable ) continue;
      canvas.interface.createScrollingText(t.center, label, {
        anchor: CONST.TEXT_ANCHOR_POINTS.CENTER,
        direction: healing ? CONST.TEXT_ANCHOR_POINTS.TOP : CONST.TEXT_ANCHOR_POINTS.BOTTOM,
        distance: (2 * t.h),
        fontSize: 24,
        fill: color,
        stroke: 0x000000,
        strokeThickness: 1,
        jitter: 2
      });
    }

    /* return this.update(updates); */
    const allowed = Hooks.call("modifyTokenAttribute", {
      attribute: type,
      value: amount,
      isDelta: false,
      isBar: true
    }, updates);
    return allowed !== false ? this.update(updates) : this;
  }

  async applyEffect(effectData) {
    console.log("Apply Effect")
    console.log(effectData)
    if(!effectData.name) return;

    if(effectData.name === 'attack_clarity') {
      return await this.applyDamage(effectData.damage, 'clarity', effectData.damageType);
    }

    if(effectData.name === 'attack') {

      await this.applyDamage(effectData.damage, 'health', effectData.damageType);

      if(effectData.noWound) return;

      let slotName = CONFIG.TAC.ARMOR_SLOTS[effectData.hitLocation];
      let slot = this.system.equipmentSlots[slotName];
      if(slot?.item) {
        await slot.item.update({'system.durability': Math.clamped( Math.ceil(slot.item.system.durability - effectData.damage), 0, 100)}) 
      }
      
      // Roll for wound
      let totalWoundChance = CONFIG.TAC.woundChancePerDamage * effectData.damage; // TODO

      let randomNum = Math.random() * 100;
      if(true) {
        // Roll on injury table
        

      /*  TODO:
        if(effectData.damage >= 20) { 
          // Massively increase chance for limb severing above damage 20 & Always inflict agony
          if(effectData.hitLocation + "_severed" in injuryTable) injuryTable[effectData.hitLocation + "_severed"] = 100; 
          await this.applyEffect({name: "agony"});
        } */
        const injuryTable = CONFIG.TAC.CRIT_INJURIES[effectData.hitLocation];
        const possibleInjuries = injuryTable[effectData.damageType] ? {...injuryTable.all, ...injuryTable[effectData.damageType]} : injuryTable.all;

        let result = this.randomEntry(Object.entries(possibleInjuries));
        return await this.applyEffect({name: result});
      }
    } else {
      console.warn("ApplyEffect is deprecated");
      await this.addCondition({conditionID: effectData.name});
    }
  }

  async lapseCondition(conditionName) {
    const condition = this.items.find(item => item.type === "condition" && item.name === conditionName);
    if(!condition) {
      console.error("Condition does not exist: ", conditionName);
      return;
    }
    const lapse = condition.system.lapse;
    
    await this.deleteEmbeddedDocuments("Item", [condition.id]);
    if(lapse) {
      await this.addCondition({conditionID: lapse});
    }
  }

  createFloatingText(text, bad = true) {
    const tokens = this.isToken ? [this.token?.object] : this.getActiveTokens(true);
    console.log("WAT", text)
    for ( let t of tokens ) {
      if ( !t.visible || !t.renderable ) continue;
      const floatingTextData = {
        center: t.center,
        text: text,
        options: {
          anchor: CONST.TEXT_ANCHOR_POINTS.CENTER,
          direction: bad ? CONST.TEXT_ANCHOR_POINTS.BOTTOM : CONST.TEXT_ANCHOR_POINTS.TOP,
          distance: (2 * t.h),
          fontSize: 28,
          stroke: 0x000000, // TODO:
          strokeThickness: 4,
          jitter: 0.25
        }
      };
      // Push text data to the queue
      this.floatingTextQueue.push(floatingTextData);
      // If not already showing text, start showing
      if (!this.showingText) {
        console.log("GOGO", this.showingText)
        this.showNextFloatingText();
      }
    }
  }

  async showNextFloatingText() {
    // If there are texts in the queue
    if (this.floatingTextQueue.length > 0) {
      // Set flag to true to prevent concurrent processing
      this.showingText = true;
      const { center, text, options } = this.floatingTextQueue.shift(); // Get and remove the first element from the queue
      // Show the floating text
      canvas.interface.createScrollingText(center, text, options);
      // Wait for 500ms
      console.log("WAITINGDDD", this.showingText)
      await new Promise(resolve => setTimeout(resolve, 500));
      console.log("WAITING?")
      // Continue to the next text
      this.showNextFloatingText();
    } else {
      // Queue is empty, set the flag to false
      this.showingText = false;
    }
  }

  async addMultipleConditions(conditions=[]) {
    for(const effect of conditions) {
      await this.addCondition(effect);
    }
  }


  
  /**TODO: Add functionality:
   * - default icons
   * - icons specifiable via config
   * 
   */
  async addCondition({conditionID, options, showText=true}={}) { //TODO: Add description, extended effects
    let defaults = {
      name: "condition", 
      effects: [], 
      type: "injury", 
      next: "", // TODO:
      worse: [], 
      weaker: [], 
      lapse: "", 
      aggregates: false, 
      getsWorse: true, 
      duration: 0, 
      stopOnWait: false, 
      severity: 1, 
      severity_max: 5,
      stacking: false
    };

    // Lookup condition in the config to get defaults
    if(conditionID) {
      const condition = CONFIG.TAC.conditions[conditionID];
      if(condition) {
        condition.name = conditionID;
        defaults = mergeObject(defaults, condition, {overwrite: true, insertKeys: false, inplace: false});
      }
      else {
        console.error("Condition not found: ", conditionID);
        return;
      }
    }

    options = mergeObject(defaults, options, {overwrite: true, insertKeys: false, inplace: false});

    options.severity_max = Math.max(1, options.severity_max);
    options.severity = Math.clamped(options.severity, 1, options.severity_max);
    options.duration = Math.max(0, options.duration);

    // Remove all weaker conditions
    if(options.weaker.length) { 
      const weakerConditions = this.items.filter(item => {
        return options.weaker.includes(item.name) && item.type === "condition";
      });
      const ids = weakerConditions.map(c => c.id);
      await this.deleteEmbeddedDocuments("Item", ids);
    }

    // Look for worse conditions
    if(options.worse.length) {

      // A worse condition exists. Do nothing (except if it aggregates).
      const worseCondition = this.items.find(item => {
        return options.worse.includes(item.name) && item.type === "condition";
      });
      if(worseCondition) { // FIXME
        if(options.aggregates && worseCondition.system.worse) {
          // Existing condition gets worse due to aggregation
          await worseCondition.delete();
          await this.addCondition({conditionID: worseCondition.system.worse[0]});
        }
        return;
      }

      // The condition exists already, and can get worse. Apply the next worse condition instead.
      // const existingCondition = this.items.find(ele => {
      //   return ele.name === name && ele.type === "condition";
      // });
      // if(existingCondition){
      //   if(getsWorse) {
      //     await existingCondition.delete();
      //     await this.applyEffect({name: worse[0]});
      //   }
        
      //   return;
      // }
      
    }

    // The condition exists already
    const existingCondition = this.items.find(item => {
      return item.name === options.name && item.type === "condition";
    });
    if(existingCondition) {
      const new_duration = Math.max(existingCondition.system.remainingDuration, options.duration);

      // Increase severity of existing condition
      if(existingCondition.system.severity < existingCondition.system.severity_max) {
        const newSeverity = Math.min(existingCondition.system.severity + options.severity, existingCondition.system.severity_max);
        await existingCondition.update({'system.severity': newSeverity, 'system.remainingDuration': new_duration});
        // Floating text
        if(showText) this.createFloatingText(game.i18n.localize('TAC.CONDITIONS.' + options.name) + ` (${newSeverity})`);
        return;
      }
      else {
        // Max severity reached: upgrade condition
        if(options.worse.length) {
          await existingCondition.delete();
          await this.addCondition({conditionID: worse[0], 'system.remainingDuration': new_duration});
        }
        else { // Just update duration
          await existingCondition.update({'system.remainingDuration': new_duration});

        }
        return;
      }
    }

    let str = game.i18n.localize('TAC.CONDITIONS.' + options.name);
    if(options.severity > 1) str += ` (${options.severity})`;
    
    // Floating text
    if(showText) this.createFloatingText(str);

    // Add new condition
    await this.createEmbeddedDocuments("Item", [
      {
        name: options.name, 
        type: "condition",
        img: "systems/tac/icons/conditions/" + options.name + ".svg",
        system: {
          effects: options.effects,
          effectsActive: true,
          type: options.type,
          worse: options.worse,
          weaker: options.weaker,
          next: options.next,
          lapse: options.lapse,
          duration: options.duration,
          remainingDuration: options.duration,
          stopOnWait: options.stopOnWait,
          severity: options.severity,
          severity_max: options.severity_max
        }
    }]);
  }

  addToBags(item) {
    if(this.isInBags(item)) return;
    let actorData = this.system;
    let itemData = item.system;
    if(actorData.weight + itemData.weight > actorData.maximumWeight) {
      // Item too heavy
      return false;
    }
    let bags = actorData.bags ? duplicate(actorData.bags) : [];
    bags.push(item.id)

    const updateData = {'system.bags': bags};
    this.unequip(item);
    return this.update(updateData);
  }

  removeFromBags(item) {
    let actorData = this.system;
    let bags = actorData.bags.filter(slot => slot !== item.id)
    if(!bags) return;

    const updateData = {'system.bags': bags};
    return this.update(updateData);
  }

  isEquipped(item) {
    return Object.values(this.system.equipmentSlots).some(slot => slot.itemId === item.id);
  }

  isInBags(item) {
    return this.system.bags.some(slot => slot === item.id);
  }
  
  unequip(item) {
    let equipSlot = Object.keys(this.system.equipmentSlots).find(key => this.system.equipmentSlots[key].itemId === item.id);
    if(!equipSlot) return;

    let updateData = {};
    updateData['system.equipmentSlots.' + equipSlot + '.itemId'] = "";
    console.log("Unequipped ", item, updateData)
    
    return this.update(updateData);
  }

  automaticEquip(item) {
    if(item.type === 'armour') {
      this.equip(item, item.system.slot);
    }
    else {
      // Equip to free weapon slot, otherwise equip to first weapon slot
      let freeSlot = Object.keys(this.system.equipmentSlots).find(key => this.system.equipmentSlots[key].weaponSlot && this.system.equipmentSlots[key].itemId === "");
      console.log("FREE", freeSlot)
      if(freeSlot) {
        this.equip(item, freeSlot);
      }
      else {
        let firstSlot = Object.keys(this.system.equipmentSlots).find(key => this.system.equipmentSlots[key].weaponSlot);
        console.log("FREE", firstSlot)
        if(firstSlot) {
          this.equip(item, firstSlot);
        }
        else {
          console.error("NO WEAPON SLOT FOUND??")
        }
      }

    }
  }

  equip(item, slot) { 

    if(!this.system.equipmentSlots[slot]) {
      console.error('Equipment slot ', slot, ' does not exist!')
      return;
    }

    let updateData = {};
    updateData['system.equipmentSlots.' + slot + '.itemId'] = item.id;

    this.removeFromBags(item); //FIXME: aggregate update to remove flickering
    return this.update(updateData);
  }

  getSkills() {
    const skills = [
      {
        name: "Wait",
        img: "systems/tac/icons/skills/wait.svg",
        ticks_cost: 1,
        clarity_cost: 0,
        description: "Spend one Tick doing nothing.",
        sheetMacro: true,
        condition: () => true,
        callback: (token) => {
            // Increase Ticks if the attacker is in combat
            const activeCombat = game.combats.find(c => c.active);
            if(activeCombat) {
                const combatant = activeCombat.combatants.find(c => c.tokenId === token.id)
                if(combatant) {
                    activeCombat.waitAction(combatant/* , token.actor */);
                }
            }
            
        }
      },
      {
        name: "Walk",
        img: "systems/tac/icons/skills/walk.svg",
        ticks_cost: this.getTicksCost('walk'),
        clarity_cost: this.getClarityCost('walk'),
        description: `Walk <span class='text-highlight'>${this.system.walk}m</span> as an action, or turn by more than 90 degrees. Other actions can be performed during a walk action.`,
        sheetMacro: true,
        condition: () => true,
        callback: (token) => {
            token.actor.applyActionCost( this.getTicksCost('walk'), this.getClarityCost('walk'), token);
            createShortActionMessage("Walks", token.actor);
        }
      },
      {
        name: "Run",
        img: "systems/tac/icons/skills/run.svg",
        ticks_cost: this.getTicksCost('run'),
        clarity_cost: this.getClarityCost('run'),
        description: `Run <span class='text-highlight'>${this.system.run}m</span> as an action. 
        Actions can still be performed during running, but at a <span class='text-highlight'>${this.system.run_action_penalty}</span> penalty.
        Attacks against you are at a <span class='text-highlight'>${this.system.run_attacker_penalty}</span> penalty.`,
        sheetMacro: true,
        condition: () => true,
        callback: (token) => {
            token.actor.applyActionCost( this.getTicksCost('run'), this.getClarityCost('run'), token);
            createShortActionMessage("Runs", token.actor);
        }
      },
      {
        name: "Sprint",
        img: "systems/tac/icons/skills/sprint.svg",
        ticks_cost: this.getTicksCost('sprint'),
        clarity_cost: this.getClarityCost('sprint'),
        description: `Sprint <span class='text-highlight'>${this.system.sprint}m</span> as an action.
        Attacks against you are at a <span class='text-highlight'>${this.system.sprint_attacker_penalty}</span> penalty.`,
        sheetMacro: true,
        condition: () => true,
        callback: (token) => {
            token.actor.applyActionCost( this.getTicksCost('sprint'), this.getClarityCost('sprint'), token);
            createShortActionMessage("Sprints", token.actor);
        }
      }
    ];
    /* skills.forEach(s => {
      if(!s.ticks_cost) s.ticks_cost = 0;
      if(!s.clarity_cost) s.clarity_cost = 0;
      s.clarity_cost =  Math.floor(s.clarity_cost * this.system.other_traits.clarity_cost_multiplier.value);
    }); */
    return skills.filter(ele => ele.condition());
  }









  getTalentStacks(talent) {
    if(this.system.talents == null || this.system.talents[talent] == undefined) return 0;
    return this.system.talents[talent];
  }

  addTalent(talent) {
    //FIXME: Check if talent exists in CONFIG
    if(this.system.talents == null) this.system.talents = {};
    if(this.system.talents[talent]) this.update({['system.talents.' + talent]: this.system.talents[talent] + 1});
    else this.update({['system.talents.' + talent]: 1});
  }

  removeTalent(talent) {
    if(this.system.talents == null) return;
    if(this.system.talents[talent] > 1) this.update({['system.talents.' + talent]: this.system.talents[talent] - 1});
    else this.update({['system.talents.-=' + talent]: null});
  }


  getTicksCost(action, item) {
    const configEntry = CONFIG.TAC.BASIC_ACTIONS[action];
    if(!configEntry) {
      console.error("Action not found! ", action);
      return 0;
    }
    let ticks_cost = configEntry.ticks_cost ? configEntry.ticks_cost : 0;

    if(action === "reload" && this.getTalentStacks('in_the_action') && item && item.system.subtype === "shotgun") {
      ticks_cost = 2 
    }

    return ticks_cost;
  }

  getClarityCost(action, item) {
    const configEntry = CONFIG.TAC.BASIC_ACTIONS[action];
    if(!configEntry) {
      console.error("Action not found! ", action);
      return 0;
    }
    let clarity_cost = configEntry.clarity_cost ? configEntry.clarity_cost : 0
    clarity_cost =  Math.floor(clarity_cost * this.system.other_traits.clarity_cost_multiplier.value);

    return clarity_cost;
  }
}