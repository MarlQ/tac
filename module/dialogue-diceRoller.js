import { roll } from "./roll.js";
export class DiceRollerDialogue extends Application {
  constructor(dicePool=1, targetNumber=6, flavor, addBonusFlavor=false, ...args) {
    super(...args);
    this.targetNumber = targetNumber;
    this.dicePool = dicePool;
    this.dicePool_userMod = 0;
    this.targetNumber_userMod = 0;
    this.flavor = flavor;
    this.targetNumber_diceMod = 0;
    this.addBonusFlavor = addBonusFlavor;
  }

  /* -------------------------------------------- */

	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue"],
  	  template: "systems/tac/templates/dialogues/dialogue-diceRoller.html",
      resizable: true
    });
  }
  
  getData() {
    const data = super.getData();
    data.targetNumber = this.targetNumber;
    data.dicePool = this.dicePool;
    data.bonusDice = this.bonusDice;
    
    return data;
  }
  
  _updateBaseValueFields(html){
    const dicePoolBase = html.find('[name="dicePoolBase"]');
    const targetNumberBase = html.find('[name="targetNumberBase"]');
    
    let newDicePool = +this.dicePool + +this.dicePool_userMod;
    if(newDicePool < 1){ 
      //Dice Pool can't go below 1, increase target number instead
      this.targetNumber_diceMod = Math.abs(newDicePool)+1;
      newDicePool = 1;
    }else this.targetNumber_diceMod = 0;
    
    dicePoolBase[0].value = newDicePool;
    targetNumberBase[0].value =  Math.max(0,Math.min(10, +this.targetNumber + +this.targetNumber_userMod + +this.targetNumber_diceMod));
  }
  
  activateListeners(html) {
    super.activateListeners(html);
    
    html.find('.roll-execute').click(ev => {
      const dicePoolBase = html.find('[name="dicePoolBase"]');
      const targetNumberBase = html.find('[name="targetNumberBase"]');
      DiceRollerDialogue.createDiceRollMessage(dicePoolBase[0].value, targetNumberBase[0].value, this.flavor, this.dicePool_userMod && this.addBonusFlavor, this.dicePool_userMod);
    });
    
    html.find('[name="dicePoolBonus"]').on("input", ev => {
      this.dicePool_userMod = +ev.target.value;
      
      this._updateBaseValueFields(html);
    });
    
    html.find('[name="targetNumberBonus"]').on("input", ev => {
      this.targetNumber_userMod = +ev.target.value;
      
      this._updateBaseValueFields(html);
    });
  }
  
  static async createDiceRollMessage(dicePool=1, targetNumber=6, flavor="Skill Check", addBonusFlavor, bonus){
      if(+dicePool > 0 && +targetNumber > 0){
        //Roll
        let rollResult = await roll(dicePool, targetNumber);
        
        //Create Roll Message
        let rollMode = game.settings.get("core", "rollMode");
        let speaker = ChatMessage.getSpeaker();
        
        if(addBonusFlavor) flavor += " + " + bonus;
        
        // Convert the roll to a chat message and return the roll
        return rollResult.toMessage({
          speaker: speaker,
          flavor: flavor
        }, { rollMode });
      }
    else console.log("ERROR: dice pool or target number is 0 or lower!");
  }

}