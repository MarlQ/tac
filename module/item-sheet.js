import { TippyContextMenu } from "./ui.js";
/**
 * Extend the basic ItemSheet with some very simple modifications
 */
export class SimpleItemSheet extends ItemSheet {
  constructor(...args) {
    super(...args);

    /**
     * Keep track of the currently active sheet tab
     * @type {string}
     */
  }

  /**
   * Extend and override the default options used by the Simple Item Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
			classes: ["worldbuilding", "sheet", "item"],
      width: 550,
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "attributes"
      }]
		});
  }

  /* -------------------------------------------- */

  /**
   * Return a dynamic reference to the HTML template path used to render this Item Sheet
   * @return {string}
   */
  get template() {
    const path = "systems/tac/templates/items/";
    return `${path}/${this.item.type}.html`;
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Item sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  getData() {
    console.log("ASd")
    const sheetData = {
      limited: this.item.limited,
      options: this.options,
      editable: this.isEditable,
      //cssClass: isOwner ? "editable" : "locked",
      config: CONFIG.TAC,
    };

    sheetData.item = this.item;
    sheetData.system = this.item.system;

    console.trace(sheetData);
    sheetData.damageTypes = [];
    sheetData.rangeValues = [];
    Object.keys(CONFIG.TAC.DAMAGE_TYPES).forEach(dType => sheetData.damageTypes.push({value: dType, name: CONFIG.TAC.DAMAGE_TYPES[dType].name}));
    if(this.item.type === "firearm" || this.item.type === "ammo") CONFIG.rangeValues_firearm.forEach(rVal => sheetData.rangeValues.push(rVal));
    else if(this.item.type === "melee") CONFIG.rangeValues_melee.forEach(rVal => sheetData.rangeValues.push(rVal));

    if(this.item.type === "firearm") sheetData.range_mod_readable = CONFIG.rangeValues_firearm[sheetData.system.range_mod].name;

    if(this.item.system.traits) {
      sheetData.traits = this.item.system.traits.map(trait => {
        return {name: trait, img: "systems/tac/icons/conditions/" + trait + ".svg", type: CONFIG.TAC.conditions[trait].type}
      });
  
      let f = true;
      while(sheetData.traits.length < CONFIG.TAC.TRAIT_MAXIMUM_TOTAL[this.item.type]) {
        if(f) {
          sheetData.traits.push({addButton: true});
          f = false;
        }
        else {
          sheetData.traits.push({empty: true});
        }
      }
  
      sheetData.ammoTraits = [];
      if(this.item.system.magazine) {
        sheetData.ammoTraits = this.item.system.magazine.system.traits.map(trait => {
          return {name: trait, img: "systems/tac/icons/conditions/" + trait + ".svg", type: CONFIG.TAC.conditions[trait].type}
        });
      }
    }
    

    return sheetData;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
    super.activateListeners(html);

    // Activate tabs
    let tabs = html.find('.tabs');
    let initial = this._sheetTab;
    new Tabs(tabs, {
      initial: initial,
      callback: clicked => this._sheetTab = clicked.data("tab")
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;
    
    let possibleTraits = [];

    const goodTraits = this.item.system.traits.filter(trait => CONFIG.TAC.WEAPON_TRAITS_GOOD.includes(trait));
    if(goodTraits.length < CONFIG.TAC.TRAIT_MAXIMUM_GOOD[this.item.type]) {
      possibleTraits = possibleTraits.concat(CONFIG.TAC.WEAPON_TRAITS_GOOD.map(name => {
        let img = "systems/tac/icons/conditions/" + name + ".svg";
        let type = CONFIG.TAC.conditions[name].type;
        return {
          name: name,
          icon: `<img src="${img}" title="${name}" class="${type}"/>`,
          condition: li => !this.item.hasTrait(name),
          callback: li => this.item.addTrait(name)
        }
      }));
    }

    possibleTraits = possibleTraits.concat(CONFIG.TAC.WEAPON_TRAITS_BAD.map(name => {
      let img = "systems/tac/icons/conditions/" + name + ".svg";
      let type = CONFIG.TAC.conditions[name].type;
      return {
        name: name,
        icon: `<img src="${img}" title="${name}" class="${type}"/>`,
        condition: li => !this.item.hasTrait(name),
        callback: li => this.item.addTrait(name),
        cssClasses: ["red"]
      }
    }));
    

    //new ContextMenu(html, ".add-trait-button", possibleTraits, {eventName: 'click'});


    new TippyContextMenu({element: html[0], selector: ".add-trait-button", menuItems: possibleTraits, eventName: 'click'})


    $('.trait-icon.filled').contextmenu(ev => {
      const header = ev.currentTarget;
      const trait = header.dataset.name;
      this.item.removeTrait(trait);
    });
  }

  /* -------------------------------------------- */
}
