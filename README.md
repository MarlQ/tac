# Third Advanced Conciousness

Works started again, and progress is made!
Probably a bit more time is needed until I can start with the proper Pre-Alpha tests.


3AC is a custom rpg made by me, set in a gritty cyberpunk setting.
It is supposed to bridge the gap between tabletop rpg and PC game.
The user experience is a main focus, to make play as fast as possible,
while having complex and interesting systems behind it all.
So basically, a "easy to learn, hard to master"-experience that normal tabletops would not allow.
Currently still in pre-Alpha.

Many things are still subject to be changed and added,
and don't expect any backwards compatibility with earlier version.
So... don't run a campaign with this.

## Features

* Main character sheet
* Sheets for items, etc.
* Cool styling!
* In-depth attack system. Currently no special skills.

What is obviously missing: a rule book.
